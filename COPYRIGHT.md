<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# Copyright Notice

The general copyright of G4SEE toolkit is owned by CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see> (2022).

This software includes work under other copyright notices as well.

This software is subject to the terms and conditions defined in the headers of each file and referenced licenses, which are part of this source code (in 'LICENSES' directory).

CERN intends to keep the copyright over the whole G4SEE toolkit, therefore asks a Copyright Transfer Agreement (CTA) from authors and contributors.
