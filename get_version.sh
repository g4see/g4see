#!/bin/bash

# clang-format off
#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
#
# SPDX-License-Identifier: GPL-3.0-or-later
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  
# clang-format on

VERSION=$(git describe --always --tags --long --abbrev=7)
COMMIT_ID=$(git log --format=%h -n 1)
BRANCH=$(git rev-parse --abbrev-ref HEAD)
TAG=$(git describe --abbrev=0 --tags)
mkdir -p $1/include
echo "
#ifndef VERSION_HH
#define VERSION_HH
#include <string>
const std::string kGitVersion = \"$VERSION\";
const std::string kGitTag = \"$TAG\";
const std::string kGitCommitID = \"$COMMIT_ID\";
const std::string kGitBranch = \"$BRANCH\";
#endif" > $1/include/version.hh