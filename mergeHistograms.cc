// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "include/version.hh"

#include <dirent.h>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <set>
#include <sstream>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

struct HistValues
{
	double bin = 0.;
	double count = 0.;
	double count_sq = 0.;
} emptyValue;

bool is_path_exists(std::string path)
{
	const std::filesystem::path realPath = path;
	return std::filesystem::exists(realPath);
}

std::string get_canonical_path(std::string path)
{
	const std::filesystem::path realPath = path;
	std::filesystem::path canPath = std::filesystem::canonical(realPath);
	return canPath;
}

std::vector<std::string> read_directory(const std::string& folder, std::string HistName)
{
	std::vector<std::string> v;
	DIR* dirp = opendir(folder.c_str());
	struct dirent* dp;
	while((dp = readdir(dirp)) != NULL)
	{
		std::string filename(dp->d_name);
		if(filename.find(HistName) != std::string::npos)
		{
			v.push_back(get_canonical_path(folder + "/" + filename));
		}
	}
	closedir(dirp);
	return v;
}

std::vector<std::vector<std::string>> find_hist_files(const std::string& name, std::string histName)
{
	std::size_t found = name.find_last_of("#");
	std::string common_path = name.substr(0, found);

	std::size_t found2 = common_path.find_last_of("/");
	std::string parent_dir = common_path.substr(0, found2);

	std::vector<std::vector<std::string>> vv;
	DIR* dir = opendir(parent_dir.c_str());
	struct dirent* entry = readdir(dir);
	while(entry != NULL)
	{
		if(entry->d_type == DT_DIR)
		{
			std::string folderName(entry->d_name);
			folderName = get_canonical_path(folderName);

			if(folderName.find(common_path) != std::string::npos)
			{
				vv.push_back(read_directory(folderName, histName));
			}
		}
		entry = readdir(dir);
	}
	closedir(dir);
	return vv;
}

std::set<std::string> FindUniqueHistogramTypes(std::string inFolder)
{
	std::set<std::string> hist_set;
	std::vector<std::vector<std::string>> folders_files = find_hist_files(inFolder, "_hist_");
	for(std::vector<std::vector<std::string>>::iterator it = folders_files.begin();
		it != folders_files.end(); ++it)
	{
		for(std::vector<std::string>::iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2)
		{
			std::string filename = *it2;
			std::size_t a = filename.find_last_of("/") + 1;
			std::size_t b = filename.find("_hist_");
			hist_set.insert(filename.substr(a, b - a));
		}
	}
	return hist_set;
}

std::pair<double, double> calculateStdDev(int N, double sum_count, double sum_count_sq)
{
	double sample_mean = 1. / N * sum_count;
	double sample_var_biased = (1. / N * sum_count_sq) - (sample_mean * sample_mean);
	double sample_var_unbiased = (double)N / (N - 1) * sample_var_biased;
	double sample_std_dev_uncorr = sqrt(sample_var_biased);
	double sample_std_dev_corr = sqrt(sample_var_unbiased);
	return {sample_std_dev_uncorr, sample_std_dev_corr};
}

int get_floating_precision(std::string str)
{
	std::stringstream ss(str);
	std::string bin_str, val_str;
	ss >> bin_str >> val_str;
	return static_cast<int>(bin_str.length()) - 6;
}

bool MergeHistograms(std::string inFolder, std::string outFolder, std::string Parameter,
					 bool flagDel = false)
{
	std::map<double, HistValues> histogram;
	double underflow = 0., overflow = 0.;
	double underflow_sq = 0., overflow_sq = 0.;
	bool histsExist = false;

	std::vector<std::vector<std::string>> folders = find_hist_files(inFolder, Parameter + "_hist_");
	if(folders.empty())
	{
		std::cerr << "No data folders found.\n\n";
	}

	int prec = -1;
	int nn = 0;
	for(std::vector<std::vector<std::string>>::iterator it = folders.begin(); it != folders.end();
		++it)
	{
		std::vector<std::string> files = *it;

		for(std::vector<std::string>::iterator it2 = files.begin(); it2 != files.end(); ++it2)
		{
			std::string filename = *it2;
			std::size_t a = filename.find_last_of("/") + 1;
			std::size_t b = filename.find("_hist_");
			std::string quantity = filename.substr(a, b - a);
			if(quantity.compare(Parameter) == 0)
			{
				histsExist = true;
				std::ifstream myfile;
				myfile.open(filename, std::ifstream::in);
				if(myfile.is_open())
				{
					nn += 1;
					std::string str;
					std::getline(myfile, str);
					std::string s1, s2;
					double uf, of;
					std::getline(myfile, str);
					std::stringstream us(str);
					us >> s1 >> uf;
					std::getline(myfile, str);
					std::stringstream os(str);
					os >> s2 >> of;
					underflow += uf;
					overflow += of;
					underflow_sq += uf * uf;
					overflow_sq += of * of;
					while(!myfile.eof())
					{
						std::getline(myfile, str);
						if(prec == -1)
							prec = get_floating_precision(str);
						std::stringstream ss(str);
						double bin, value;
						ss >> bin >> value;

						histogram.insert(std::pair<double, HistValues>(bin, emptyValue));
						histogram[bin].bin = bin;
						histogram[bin].count += value;
						histogram[bin].count_sq += value * value;
					}
					myfile.close();

					if(flagDel)
					{
						if(remove(filename.c_str()) != 0)
							perror("Error deleting file");
					}
				}
			}
		}
	}

	if(histsExist)
	{
		int w = 15;
		std::ofstream ofs;
		std::string output_filepath = outFolder + "/" + Parameter + "_histogram.out";
		ofs.open(output_filepath, std::ofstream::out | std::ofstream::trunc);
		if(ofs.is_open())
		{
			std::string ParameterName = Parameter.substr(0, Parameter.find_last_of("_"));

			// CALCULATE STD. DEV.
			double uf_std_dev_uncorr, uf_std_dev_corr, of_std_dev_uncorr, of_std_dev_corr;
			std::tie(uf_std_dev_uncorr, uf_std_dev_corr) =
				calculateStdDev(nn, underflow, underflow_sq);
			std::tie(of_std_dev_uncorr, of_std_dev_corr) =
				calculateStdDev(nn, overflow, overflow_sq);

			ofs << std::setw(w) << ParameterName << std::setw(w) << "Counts" << std::setw(w)
				<< "StdDev(corr)" << std::endl;
			ofs << std::setw(w) << "underflow";
			if(underflow != 0.)
				ofs << std::scientific << std::setprecision(prec) << std::setw(w) << underflow
					<< std::setw(w) << uf_std_dev_corr;
			else
				ofs << std::fixed << std::setprecision(0) << std::setw(w) << 0. << std::setw(w)
					<< 0.;
			ofs << std::endl;
			ofs << std::setw(w) << "overflow";
			if(overflow != 0.)
				ofs << std::scientific << std::setprecision(prec) << std::setw(w) << overflow
					<< std::setw(w) << of_std_dev_corr;
			else
				ofs << std::fixed << std::setprecision(0) << std::setw(w) << 0. << std::setw(w)
					<< 0.;
			ofs << std::endl;

			std::map<double, HistValues>::iterator it1;
			for(it1 = histogram.begin(); it1 != histogram.end(); ++it1)
			{
				double Bin = it1->first;
				HistValues hValues = it1->second;

				// CALCULATE STD. DEV.
				double std_dev_uncorr, std_dev_corr;
				std::tie(std_dev_uncorr, std_dev_corr) =
					calculateStdDev(nn, hValues.count, hValues.count_sq);

				ofs << std::scientific << std::setprecision(prec) << std::setw(w) << Bin;
				if((it1 != histogram.end()) && (next(it1) == histogram.end()))
				{
				}
				else
				{
					if(hValues.count != 0.)
					{
						ofs << std::scientific << std::setprecision(prec) << std::setw(w)
							<< hValues.count;
						ofs << std::scientific << std::setprecision(prec) << std::setw(w)
							<< std_dev_corr;
					}
					else
						ofs << std::fixed << std::setprecision(0) << std::setw(w) << 0.
							<< std::setw(w) << 0.;
				}
				ofs << std::endl;
			}
			ofs.close();
			std::cout << Parameter << " histograms are merged in '" << output_filepath << "'\n";
		}
	}
	return histsExist;
}

static void show_version()
{
	std::cerr << "\nG4SEE Single Event Effect simulation toolkit\n"
			  << "Git version: " << kGitVersion << "\n"
			  << "Git tag:     " << kGitTag << "\n"
			  << "Git commit:  " << kGitCommitID << "\n"
			  << "Git branch:  " << kGitBranch << "\n\n";
}

static void show_help(std::string name)
{
	std::cerr << "\nusage: " << name
			  << " [-h] [-v] [/common/path/to/data/folder(_#)] [-o OUTPUT_FOLDER] [-d]\n\n"
			  << "G4SEE Single Event Effect simulation toolkit - Histogram merging tool\n"
			  << "by David Lucsanyi (david.lucsanyi@cern.ch)\n\n"
			  << "version " << kGitVersion << "\n\n"
			  << "Merging histograms in a SINGLE FOLDER:   Simply define the folder path\n"
			  << "Merging histograms in MULTIPLE FOLDERS:  Common path has to be defined with '#' "
				 "symbol at the end!\n"
			  << "Default path: './out_#' \n\n"
			  << "optional arguments:\n"
			  << "\t-o, --out               Define output directory, default: '.'\n"
			  << "\t-d, --delete            Delete merged raw histogram files after merging\n"
			  << "\t-v, --version           Show version info\n"
			  << "\t-h, --help              Show this help message\n\n";
}

int main(int argc, char** argv)
{
	std::string nInputDir = "out_#";
	std::string nOutputDir(std::filesystem::current_path());
	bool nDelFiles = false;
	if(argc > 1)
	{
		std::string arg1 = argv[1];
		if((arg1 == "-h") || (arg1 == "--help") || (arg1 == "-o") || (arg1 == "--out"))
		{
			show_help(argv[0]);
			return 0;
		}
		else if((arg1 == "-v") || (arg1 == "--version"))
		{
			show_version();
			return 0;
		}
		else
		{
			nInputDir = arg1;
		}
	}
	if(argc > 2)
	{
		for(int i = 2; i < argc; ++i)
		{
			std::string arg = argv[i];
			if((arg == "-o") || (arg == "--out"))
			{
				if(i + 1 < argc)
				{
					nOutputDir = argv[++i];
				}
				else
				{
					std::cerr << "-o/--out argument requires a path string!\n\n";
					return 1;
				}
			}
			else if((arg == "-d") || (arg == "--delete"))
			{
				nDelFiles = true;
			}
			else
			{
				std::cerr << "Unknown or not valid input argument(s). See usage: " << argv[0]
						  << " -h\n\n";
				return 1;
			}
		}
	}

	std::cout << "\n###############################"
			  << "\n###  G4SEE mergeHistograms  ###"
			  << "\n###############################\n";

	if(is_path_exists(nOutputDir))
		nOutputDir = get_canonical_path(nOutputDir);
	else
	{
		std::cerr << "Output directory '" << nOutputDir << "' does not exist!\n\n";
		return 1;
	}

	std::string nInputDirParent(std::filesystem::current_path());

	if(is_path_exists(nInputDir))
	{
		nInputDir = get_canonical_path(nInputDir);
		std::filesystem::path realInputPath = nInputDir;
		nInputDirParent = realInputPath.parent_path();
		std::cout << "Directory with histograms to merge:    '" << nInputDir << "'\n\n";
	}
	else
	{
		// check if non-existent path contains wildcard (otherwise raise error)
		std::size_t found_wildcard = nInputDir.find_last_of("#");
		if(found_wildcard == std::string::npos)
		{
			std::cerr << "Path does not exist and no wildcard found in '" << nInputDir << "'!\n\n";
			return 1;
		}
		// split common path to existing canonical parent directory + subdirectory including wild
		// card
		std::size_t found_parent = nInputDir.find_last_of("/");
		if(found_parent == std::string::npos)
		{
			nInputDir = nInputDirParent + "/" + nInputDir;
		}
		else
		{
			found_parent += 1;
			std::string dirParent = nInputDir.substr(0, found_parent);
			std::string subFolder = nInputDir.substr(found_parent);
			if(is_path_exists(dirParent))
			{
				nInputDirParent = get_canonical_path(dirParent);
				nInputDir = nInputDirParent + "/" + subFolder;
			}
			else
			{
				std::cerr << "Parent directory '" << nInputDirParent << "' does not exist!\n\n";
				return 1;
			}
		}
		std::cout << "Parent directory to merge in:          '" << nInputDirParent << "'\n";
		std::cout << "Common path of histograms to merge:    '" << nInputDir
				  << "'  (where '#' is the wildcard)\n\n";
	}
	chdir(nInputDirParent.c_str());

	bool merged = false;
	std::set<std::string> histTypes = FindUniqueHistogramTypes(nInputDir);
	for(std::set<std::string>::iterator itt = histTypes.begin(); itt != histTypes.end(); ++itt)
		if(MergeHistograms(nInputDir, nOutputDir, *itt, nDelFiles))
			merged = true;

	if(!merged)
	{
		std::cerr << "No histogram files found to merge!\n\n";
		return 1;
	}

	std::cout << "\n";
	return 0;
}
