<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# Authors

## Main Developer

- Dávid Lucsányi (CERN)

## Contributor

- Eva Fialová (CERN)

## Alpha Testers

- Andrea Coronetti
- Matteo Cecchetto (CERN)

----

![cern](https://g4see.web.cern.ch/img/CERN-logo.png)  ![r2e](https://g4see.web.cern.ch/img/R2E-logo.png) 

- **[CERN (home.cern)](https://home.cern)**
- **[CERN Radiation To Electronics (cern.ch/r2e)](https://cern.ch/r2e)** 

----


```{important}
The use of G4SEE toolkit (https://cern.ch/g4see) must be acknowledged explicitly by citing the open-access G4SEE publication:

D. Lucsányi, R. García Alía, K. Biłko, M. Cecchetto, S. Fiore and E. Pirovano, "G4SEE: A Geant4-Based Single Event Effect Simulation Toolkit and Its Validation Through Monoenergetic Neutron Measurements," in IEEE Transactions on Nuclear Science, vol. 69, no. 3, pp. 273-281, March 2022, [doi:10.1109/TNS.2022.3149989](https://doi.org/10.1109/TNS.2022.3149989).
```
