<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# Contribution Guidelines

## Reporting Bugs

If you found a bug or any strange, unexpected behavior of the application or scripts, please create a GitLab issue in 
[CERN GitLab repository](https://gitlab.cern.ch/g4see/g4see/issues) (CERN account needed).

If you don't have access to CERN GitLab, please post your issue on the [G4SEE User Forum](https://g4see-forum.web.cern.ch/)!

## Feature requests

Do you have a new idea? Missing an essential feature? Do you want to request a specific feature? If yes, please ... 
- check existing GitLab issues, and create a new GitLab issue with your request (see above)
- join related discussion or start a new one on the [G4SEE User Forum](https://g4see-forum.web.cern.ch/)
- consider contributing to the code (see below)

## Contributing to the code

If you are interested to contribute to G4SEE, please contact us at **g4see.toolkit** *at* **cern** *dot* **ch**!

### Coding style

The `.clang-format` config file contains the G4SEE C++ coding style, all C++ source code in this repo should follow this coding style, 
which can be checked and enforced using the `clang-format` tool. 

Dry-run (no files are being overwritten):
```shell
clang-format -style=file --dry-run -Werror -i {,src/}*.cc
clang-format -style=file --dry-run -Werror -i include/*.hh
```
C++ files are being overwritten:
```shell
clang-format -style=file -Werror -i {,src/}*.cc
clang-format -style=file -Werror -i include/*.hh
```

Python source code in this repo should follow the PEP8 coding style guide, which could be checked using the `flake8` tool.
```shell
flake8 --config g4see-scripts/tests/.flake8 g4see-scripts/scripts/
```

### Licenses and copyrights

Please check the [COPYRIGHT notice](copyright.rst) for important information about copyrights!

The G4SEE toolkit is compliant with the [SPDX open standard](https://spdx.dev) for licensing and copyrights, 
and [REUSE Specification v3.3](https://reuse.software/spec-3.3) by the [Free Software Foundation Europe](https://fsfe.org). 

Please use the `REUSE` toolkit ([https://reuse.software](https://reuse.software)) to check compliance with the license and copyright standards:
```shell
python -m reuse lint
```
One should get such a summary by running the `reuse lint` tool: 
```shell
# SUMMARY

* Bad licenses: 0
* Deprecated licenses: 0
* Licenses without file extension: 0
* Missing licenses: 0
* Unused licenses: 0
* Used licenses: CC0-1.0, GPL-3.0-or-later, CC-BY-4.0, LicenseRef-Geant4, CC-BY-NC-ND-4.0
* Read errors: 0
* Files with copyright information: 199 / 199
* Files with license information: 199 / 199

Congratulations! Your project is compliant with version 3.3 of the REUSE Specification :-)
```

### Git commit messages

- `+ADD`  added new files and/or major feature
- `+CHG`  changes in features/classes/functions, new minor feature
- `+FIX`  fixed bug(s)
- `+REF`  refactoring
- `+DOC`  updated documentation
- `+DEL`  deleted files

### Build Docker image locally

To build Docker image locally in `g4see/` repository:
```shell
docker build --build-arg G4TAG=11.2.2 \
             --build-arg MT=True \
             --build-arg N_CPU=4 \
             --tag g4see:devel .
```
