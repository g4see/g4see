# clang-format off
#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
#
# SPDX-License-Identifier: GPL-3.0-or-later
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  
# clang-format on

local job(G4_VERSION) =
  {
    stage: 'test',
    image: '${IMAGE_PATH}',
    variables: {
      IMAGE_TAG: '${CI_COMMIT_REF_NAME}_G4-' + G4_VERSION,
      IMAGE_PATH: '${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}_G4-' + G4_VERSION,
    },
    before_script: [
      'cat /etc/os-release',
      'python3 -V',
      'echo Geant4 version: ' + G4_VERSION,
      'echo Geant4 library path: $G4LIB',
      'echo G4SEE git branch/tag: $CI_COMMIT_REF_NAME',
      'echo G4SEE image tag: $IMAGE_TAG',
      'echo G4SEE image URL: $IMAGE_PATH',
      'pwd',
      'ls -la',
      'source /opt/geant4/install/bin/geant4.sh',
    ],
    script: [
      'cd /home',
      'g4see -h',
      'echo Running G4SEE with SRAM example...',
      'mkdir -p output_sram',
      'g4see $G4SEE_BUILD/examples/SRAM.mac -o output_sram/',
      'mergeHistograms output_sram/ -o output_sram/',
      'ls -l output_sram/',
      'echo Running G4SEE with Diode example...',
      'mkdir -p output_diode',
      'g4see $G4SEE_BUILD/examples/silicon_diode.mac -o output_diode/',
      'mergeHistograms output_diode/ -o output_diode/',
      'ls -l output_diode/',
    ],
    allow_failure: false,
    artifacts: {
      paths: [
        'output_sram/',
        'output_diode/',
      ],
    },
  };

local G4_releases = [
  '11.2.2',
  '11.2.1',
  '11.2.0',
];
{
  ['tests/' + version]: job(version)
  for version in G4_releases
}