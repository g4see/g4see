# clang-format off
#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
#
# SPDX-License-Identifier: GPL-3.0-or-later
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  
# clang-format on

local job(G4_VERSION) =
  {
    stage: 'deploy',
    image: '$CI_REGISTRY/docker-images/docker:dind',
    services: [{
      name: '$CI_REGISTRY/docker-images/docker:dind',
      alias: 'docker',
    }],
    variables: {
      IMAGE_TAG: '${CI_COMMIT_REF_NAME}_G4-' + std.strReplace(G4_VERSION, '_Debian.12', ''),
      IMAGE_PATH: '${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}_G4-' + std.strReplace(G4_VERSION, '_Debian.12', ''),
    },
#    needs: [       # TODO
#    {
#      pipeline: '$PARENT_PIPELINE_ID',
#      job: 'sphinx-build',
#      artifacts: true,
#    },
#    ],
    before_script: [
      'docker info',
      'echo Geant4 docker: ' + G4_VERSION,
      'echo G4SEE git branch/tag: $CI_COMMIT_REF_NAME',
      'echo G4SEE image tag: $IMAGE_TAG',
      'echo G4SEE image URL: $IMAGE_PATH',
      'echo parent pipeline ID: $PARENT_PIPELINE_ID',
      'pwd',
#      'tar -xf html-docs.tar.gz',   # TODO
      'ls -la',
#      'ls -la build',
    ],
    script: [
      'echo -n $CI_JOB_TOKEN | docker login --username gitlab-ci-token --password-stdin $CI_REGISTRY',
      'docker build --build-arg G4TAG=' + G4_VERSION +
                  ' --build-arg MT=False' +
                  ' --build-arg N_CPU=8' +
                  ' --tag $IMAGE_PATH .',
      'docker image ls',
      'docker push $IMAGE_PATH',
    ],
    allow_failure: false,
    tags: ['docker-privileged-xl'],
  };

local G4_releases = [
  '11.2.2_Debian.12',
  '11.2.1_Debian.12',
  '11.2.0_Debian.12',
];
{
  ['docker/' + version]: job(version)
  for version in G4_releases
}
