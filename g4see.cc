// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ActionInitialization.hh"
#include "BiasingMessenger.hh"
#include "DeviceConstruction.hh"
#include "PhysicsList.hh"
#include "include/version.hh"

#include "G4MTRunManager.hh"
#include "G4ParticleHPManager.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UIExecutive.hh"
#include "G4UImanager.hh"
#include "G4VisExecutive.hh"

#include <chrono>
#include <ctime>
#include <dirent.h>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <random>
#include <sstream>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <vector>

bool is_path_exists(std::string path)
{
	const std::filesystem::path realPath = path;
	return std::filesystem::exists(realPath);
}

std::string get_canonical_path(std::string path)
{
	const std::filesystem::path realPath = path;
	std::filesystem::path canPath = std::filesystem::canonical(realPath);
	return canPath;
}

void GenerateOutput(G4String folder, G4String macrofile, clock_t begin, struct tm* datetime,
					G4String g4version, G4int events, G4int threads)
{
	clock_t end = clock(); // CPU TIME!
	double total_cpu_secs = double(end - begin) / CLOCKS_PER_SEC;
	char formatted_datetime[100];
	strftime(formatted_datetime, sizeof(formatted_datetime), "%Y/%m/%d %H:%M:%S", datetime);

	(void)threads; // suppress unused variable warning

	std::ifstream macro(macrofile, std::ifstream::in);
	std::ofstream out(folder + "/g4see.out", std::ofstream::out | std::ofstream::trunc);
	if(out.is_open())
	{
		out << "###################################################################################"
			   "##\n"
			<< "#                                 G4SEE OUTPUT FILE                                "
			   " #\n"
			<< "###################################################################################"
			   "##\n\n";

		out << "Simulation started:           " << formatted_datetime << "\n\n";

		out << "Geant4 release:               " << g4version.substr(g4version.find("geant4"))
			<< "\n\n";

		out << "G4SEE version:                " << kGitVersion << "\n";
		out << "G4SEE tag:                    " << kGitTag << "\n";
		out << "G4SEE commit ID:              " << kGitCommitID << "\n";
		out << "G4SEE branch:                 " << kGitBranch << "\n\n";

#if defined(G4MULTITHREADED) && defined(APP_MULTITHREADED)
		out << "Multi-Threaded mode\n";
		out << "Number of threads used:       " << threads << "\n\n";
#endif

		out << "Number of primary particles:  " << events << "\n\n";

		out << "Total CPU time (s):           " << total_cpu_secs << "\n";
		out << "CPU time / primary (s):       " << (double)total_cpu_secs / events << "\n\n";

		out << "Output directory:             " << folder << "\n";
		out << "Input macro file:             " << macrofile << "\n\n";

		out << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			   "++\n"
			<< "+                               Input Macro Commands                               "
			   " +\n"
			<< "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			   "++\n\n";
		out << macro.rdbuf();
	}
}

static void show_version()
{
	std::cerr << "\nG4SEE Single Event Effect simulation toolkit\n"
			  << "Git version: " << kGitVersion << "\n"
			  << "Git tag:     " << kGitTag << "\n"
			  << "Git commit:  " << kGitCommitID << "\n"
			  << "Git branch:  " << kGitBranch << "\n\n";
}

static void show_help(std::string name)
{
	std::cerr << "\nusage: " << name << " [-h] [-v] /path/to/input/macro.mac [-o OUTPUT_FOLDER]\n\n"
			  << "G4SEE Single Event Effect simulation toolkit - G4SEE application\n"
			  << "by David Lucsanyi (david.lucsanyi@cern.ch)\n\n"
			  << "version " << kGitVersion << "\n\n"
			  << "optional arguments:\n"
			  << "\t-o, --out               Define output directory, default: '.'\n"
			  << "\t-v, --version           Show version info\n"
			  << "\t-h, --help              Show this help message\n\n";
}

int main(int argc, char** argv)
{
	// MEASURING CPU TIME
	time_t systime = time(NULL); // for random seed
	clock_t start_cpu_time = clock();
	time_t rawtime;
	time(&rawtime);
	struct tm* datetime;
	datetime = localtime(&rawtime);

	G4String MacroFileName;
	G4String nOutputDir(std::filesystem::current_path());
	if(argc != 1)
	{
		std::string arg1 = argv[1];
		if((arg1 == "-h") || (arg1 == "--help") || (arg1 == "-o") || (arg1 == "--out"))
		{
			show_help(argv[0]);
			return 0;
		}
		else if((arg1 == "-v") || (arg1 == "--version"))
		{
			show_version();
			return 0;
		}
		else
			MacroFileName = arg1;
		if(is_path_exists(MacroFileName))
			MacroFileName = get_canonical_path(MacroFileName);
		else
		{
			std::cerr << "Macro file '" << MacroFileName << "' does not exist!\n\n";
			return 1;
		}

		for(int i = 2; i < argc; ++i)
		{
			std::string arg = argv[i];
			if((arg == "-o") || (arg == "--out"))
			{
				if(i + 1 < argc)
				{
					nOutputDir = argv[++i];
				}
				else
				{
					std::cerr << "-o/--out argument requires a path string!\n\n";
					return 1;
				}
			}
			else
			{
				std::cerr << "Unknown or not valid input argument(s). See usage: " << argv[0]
						  << " -h\n\n";
				return 1;
			}
		}
	}

	if(is_path_exists(nOutputDir))
		nOutputDir = get_canonical_path(nOutputDir);
	else
	{
		std::cerr << "Output directory '" << nOutputDir << "' does not exist!\n\n";
		return 1;
	}
	G4String g4see_path = get_canonical_path(argv[0]);

#if defined(G4MULTITHREADED) && defined(APP_MULTITHREADED)
	G4MTRunManager* pRunManager = new G4MTRunManager;
	G4int nCores = G4Threading::G4GetNumberOfCores();
	G4int nThreads = 1; // Default number of threads, unless overwritten by macro
	pRunManager->SetNumberOfThreads(nThreads);
	G4cout << "Default number of CPU threads used: " << nThreads << "  (out of " << nCores
		   << " available)" << G4endl
		   << "which will be overwritten by macro command:  /run/numberOfThreads  k" << G4endl
		   << G4endl;
#else
	G4RunManager* pRunManager = new G4RunManager;
#endif

	// Set random engines with a random seed by default
	std::mt19937::result_type seed =
		std::chrono::high_resolution_clock::now().time_since_epoch().count();
	auto real_mt_rand = std::bind(std::uniform_real_distribution<double>(0, 1), std::mt19937(seed));

	long seeds[2];
	seeds[0] = systime * real_mt_rand();
	seeds[1] = time(NULL) * real_mt_rand();
	G4Random::setTheSeeds(seeds);

	const long int* ss = G4Random::getTheSeeds();
	G4cout << "Default random seeds: " << ss[0] << ", " << ss[1] << G4endl
		   << "which will be overwritten by macro command:  /random/setSeeds  iiii, jjjj" << G4endl
		   << G4endl;

	G4cout << "G4SEE path:        " << g4see_path << G4endl;
	G4cout << "Input macro file:  " << MacroFileName << G4endl;
	G4cout << "Output directory:  " << nOutputDir << G4endl << G4endl;

	DeviceConstruction* device = new DeviceConstruction(nOutputDir);

	pRunManager->SetUserInitialization(device);

	PhysicsList* physics = new PhysicsList();
	pRunManager->SetUserInitialization(physics);

	new BiasingMessenger(device, physics); // todo

	// User action initialization
	ActionInitialization* actions = new ActionInitialization(nOutputDir);
	pRunManager->SetUserInitialization(actions);

	// Update settings of ParticleHP physics models:
	G4ParticleHPManager::GetInstance();

	G4VisManager* visManager = new G4VisExecutive();
	visManager->Initialize();

	// Get the pointer to the User Interface manager
	G4UImanager* UImanager = G4UImanager::GetUIpointer();

	if(argc == 1)
	{
		// this sets up the user interface to run in interactive mode
		G4UIExecutive* ui = new G4UIExecutive(argc, argv);
		G4cout << " UI session starts ..." << G4endl;
		ui->SessionStart();
		delete ui;
	}
	else
	{
		// otherwise we run in batch mode
		G4String command = "/control/execute "; // create first part of command
		UImanager->ApplyCommand(
			command + MacroFileName); // join the two and pass to the UI manager for interpretation
	}

	G4int NumOfEvents = pRunManager->GetCurrentRun()->GetNumberOfEvent();
	G4String G4version = pRunManager->GetVersionString();

	G4int NumOfThreads = 1;
#if defined(G4MULTITHREADED) && defined(APP_MULTITHREADED)
	NumOfThreads = pRunManager->GetNumberOfThreads();
#endif

	GenerateOutput(nOutputDir, MacroFileName, start_cpu_time, datetime, G4version, NumOfEvents,
				   NumOfThreads);

	delete visManager;
	delete pRunManager;

	return 0;
}
