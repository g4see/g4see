<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# G4SEE tutorials and examples

In this file one can find G4SEE examples and tutorials to demonstrate how to use G4SEE toolkit. 
The user can can customize these examples for their own simulation.

This file consist of two G4SEE tutorials, which demonstrates step by step 
creation of G4SEE simulation run and macro file using main G4SEE features.

Examples simulating different types of devices using various simulation features:
1) **SRAM.mac** simulates 65-nm SRAM memory, irradiated with 10 MeV protons
2) **detailed_scoring.mac** is an example of using the detailed scoring feature of G4SEE 
3) **diamond_detector.mac** simulates diamond detector irradiated with 14 MeV neutrons. This example shows usage of special UI commands which have to be used for neutron interactions with the carbon nucleus.
4) **silicon_diode.mac** simulates response of silicon diode detector irradiated with 15 MeV neutrons
5) **spectrum.mac** with **differential_fluence.data** is an example of how to import an arbitrary point-wise energy distribution from an ASCII file
6) **vis.mac** is an additional example macro for visualization of G4SEE examples


## How to Run the Examples

Running G4SEE examples with Docker:
1) Create a new output directory:
   - `mkdir /path/to/run_output/`
2) Execute macro file with G4SEE application:
   - `g4see /path/to/examples/<example_name>.mac -o /path/to/run_output/`
3) Merge created standard scoring histogram files of *Edep* and *Ekin* in one folder:
   - `mergeHistograms /path/to/run_output/ -o /path/to/run_output/`

Running G4SEE examples on computer cluster:
1) Create a new output directory:
   - `mkdir run_output/`
2) Submit G4SEE parallel jobs to a computer cluster queue:
   - `python3 g4see.py submit -o run_output/ -j <Njobs> -q <queue_name> -G4 geant4_path -G4SEE g4see_path /path/to/examples/<example_name>.mac`
3) Merging created histograms recursively:
   - `python3 g4see.py merge run_output/`

## Running with visualization

In the [visualization macro](vis.mac) are listed `/vis/` commands for 
the initialization of drawing. This macro is read from the `examples/<example_name>.mac`
file by using the command `/control/execute examples/vis.mac`.
By commenting, uncommenting or adding new commands in this `vis.mac` macro one can change 
display settings.

1) Add (or uncomment) `vis.mac` macro execution in G4SEE example macro after run initialization (`/run/initialize`): 
   - `/control/execute examples/vis.mac`
2) Run G4SEE application in interactive UI mode with visualization:
   - `g4see`
3) Execute G4SEE example macro in interactive UI mode:
   - `/control/execute <example_name>.mac`

## Plotting geometry and histograms

Defined geometry can be plotted by command:
- `python3 g4see.py view <example_name>.mac`

Created histograms can be plotted by command:
- `python3 g4see.py plot <data_hist_name>.out`







