# clang-format off
#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
#
# SPDX-License-Identifier: GPL-3.0-or-later
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  
# clang-format on

cmake_minimum_required(VERSION 3.17 FATAL_ERROR)
project(g4see)

set (CMAKE_CXX_STANDARD 17)

#----------------------------------------------------------------------------
if(WIN32)
  # TODO implement and test this on Windows
  # add_custom_target( get_git_version
  #   get_version.cmd ${CMAKE_CURRENT_BINARY_DIR}
  #   COMMENT "Call batch script to get git version"
  #   WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  # )
else()
  add_custom_target( get_git_version
    ./get_version.sh ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Call shell script to get git version"
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  )
endif()

#----------------------------------------------------------------------------
# Find Geant4 package, activating all available Vis drivers by default
# You can set WITH_GEANT4_VIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build application with Geant4 UI and Vis drivers" ON)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
#
include(${Geant4_USE_FILE})

# Setup of Multi-Threaded mode : optional.
option(MULTITHREADED "Build multi-threaded application" ON)
if(MULTITHREADED)
add_definitions(-DAPP_MULTITHREADED)
endif()

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
include_directories(${PROJECT_SOURCE_DIR}/include  
                    ${Geant4_INCLUDE_DIR} )
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(g4see g4see.cc ${sources} ${headers})
target_link_libraries(g4see ${Geant4_LIBRARIES})

add_executable(mergeHistograms mergeHistograms.cc ${headers})

add_dependencies(g4see get_git_version)
add_dependencies(mergeHistograms get_git_version)

#----------------------------------------------------------------------------
# Set C++17 standard
#
set_property(TARGET g4see PROPERTY CXX_STANDARD 17)
set_property(TARGET mergeHistograms PROPERTY CXX_STANDARD 17)

#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build g4see. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
file(GLOB MAC_FILES_1 "examples/*.mac")
file(GLOB YAML_FILES_1 "examples/*.yaml")
file(GLOB HIST_FILES "examples/*.data")
file(GLOB MD_FILES "examples/README.md")
file(COPY ${MAC_FILES_1} DESTINATION examples)
file(COPY ${YAML_FILES_1} DESTINATION examples)
file(COPY ${HIST_FILES} DESTINATION examples)
file(COPY ${MD_FILES} DESTINATION examples)

file(GLOB TUT_FILES_1 "examples/tutorial_1/*.*")
file(GLOB TUT_FILES_2 "examples/tutorial_2/*.*")
file(COPY ${TUT_FILES_1} DESTINATION examples/tutorial_1)
file(COPY ${TUT_FILES_2} DESTINATION examples/tutorial_2)

file(GLOB MAC_FILES_2 "g4see-scripts/examples_parametric/*.mac")
file(GLOB YAML_FILES_2 "g4see-scripts/examples_parametric/*.yaml")
file(COPY ${MAC_FILES_2} DESTINATION examples_parametric)
file(COPY ${YAML_FILES_2} DESTINATION examples_parametric)

file(GLOB CFG_FILES "g4see-scripts/configs/*.yaml")
file(COPY ${CFG_FILES} DESTINATION configs)

file(GLOB PY_SCRIPTS "g4see-scripts/scripts/*.py")
file(GLOB REQ_FILE "g4see-scripts/requirements.txt")
file(COPY ${PY_SCRIPTS} DESTINATION scripts)
file(COPY ${REQ_FILE} DESTINATION scripts)

#----------------------------------------------------------------------------
# ACCESS GIT VERSION INFO IN SOURCECODE
#
target_include_directories(g4see PUBLIC "${CMAKE_CURRENT_BINARY_DIR}")
target_include_directories(mergeHistograms PUBLIC "${CMAKE_CURRENT_BINARY_DIR}")

#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
install(TARGETS g4see mergeHistograms DESTINATION bin)

#----------------------------------------------------------------------------
# Export compile command database (JSON) file for the clang-tidy tool
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Get all project files
file(GLOB_RECURSE ALL_SOURCE_FILES *.cc *.hh)

#----------------------------------------------------------------------------
# Clang-tidy
# Adding clang-tidy target if executable is found
# supported version: 14.0.6
find_program(CLANG_TIDY "clang-tidy")
if(CLANG_TIDY)
  add_custom_target(
    clang-tidy
    # COMMAND /usr/bin/clang-tidy
    # COMMAND run-clang-tidy-14
    COMMAND python3 /usr/share/clang/run-clang-tidy.py
    -j 4
    -quiet
    -checks=*,-*-use-auto
    -header-filter="^include"
    -p .
    -format -style=file
    # -export-fixes fixes.yaml   ## BUG: duplicated fixes
    # -fix
    ${ALL_SOURCE_FILES}
  )
endif()

#----------------------------------------------------------------------------
# Clang-format
# Adding clang-format target if executable is found
# supported version: 14.0.6
find_program(CLANG_FORMAT "clang-format")
if(CLANG_FORMAT)
  add_custom_target(
    clang-format-version
    COMMAND /usr/bin/clang-format
    --version
  )
  add_custom_target(
    clang-format
    COMMAND /usr/bin/clang-format
    -i
    -style=file
    ${ALL_SOURCE_FILES}
  )
endif()
