# clang-format off
#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
#
# SPDX-License-Identifier: GPL-3.0-or-later
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  
# clang-format on

ARG G4TAG=11.2.2_Debian.12
FROM lucsanyid/geant4:${G4TAG}

LABEL description="G4SEE image"
LABEL maintainer="David Lucsanyi"
LABEL website="https://cern.ch/g4see"
LABEL base_image="lucsanyid/geant4:${G4TAG}"

#ARG MT=True
ARG MT=False
ARG N_CPU=4

COPY . /opt/g4see
RUN mkdir -p /opt/g4see/build
SHELL ["/bin/bash", "-c"]

# Compiling G4SEE application
RUN source /opt/geant4/install/bin/geant4.sh && \
    cd /opt/g4see/build && \
    cmake -DGeant4_DIR=$G4LIB -DMULTITHREADED=$MT .. && \
    make -j $N_CPU

# Install python dependencies for G4SEE scripts
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1 && \
    python3.11 -m pip install --break-system-packages --upgrade pip && \
    python3.11 -m pip install --break-system-packages --no-cache-dir -r /opt/g4see/build/scripts/requirements.txt

ENV G4SEE_SRC="/opt/g4see"
ENV G4SEE_BUILD="/opt/g4see/build"
ENV G4SEE_SCRIPTS="/opt/g4see/build/scripts"
ENV PYTHONPATH="${PYTHONPATH}:${G4SEE_SCRIPTS}"
ENV PATH="${G4SEE_SCRIPTS}:${G4SEE_BUILD}:${PATH}"
RUN ln -s $G4SEE_BUILD/g4see /home/g4see
RUN ln -s $G4SEE_BUILD/mergeHistograms /home/mergeHistograms

COPY config.sh /root
RUN chmod +x /root/config.sh
ENTRYPOINT ["/root/config.sh"]
WORKDIR /home
CMD ["/bin/bash"]
