// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef DeviceConstruction_H
#define DeviceConstruction_H 1
#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4VPhysicalVolume;
class G4Element;
class G4Material;
class DeviceMessenger;
class BiasingMultiParticleXS;
class SensitiveVolume;
class G4Region;

struct Layer
{
	G4String name;
	G4Material* material;
	G4double thickness;
	G4double xside;
	G4double yside;
	G4bool bias;
	G4ThreeVector position;
};

class DeviceConstruction : public G4VUserDetectorConstruction
{
  public:
	DeviceConstruction(const G4String& outputDir);
	~DeviceConstruction();

	void Defaults();
	G4VPhysicalVolume* Construct();
	void ConstructSDandField();

	void AddNewElement(G4String name, G4String symbol, std::map<G4int, G4double> isotopes);
	void AddNewMaterial(G4String name, G4double density, std::map<G4String, G4int> elements);
	void AddNewMixture(G4String name, G4double density, std::map<G4String, G4double> materials);

	void AddNewLayer(G4String name, G4double thickness, G4double xwidth, G4bool bias,
					 G4String material = "", G4ThreeVector position = G4ThreeVector(0., 0., 0.),
					 G4double ywidth = 0.);

	void AddBiasedParticle(G4String particle)
	{
		lastParticleName = particle;
		biasParticleWeights[particle] = 1.;
	};

	void AddBiasWeight(G4double factor)
	{
		biasParticleWeights[lastParticleName] = factor;
	};

	void SetBiasForNonPrimaryParticles(G4bool bool_value)
	{
		fBiasForNonPrimaries = bool_value;
	};

	void UseCylindricalGeometry(G4bool bool_value)
	{
		fCylindricalGeom = bool_value;
	};

	void SetWorldMaterial(G4String string_value)
	{
		fWorldMaterialName = string_value;
	};

	void UseWorldAsMotherOfSV(G4bool bool_value)
	{
		fWorldAsMotherSV = bool_value;
	};

	G4LogicalVolume* GetScoringVolume() const
	{
		return fLogicSensitive;
	}

	void SetCutForGamma(G4String r, G4double c)
	{
		if(r.compare("SV") == 0)
			gammCutSV = c;
		else if(r.compare("Bulk") == 0)
			gammCutBulk = c;
		else if(r.compare("BEOL") == 0)
			gammCutBEOL = c;
	}

	void SetCutForElectron(G4String r, G4double c)
	{
		if(r.compare("SV") == 0)
			elecCutSV = c;
		else if(r.compare("Bulk") == 0)
			elecCutBulk = c;
		else if(r.compare("BEOL") == 0)
			elecCutBEOL = c;
	}

	void SetCutForPositron(G4String r, G4double c)
	{
		if(r.compare("SV") == 0)
			posiCutSV = c;
		else if(r.compare("Bulk") == 0)
			posiCutBulk = c;
		else if(r.compare("BEOL") == 0)
			posiCutBEOL = c;
	}

	void SetCutForProton(G4String r, G4double c)
	{
		if(r.compare("SV") == 0)
			hadrCutSV = c;
		else if(r.compare("Bulk") == 0)
			hadrCutBulk = c;
		else if(r.compare("BEOL") == 0)
			hadrCutBEOL = c;
	}

  private:
	G4String fOutputDir;
	DeviceMessenger* fMessenger;

	std::vector<Layer> fTargetLayers;

	G4Region* fRegionWorld;
	G4Region* fRegionBulk;
	G4Region* fRegionSV;
	G4Region* fRegionBEOL;

	G4LogicalVolume* fLogicWorld;
	G4LogicalVolume* fLogicBlack;
	G4LogicalVolume* fLogicBulk;
	G4LogicalVolume* fLogicSensitive;
	G4LogicalVolume* fLogicMotherSV;
	std::vector<G4LogicalVolume*> fLogicBEOL;

	G4bool fCylindricalGeom;

	std::map<G4String, G4double> biasParticleWeights;
	G4String lastParticleName;
	G4bool fBiasForNonPrimaries;

	G4String fWorldMaterialName;
	G4bool fWorldAsMotherSV;

	std::map<G4String, G4Element*> fElementStore;
	std::map<G4String, G4Material*> fMaterialStore;
	std::map<G4String, G4Material*> fMixtureStore;

	G4double gammCutSV;
	G4double gammCutBulk;
	G4double gammCutBEOL;
	G4double elecCutSV;
	G4double elecCutBulk;
	G4double elecCutBEOL;
	G4double posiCutSV;
	G4double posiCutBulk;
	G4double posiCutBEOL;
	G4double hadrCutSV;
	G4double hadrCutBulk;
	G4double hadrCutBEOL;
};
#endif
