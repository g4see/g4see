// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ScoringMessenger_h
#define ScoringMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class SensitiveVolume;
class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithABool;
class G4UIcommand;

class ScoringMessenger : public G4UImessenger
{
  public:
	ScoringMessenger(SensitiveVolume*);
	virtual ~ScoringMessenger();

	virtual void SetNewValue(G4UIcommand*, G4String);

  private:
	SensitiveVolume* fSensitiveVolume;

	G4UIdirectory* fScoreDirectory;
	G4UIdirectory* fStdScoreDirectory;

	G4UIcommand* fEdepScoreCmd;
	G4UIcommand* fEkinScoreCmd;
	G4UIcommand* fLETScoreCmd;
	G4UIcommand* fHistCmd;
	G4UIcmdWithAnInteger* fHistPrecCmd;
	G4UIcmdWithAnInteger* fHistDumpCmd;

	G4UIcmdWithABool* fDSCmd;

	G4UIcmdWithABool* fDSCsvCmd;
	G4UIcmdWithAnInteger* fDSEventLimCmd;
	G4UIcommand* fDSMemLimCmd;

	G4UIcmdWithABool* fElecGrpCmd;
	G4UIcmdWithABool* fPosiGrpCmd;
	G4UIcmdWithABool* fGammGrpCmd;

	G4UIcmdWithABool* fPrimCmd;

	G4UIcmdWithADoubleAndUnit* fElecThrCmd;
	G4UIcmdWithADoubleAndUnit* fPosiThrCmd;
	G4UIcmdWithADoubleAndUnit* fGammThrCmd;

	G4UIcmdWithABool* fDSCmdTrack;
	G4UIcmdWithABool* fDSCmdParent;
	G4UIcmdWithABool* fDSCmdEkin;
	G4UIcmdWithABool* fDSCmdPos;
	G4UIcmdWithABool* fDSCmdMom;
	G4UIcmdWithABool* fDSCmdProcess;
	G4UIcmdWithABool* fDSCmdEdep;
	G4UIcmdWithABool* fDSCmdNdep;
	G4UIcmdWithABool* fDSCmdCounts;
	G4UIcmdWithABool* fDSCmdZ;
	G4UIcmdWithABool* fDSCmdA;
	G4UIcmdWithABool* fDSCmdVol;
	G4UIcmdWithABool* fDSCmdEexc;
};

#endif
