// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef BiasingMultiParticleXS_hh
#define BiasingMultiParticleXS_hh 1

#include "G4VBiasingOperator.hh"
class BiasingXS;
class G4ParticleDefinition;

#include <map>

class BiasingMultiParticleXS : public G4VBiasingOperator
{
  public:
	BiasingMultiParticleXS();

	virtual ~BiasingMultiParticleXS()
	{}

	// ---------------------------------
	// -- Method specific to this class:
	// ---------------------------------
	// -- Each particle type for which its name is passed will be biased; *provided*
	// -- that the proper calls to biasingPhysics->Bias(particleName) have been done
	// -- in the main program.
	void AddParticle(G4String particleName, G4double biasFactor);

	void ExtendBiasForNonPrimaries()
	{
		fBiasOnlyPrimaries = false;
	};

  private:
	// -----------------------------
	// -- Mandatory from base class:
	// -----------------------------
	// -- This method returns a biasing operation that will bias the physics process occurence:
	virtual G4VBiasingOperation*
		ProposeOccurenceBiasingOperation(const G4Track* track,
										 const G4BiasingProcessInterface* callingProcess);

	// -- Methods not used:
	virtual G4VBiasingOperation* ProposeFinalStateBiasingOperation(const G4Track*,
																   const G4BiasingProcessInterface*)
	{
		return 0;
	}

	virtual G4VBiasingOperation* ProposeNonPhysicsBiasingOperation(const G4Track*,
																   const G4BiasingProcessInterface*)
	{
		return 0;
	}

	G4bool fBiasOnlyPrimaries;

  private:
	// -- ("using" is to avoid compiler complaining against (false) method shadowing.)
	using G4VBiasingOperator::OperationApplied;

	// -- Optionnal base class method implementation.
	// -- This method is called to inform the operator that a proposed operation has been applied.
	// -- In the present case, it means that a physical interaction occured (interaction at
	// -- PostStepDoIt level):
	virtual void OperationApplied(const G4BiasingProcessInterface* callingProcess,
								  G4BiasingAppliedCase biasingCase,
								  G4VBiasingOperation* occurenceOperationApplied,
								  G4double weightForOccurenceInteraction,
								  G4VBiasingOperation* finalStateOperationApplied,
								  const G4VParticleChange* particleChangeProduced);

  public:
	// -- Optionnal base class method. It is called at the time a tracking of a particle starts:
	void StartTracking(const G4Track* track);

  private:
	// -- List of associations between particle types and biasing operators:
	std::map<const G4ParticleDefinition*, BiasingXS*> fBOptrForParticle;
	std::vector<const G4ParticleDefinition*> fParticlesToBias;
	BiasingXS* fCurrentOperator;

	// -- count number of biased interations for current track:
	G4int fnInteractions;
};

#endif
