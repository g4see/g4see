// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef DeviceMessenger_h
#define DeviceMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class DeviceConstruction;
class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcommand;

class DeviceMessenger : public G4UImessenger
{
  public:
	DeviceMessenger(DeviceConstruction*);
	virtual ~DeviceMessenger();

	virtual void SetNewValue(G4UIcommand*, G4String);

  private:
	DeviceConstruction* fDeviceConstruction;

	G4UIdirectory* fMatDirectory;
	G4UIdirectory* fDetDirectory;
	G4UIdirectory* cutsDir;

	G4UIcommand* fElemCmd;
	G4UIcommand* fMatCmd;
	G4UIcommand* fMixCmd;

	G4UIcommand* fBulkCmd;
	G4UIcommand* fSensVolCmd;
	G4UIcommand* fBEOLCmd;

	G4UIcommand* gammaCutCmd;
	G4UIcommand* electCutCmd;
	G4UIcommand* positCutCmd;
	G4UIcommand* hadroCutCmd;

	G4UIcmdWithABool* fCylCmd;
	G4UIcmdWithABool* fWorldSVCmd;
	G4UIcmdWithAString* fWorldMatCmd;
};

#endif
