// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ElectronCapture_h
#define ElectronCapture_h 1

#include "G4ParticleChangeForGamma.hh"
#include "G4VDiscreteProcess.hh"
#include "globals.hh"

class G4Region;

class ElectronCapture : public G4VDiscreteProcess
{
  public:
	ElectronCapture(const G4String& regName, G4double ekinlimit);

	virtual ~ElectronCapture();

	void SetKinEnergyLimit(G4double);

	virtual void BuildPhysicsTable(const G4ParticleDefinition&);

	virtual G4bool IsApplicable(const G4ParticleDefinition&);

	virtual G4double PostStepGetPhysicalInteractionLength(const G4Track& track,
														  G4double previousStepSize,
														  G4ForceCondition* condition);

	virtual G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);

  protected:
	virtual G4double GetMeanFreePath(const G4Track&, G4double, G4ForceCondition*);

  private:
	// hide assignment operator as private
	ElectronCapture(const ElectronCapture&);
	ElectronCapture& operator=(const ElectronCapture& right);

	G4double kinEnergyThreshold;
	G4String regionName;
	G4Region* region;
	G4ParticleChangeForGamma fParticleChange;
};

#endif
