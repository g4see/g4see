// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ParticleHit_h
#define ParticleHit_h 1

#include "tls.hh" // FOR MT
#include <unordered_set>

#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "G4VHit.hh"

class ParticleHit : public G4VHit
{
  public:
	ParticleHit();
	ParticleHit(const ParticleHit&);
	virtual ~ParticleHit();

	// operators
	const ParticleHit& operator=(const ParticleHit&);
	G4bool operator==(const ParticleHit&) const;

	inline void* operator new(size_t);
	inline void operator delete(void*);

	// methods from base class
	virtual void Draw();
	virtual void Print();

	// Set methods
	void SetEventID(G4int event)
	{
		fEventID = event;
	};

	void SetTrackID(G4int track)
	{
		fTrackID = track;
	};

	void SetParentID(G4int parent)
	{
		fParentID = parent;
	};

	void SetName(G4String name)
	{
		fName = name;
	};

	void SetEdep(G4double de)
	{
		fEdep = de;
	};

	void SetNdep(G4double dn)
	{
		fNdep = dn;
	};

	void SetStepLength(G4double s)
	{
		fStepLen = s;
	};

	void SetLETcalc(G4double let)
	{
		fLETcalc = let;
	};

	void SetLETstep(G4double let)
	{
		fLETstep = let;
	};

	void SetFirstStep(G4bool b)
	{
		fIsFirstStep = b;
	};

	void SetLastStep(G4bool b)
	{
		fIsLastStep = b;
	};

	void SetFirstPos(G4ThreeVector xyz)
	{
		fPosFirst = xyz;
	};

	void SetLastPos(G4ThreeVector xyz)
	{
		fPosLast = xyz;
	};

	void SetFirstMom(G4ThreeVector uvw)
	{
		fMomFirst = uvw;
	};

	void SetLastMom(G4ThreeVector uvw)
	{
		fMomLast = uvw;
	};

	void SetEkin(G4double dk)
	{
		fEkin = dk;
	};

	void SetProcess(G4String proc)
	{
		fProc = proc;
	};

	void SetWeight(G4double w)
	{
		fWeight = w;
	};

	void SetWeightPreStep(G4double w)
	{
		fWeightPreStep = w;
	};

	void SetWeightPostStep(G4double w)
	{
		fWeightPostStep = w;
	};

	void SetZ(G4int z)
	{
		fZ = z;
	};

	void SetA(G4int a)
	{
		fA = a;
	};

	void SetVolume(G4String vol)
	{
		fVol = vol;
	};

	// Get methods
	G4bool IsFirstStep() const
	{
		return fIsFirstStep;
	};

	G4bool IsLastStep() const
	{
		return fIsLastStep;
	};

	G4int GetEventID() const
	{
		return fEventID;
	};

	G4int GetTrackID() const
	{
		return fTrackID;
	};

	G4int GetParentID() const
	{
		return fParentID;
	};

	G4String GetName() const
	{
		return fName;
	};

	G4double GetEdep() const
	{
		return fEdep;
	};

	G4double GetNdep() const
	{
		return fNdep;
	};

	G4double GetStepLength() const
	{
		return fStepLen;
	};

	G4double GetLET(int e) const
	{
		switch(e)
		{
			case 2:
				return fLETcalc;
			case 3:
				return fLETstep;
			default:
				return 0; // todo: exception to handle
		}
	};

	G4ThreeVector GetFirstPos() const
	{
		return fPosFirst;
	};

	G4ThreeVector GetFirstMom() const
	{
		return fMomFirst;
	};

	G4double GetEkin() const
	{
		return fEkin;
	};

	G4String GetProcess() const
	{
		return fProc;
	};

	G4double GetWeight() const
	{
		return fWeight;
	};

	G4int GetZ() const
	{
		return fZ;
	};

	G4int GetA() const
	{
		return fA;
	};

	G4String GetVolume() const
	{
		return fVol;
	};

  private:
	G4bool fIsFirstStep;
	G4bool fIsLastStep;
	G4int fEventID;
	G4int fTrackID;
	G4int fParentID;
	G4String fName;
	G4double fEdep;
	G4double fNdep;
	G4double fStepLen;
	G4double fLETcalc;
	G4double fLETstep;
	G4ThreeVector fPosFirst;
	G4ThreeVector fPosLast;
	G4ThreeVector fMomFirst;
	G4ThreeVector fMomLast;
	G4double fEkin;
	G4String fProc;
	G4double fWeight;
	G4double fWeightPreStep;
	G4double fWeightPostStep;
	G4int fZ;
	G4int fA;
	G4String fVol;
};

typedef G4THitsCollection<ParticleHit> ParticleHitsCollection;

extern G4ThreadLocal G4Allocator<ParticleHit>* ParticleHitAllocator;

inline void* ParticleHit::operator new(size_t)
{
	if(!ParticleHitAllocator)
		ParticleHitAllocator = new G4Allocator<ParticleHit>;
	return (void*)ParticleHitAllocator->MallocSingle();
}

inline void ParticleHit::operator delete(void* hit)
{
	ParticleHitAllocator->FreeSingle((ParticleHit*)hit);
}

#endif
