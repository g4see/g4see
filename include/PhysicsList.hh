// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "G4EmConfigurator.hh"
#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class G4VPhysicsConstructor;
class HadrontherapyStepMax;
class PhysicsListMessenger;

class PhysicsList : public G4VModularPhysicsList
{
  public:
	PhysicsList();
	virtual ~PhysicsList();

	void ConstructParticle();
	void ConstructProcess();

	void SetCuts();
	void SetCutForAllParticles(G4double);
	void SetCutForGamma(G4double);
	void SetCutForElectron(G4double);
	void SetCutForPositron(G4double);
	void SetCutForProton(G4double);

	void AddPhysicsModule(const G4String& name);

	void AddBiasedParticle(G4String particle)
	{
		lastParticleName = particle;
		biasParticleProcesses[particle] = nullVector;
	};

	void AddBiasedProcess(G4String process)
	{
		biasParticleProcesses[lastParticleName].push_back(process);
	};

	void MarkProcessToRemove(const G4String& name);

  private:
	void ListProcesses();
	void RemoveProcesses();

	G4bool helIsRegisted;
	G4bool hinelIsRegisted;
	G4bool ionIsRegisted;

	G4String emName;
	G4VPhysicsConstructor* emPhysicsList;
	G4VPhysicsConstructor* decPhysicsList;
	std::vector<G4VPhysicsConstructor*> hadronPhys;

	std::map<G4String, std::vector<G4String>> particle_processes_to_remove;
	G4bool removeProcesses;

	std::map<G4String, std::vector<G4String>> biasParticleProcesses;
	G4String lastParticleName;
	std::vector<G4String> nullVector;

	PhysicsListMessenger* pMessenger;
};

#endif
