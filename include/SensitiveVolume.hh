// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef SensitiveVolume_h
#define SensitiveVolume_h 1

#include "ParticleGroup.hh"
#include "ParticleHit.hh"
#include "Scoring.hh"
#include "ScoringMessenger.hh"
#include <unordered_set>

#include "G4VSensitiveDetector.hh"

#include <array>
#include <iostream>
#include <map>
#include <sstream>
#include <utility>
#include <vector>

class G4Step;
class G4HCofThisEvent;

class SensitiveVolume : public G4VSensitiveDetector
{
  public:
	SensitiveVolume(const G4String& name, const G4String& hitsCollectionName,
					const G4String& outputDir);
	virtual ~SensitiveVolume();

	// methods from base class
	virtual void Initialize(G4HCofThisEvent* hitCollection);
	virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);
	virtual void EndOfEvent(G4HCofThisEvent* hitCollection);

	void SetHistogramDumping(G4int n)
	{
		nDump = n;
	};

	void SetDSEventLimit(G4int n)
	{
		fDSEventLimit = n;
		fDSMemoryLimit = 10000;
	};

	void SetDSMemoryLimit(G4int m)
	{
		fDSMemoryLimit = m;
		fDSEventLimit = -1;
	};

	void AddScoring(G4String scoringType, G4int histID, G4String hist, G4String particle = "",
					G4String step = "", G4String option = "");

	Scoring* GetLastScoring()
	{
		return lastScoring;
	};

	void SetDetailedScoring(G4bool b)
	{
		fDetailedScoring = b;
	};

	void SetElecGroupingByAncestors(G4bool b)
	{
		fElecGroupAncestor = b;
	};

	void SetPosiGroupingByAncestors(G4bool b)
	{
		fPosiGroupAncestor = b;
	};

	void SetGammGroupingByAncestors(G4bool b)
	{
		fGammGroupAncestor = b;
	};

	void SetPrimaryPrinting(G4bool b)
	{
		fPrintPrimary = b;
	};

	void SetElectronThreshold(G4double d)
	{
		fElectronThreshold = d;
	};

	void SetPositronThreshold(G4double d)
	{
		fPositronThreshold = d;
	};

	void SetGammaThreshold(G4double d)
	{
		fGammaThreshold = d;
	};

	void SetDS_Track(G4bool b)
	{
		fDS["Track"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Parent(G4bool b)
	{
		fDS["Parent"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Process(G4bool b)
	{
		fDS["Process"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Counts(G4bool b)
	{
		fDS["Counts"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Position(G4bool b)
	{
		fDS["Position"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Momentum(G4bool b)
	{
		fDS["Momentum"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Edep(G4bool b)
	{
		fDS["Edep"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Ndep(G4bool b)
	{
		fDS["Ndep"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Ekin(G4bool b)
	{
		fDS["Ekin"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Z(G4bool b)
	{
		fDS["Z"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_A(G4bool b)
	{
		fDS["A"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Volume(G4bool b)
	{
		fDS["Volume"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetDS_Eexc(G4bool b)
	{
		fDS["Eexc"] = b;
		if(fDetailedScoring)
			CreateFile();
	};

	void SetCSVFileFormat(G4bool b)
	{
		remove(GetHitsFileName().c_str());
		fCSVformat = b;
		if(fDetailedScoring)
			CreateFile();
	};

	const G4double GetEdepPerEvent()
	{
		return fEdepPerEventWeighted;
	};

	const G4double GetNEdepPerEvent()
	{
		return fNEdepPerEventWeighted;
	};

  private:
	G4String GetHitsFileName();

	void CreateFile();
	void CreateAsciiFileHits(G4String filename);
	void DumpScoringObjsToFile();
	void DumpEventNumberToFile();
	void DumpHitsToFile();

	G4int SearchInGroupTracks(G4int track_to_find, std::vector<ParticleGroup*> group1,
							  std::vector<ParticleGroup*> group2);

	ParticleGroup* SearchInAllGroupTracks(G4int track_to_find,
										  std::vector<ParticleGroup*> all_groups);

	G4String SensitiveVolumeName;

	G4String fOutputDir;
	ParticleHitsCollection* fHitsCollection;

	G4int fTHREAD;

	G4double fEdepPerEvent;
	G4double fNEdepPerEvent;

	G4double fWeightPerEvent;

	G4double fEdepPerEventWeighted;
	G4double fNEdepPerEventWeighted;

	std::map<G4int, G4int> fHitsPerTrackMap;

	G4double fPrimStepLengthPerEvent;
	G4ThreeVector firstPrimPos;
	G4ThreeVector lastPrimPos;

	Scoring* lastScoring;
	std::vector<Scoring*> fEventScoringVector;
	std::vector<Scoring*> fHitScoringVector;
	ScoringMessenger* fScoringMessenger;

	G4bool fDetailedScoring;

	G4bool fElecGroupAncestor;
	G4bool fPosiGroupAncestor;
	G4bool fGammGroupAncestor;

	G4bool fPrintPrimary;

	G4double fElectronThreshold;
	G4double fPositronThreshold;
	G4double fGammaThreshold;

	std::map<G4String, G4bool> fDS;

	G4int nEvents;
	G4int nDump;

	G4int fDSEventLimit; // events
	G4int fDSMemoryLimit; // bytes

	G4bool fCSVformat;
	std::stringstream fStream;

	enum GroupType
	{
		electron,
		positron,
		gamma,
		individual,
		none
	};
};

#endif
