// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef Particle_h
#define Particle_h 1

#include "ParticleHit.hh"

#include "globals.hh"

#include <map>

class Particle
{
  public:
	Particle(const ParticleHit* hit);
	virtual ~Particle();

	void PrintToStream(std::stringstream& ofs, G4bool csv_format, std::map<G4String, G4bool> fDS);

	void AddHit(const ParticleHit* hit);

	G4int GetEventID() const
	{
		return fEventID;
	};

	G4int GetTrackID() const
	{
		return fTrackID;
	};

	G4int GetParentID() const
	{
		return fParentID;
	};

	G4String GetName() const
	{
		return fName;
	};

	G4double GetEdep() const
	{
		return fEdep;
	};

	G4double GetNdep() const
	{
		return fNdep;
	};

	G4ThreeVector GetPos() const
	{
		return fPos;
	};

	G4ThreeVector GetMom() const
	{
		return fMom;
	};

	G4double GetEkin() const
	{
		return fEkin;
	};

	G4String GetProcess() const
	{
		return fProc;
	};

	G4double GetWeight() const
	{
		return fWeight;
	};

	G4int GetZ() const
	{
		return fZ;
	};

	G4int GetA() const
	{
		return fA;
	};

	G4String GetVolume() const
	{
		return fVol;
	};

	G4double GetEexc() const
	{
		return fEexc;
	};

	G4bool isPrimary()
	{
		return isPrimaryParticle;
	};

	G4bool isExcited()
	{
		return hasExcitation;
	};

  private:
	G4String RenameParticles(const G4String ParticleName);
	G4String RenameProcesses(G4String ProcessName);

	void SetDoubleFormat(std::stringstream& out, G4double fVal, int prec, G4double defVal = 0.);

	G4bool isPrimaryParticle;
	G4bool hasExcitation;

	G4int fEventID;
	G4int fTrackID;
	G4int fParentID;
	G4String fName;
	G4double fEdep;
	G4double fLETcalc;
	G4double fLETstep;
	G4double fNdep;
	G4ThreeVector fPos;
	G4ThreeVector fMom;
	G4double fEkin;
	G4String fProc;
	G4double fWeight;
	G4int fZ;
	G4int fA;
	G4String fVol;
	G4double fEexc;

	G4int fCounts;
};

#endif
