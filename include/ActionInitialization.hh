// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ActionInitialization_h
#define ActionInitialization_h 1

#include "G4String.hh"
#include "G4VUserActionInitialization.hh"

class G4GeneralParticleSource;

class ActionInitialization : public G4VUserActionInitialization
{
  public:
	ActionInitialization(const G4String& outputDir);
	virtual ~ActionInitialization();

	virtual void BuildForMaster() const;
	virtual void Build() const;

  private:
	G4String fOutputDir;
};

#endif
