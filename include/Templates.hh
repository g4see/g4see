// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef Templates_h
#define Templates_h 1

// std::vector<class A>
template<class C> void ClearVector(C& cntr)
{
	for(typename C::iterator it = cntr.begin(); it != cntr.end(); ++it)
		delete *it;
	cntr.clear();
}

// std::map<class A, class B>
template<class C> void ClearMap(C& cntr)
{
	for(typename C::iterator it = cntr.begin(); it != cntr.end(); ++it)
		delete it->second;
	cntr.clear();
}

// // std::map<class A, std::vector<class B>>
// template <class C> void ClearMapVector( C & cntr )
// {
//     for ( typename C::iterator it = cntr.begin(); it != cntr.end(); ++it )
//     ClearVector(it->second); cntr.clear();
// }

#endif
