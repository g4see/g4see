// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef Scoring_h
#define Scoring_h 1

#include "globals.hh"

#include <math.h>
#include <string>

class Scoring
{
  public:
	Scoring();
	virtual ~Scoring();

	void AddToHistogram(G4double value, G4double counts);
	void PrintHistogramToFile(G4String folder_path, G4int thread);

	void SetScoring(G4String hist, G4int histID)
	{
		if(hist.compare("Edep") == 0)
		{
			quantity = hist; // "Edep"
			edep_hist = true;
		}
		else if(hist.compare("Ekin") == 0)
		{
			quantity = hist; // "Ekin"
			ekin_hist = true;
		}
		else if(hist.compare("LET") == 0)
		{
			quantity = hist; // "LET"
			let_hist = true;
		}

		std::stringstream ss;
		ss << histID;
		hist_key = hist + '_' + ss.str();
	};

	void SetOption(G4String opt)
	{
		if(opt.compare("Opt1") == 0)
		{
			let_opt = 1;
		}
		else if(opt.compare("Opt2") == 0)
		{
			let_opt = 2;
		}
		else if(opt.compare("Opt3") == 0)
		{
			let_opt = 3;
		}
	};

	void SetParticle(G4String particle)
	{
		selectedParticle = particle;
	};

	void SetStep(G4String step)
	{
		if(step.compare("first") == 0)
		{
			first_step = true;
			last_step = false;
			all_step = false;
		}
		else if(step.compare("last") == 0)
		{
			first_step = false;
			last_step = true;
			all_step = false;
		}
		else if(step.compare("all") == 0)
		{
			first_step = false;
			last_step = false;
			all_step = true;
		}
	};

	void SetHistogram(G4String scale, G4double ll, G4double ul, G4int bin)
	{
		if(scale.compare("log") == 0)
			log = true;
		else if(scale.compare("lin") == 0)
			log = false;
		llim = ll;
		llim_log = log10(ll); // MeV
		ulim = ul;
		ulim_log = log10(ul); // MeV
		bins = bin;
		CreateHistogram();
	};

	void SetHistogramPrecision(G4int val)
	{
		prec = val;
	};

	G4String GetSelectedParticle()
	{
		return selectedParticle;
	};

	G4int GetLETOption()
	{
		return let_opt;
	};

	G4bool IsEdepScoring()
	{
		return edep_hist;
	};

	G4bool IsEkinScoring()
	{
		return ekin_hist;
	};

	G4bool IsLETScoring()
	{
		return let_hist;
	};

	G4bool IsFirstStepScoring()
	{
		return first_step;
	};

	G4bool IsLastStepScoring()
	{
		return last_step;
	};

	G4bool IsAllStepScoring()
	{
		return all_step;
	};

  private:
	void CreateHistogram();

	G4int FindBinLogarithmic(G4double value);
	G4int FindBinLinear(G4double value);
	G4double GetBinLimitLogarithmic(G4int bin);
	G4double GetBinLimitLinear(G4int bin);

	G4String GetHistogramFileName(G4String OutputDir, G4int Thread);

	std::map<G4int, G4double> histogram;
	G4String quantity;
	G4String hist_key;
	G4bool edep_hist;
	G4bool ekin_hist;
	G4bool let_hist;
	G4int let_opt;
	G4bool first_step;
	G4bool last_step;
	G4bool all_step;
	G4bool log;
	G4int bins;
	G4double llim;
	G4double llim_log;
	G4double ulim;
	G4double ulim_log;
	G4double underflow;
	G4double overflow;
	G4int prec;
	G4String selectedParticle;
};

#endif
