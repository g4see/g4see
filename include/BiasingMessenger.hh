// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef BiasingMessenger_h
#define BiasingMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class PhysicsList;
class DeviceConstruction;
class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithAString;
class G4UIcmdWithABool;

class BiasingMessenger : public G4UImessenger
{
  public:
	BiasingMessenger(DeviceConstruction*, PhysicsList*);
	~BiasingMessenger();

	void SetNewValue(G4UIcommand*, G4String);

  private:
	DeviceConstruction* pDeviceConstruction;
	PhysicsList* pPhysicsList;

	G4UIdirectory* biasDir;

	G4UIcmdWithAString* biasParticleCmd;
	G4UIcmdWithAString* biasProcessCmd;
	G4UIcmdWithADouble* biasFactorCmd;
	G4UIcmdWithABool* biasNonPrimCmd;
};

#endif
