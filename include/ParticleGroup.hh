// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ParticleGroup_h
#define ParticleGroup_h 1

#include "Particle.hh"

#include "globals.hh"

#include <map>
#include <vector>

class ParticleGroup
{
  public:
	ParticleGroup(G4int grpTrackID, G4int grpParentID = 0);

	virtual ~ParticleGroup();

	void PrintGroupToStream(std::stringstream& ofs, G4bool csv_format,
							std::map<G4String, G4bool> fDS);

	G4int GetEventID() const
	{
		return fEventID;
	}; // TODO to remove

	G4int GetGrpTrackID() const
	{
		return fTrackID;
	};

	G4int GetGrpParentID() const
	{
		return fParentID;
	}; // returns 0 instead of "nan"

	G4String GetGrpParticleName() const
	{
		return fParticleName;
	};

	G4bool FindTrackID(G4int track_to_find);

	void CreateGroup(std::vector<Particle*> pVector);
	void AddToGroup(std::vector<Particle*> particles_to_add);

  private:
	void SetDoubleFormat(std::stringstream& out, G4double fVal, int prec);

	G4bool isGroupedByAncestors;

	G4int fTrackID;
	G4int fParentID;

	G4int fEventID;
	G4String fName;
	G4int fZ;
	G4int fA;

	G4String fProc;
	G4String fVol;

	G4double fEdep;
	G4double fNdep;
	G4int fCounts;

	G4double fEexc;

	G4String fParticleName;

	std::vector<G4int> fTrackIDVector;
};

#endif
