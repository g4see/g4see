// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
// Contributor: Eva Fialová (CERN)
//
// clang-format on

#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;

class RunAction : public G4UserRunAction
{
  public:
	RunAction(const G4String& outputDir);
	~RunAction();

  public:
	void BeginOfRunAction(const G4Run*);
	void EndOfRunAction(const G4Run*);

	void AccumulateDose(G4double edep, G4double nedep);

  private:
	void CreateFile();

	G4double fDose;
	G4double fNDose;
	G4double fIDose;

	G4int fNumberOfEvents;

	G4int fTHREAD;

	G4String fOutputDir;
};
#endif
