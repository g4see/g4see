// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef MicroElecPhysics_h
#define MicroElecPhysics_h 1

#include "G4EmParticleList.hh"
#include "G4ParticleTypes.hh"
#include "G4ProcessManager.hh"
#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

class MicroElecPhysics : public G4VPhysicsConstructor
{
  public:
	explicit MicroElecPhysics(G4int ver = 1, const G4String& name = "");

	virtual ~MicroElecPhysics();

	virtual void ConstructParticle();
	virtual void ConstructProcess();

  private:
	G4int verbose;
	G4String RegionName;
};

#endif
