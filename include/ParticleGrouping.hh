// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef ParticleGrouping_h
#define ParticleGrouping_h 1

#include "Particle.hh"
#include "ParticleGroup.hh"

#include "globals.hh"

#include <map>
#include <vector>

class ParticleGrouping
{
  public:
	ParticleGrouping(const G4int grpTrackInit);
	virtual ~ParticleGrouping();

	std::vector<ParticleGroup*> GroupParticles(std::vector<Particle*> fParticleVector);

	std::vector<ParticleGroup*> GroupByAncestors(std::vector<Particle*> fParticleVector,
												 std::vector<Particle*> partVector);

	G4bool FindAncestor(Particle* p);

	std::map<G4int, std::vector<Particle*>> GetOrphanParticles()
	{
		return fOrphanParticleMap;
	};

  private:
	void IterateParentTrackMap(std::map<G4int, std::unordered_set<G4int>> parent_map);

	G4bool isGroupedByAncestors;

	G4int fEventID;
	G4String fName;
	G4String fProc;
	G4int fZ;
	G4int fA;
	G4String fVol;

	G4int fParentID;

	G4int gTrackID;

	G4double fEdep;
	G4double fNdep;
	G4int fCounts;

	std::map<G4int, std::unordered_set<G4int>> fParentTrackIDmap;
	std::map<G4int, std::vector<Particle*>> fParentParticleMap;

	std::map<G4int, std::unordered_set<G4int>> fOrphanTrackIDmap;
	std::map<G4int, std::vector<Particle*>> fOrphanParticleMap;
};

#endif
