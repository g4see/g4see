// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#ifndef PhysicsListMessenger_h
#define PhysicsListMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class PhysicsList;
class G4UIdirectory;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;

class PhysicsListMessenger : public G4UImessenger
{
  public:
	PhysicsListMessenger(PhysicsList*);
	~PhysicsListMessenger();

	void SetNewValue(G4UIcommand*, G4String);

  private:
	PhysicsList* pPhysicsList;

	G4UIdirectory* cutDir;
	G4UIdirectory* physDir;
	G4UIcmdWithADoubleAndUnit* gammaCutCmd;
	G4UIcmdWithADoubleAndUnit* electCutCmd;
	G4UIcmdWithADoubleAndUnit* positCutCmd;
	G4UIcmdWithADoubleAndUnit* hadroCutCmd;
	G4UIcmdWithADoubleAndUnit* allCutCmd;
	G4UIcmdWithAString* pListCmd;
	G4UIcmdWithAString* remProcCmd;
};

#endif
