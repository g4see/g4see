// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ParticleGrouping.hh"

#include "ParticleGroup.hh"
#include "Templates.hh"

#include <fstream>
#include <vector>

ParticleGrouping::ParticleGrouping(const G4int grpTrackInit) :
	gTrackID(grpTrackInit)
{}

ParticleGrouping::~ParticleGrouping()
{}

std::vector<ParticleGroup*> ParticleGrouping::GroupParticles(std::vector<Particle*> fParticleVector)
{
	isGroupedByAncestors = false;
	std::vector<ParticleGroup*> grpList;
	ParticleGroup* grp = new ParticleGroup(gTrackID);
	grp->CreateGroup(fParticleVector);
	grpList.push_back(grp);

	return grpList;
}

std::vector<ParticleGroup*>
	ParticleGrouping::GroupByAncestors(std::vector<Particle*> fParticleVector,
									   std::vector<Particle*> potentialParents)
{
	isGroupedByAncestors = true;

	std::unordered_set<G4int> empty_set;
	std::vector<Particle*>::iterator pit;
	for(pit = potentialParents.begin(); pit != potentialParents.end(); ++pit)
	{
		fParentTrackIDmap[(*pit)->GetTrackID()] = empty_set;
	}

	//------------------------------------------
	//   IterateParentTrackMap(fParentTrackIDmap);
	//------------------------------------------

	std::vector<Particle*>::iterator git;
	for(git = fParticleVector.begin(); git != fParticleVector.end(); ++git)
	{
		if(!FindAncestor(*git)) // filling fParentTrackIDmap and fParentParticleMap here
		{
			// ancestor is not yet found - it should not happen:
			G4cerr << "ERROR: particle's ancestor is not found" << G4endl;
			G4cerr << "TrackID: " << (*git)->GetTrackID() << ", ParentID: " << (*git)->GetParentID()
				   << ", Particle: " << (*git)->GetName() << G4endl;
			IterateParentTrackMap(fParentTrackIDmap);

			getchar();
		}
	}

	//------------------------------------------
	//   IterateParentTrackMap(fParentTrackIDmap);
	//------------------------------------------

	std::vector<ParticleGroup*> grpList;
	std::map<G4int, std::vector<Particle*>>::iterator it;
	for(it = fParentParticleMap.begin(); it != fParentParticleMap.end(); it++)
	{
		ParticleGroup* grp = new ParticleGroup(gTrackID--, it->first);
		grp->CreateGroup(it->second);
		grpList.push_back(grp);
	}

	return grpList;
}

G4bool ParticleGrouping::FindAncestor(Particle* p)
{
	G4int ParentID = p->GetParentID();
	G4int TrackID = p->GetTrackID();

	if(fParentTrackIDmap.find(ParentID) !=
	   fParentTrackIDmap.end()) // its parent (with 'parentID') already in map
	{
		fParentTrackIDmap[ParentID].insert(
			TrackID); // add its track to unordered track set belonging to parent
		fParentParticleMap[ParentID].push_back(p);
		return true; // its ancestor is found among individually printed particles
	}
	else
	{
		// check if its parent is secondary or tertiary ... AND which is already in map
		std::map<G4int, std::unordered_set<G4int>>::iterator it;
		for(it = fParentTrackIDmap.begin(); it != fParentTrackIDmap.end(); it++)
		{
			if(it->second.find(ParentID) != it->second.end())
			{
				// its parent found in the unordered track set of secondaries, tertiaries, ...)
				// it->first is its ancestor (grandparent, great grandparent, ...)
				fParentTrackIDmap[it->first].insert(TrackID);
				fParentParticleMap[it->first].push_back(p);
				return true; // its ancestor is found among descendants of an individually printed
							 // particle
			}
		}

		// parent is not found yet, so probably its ancestor will be in another ParticleGroup object
		// later
		fOrphanTrackIDmap[ParentID].insert(
			TrackID); // add its parent and track pair to the orphan particles
		fOrphanParticleMap[ParentID].push_back(p);
		return true; // its ancestor belongs to another group
	}
	return false; // its ancestor is NOT found
}

void ParticleGrouping::IterateParentTrackMap(std::map<G4int, std::unordered_set<G4int>> parent_map)
{
	G4cout << "fParentTrackIDmap: " << G4endl;
	std::map<G4int, std::unordered_set<G4int>>::iterator it;
	for(it = parent_map.begin(); it != parent_map.end(); it++)
	{
		G4cout << it->first << ": ";

		std::unordered_set<G4int> track_set = it->second;
		std::unordered_set<G4int>::iterator it2;
		for(it2 = track_set.begin(); it2 != track_set.end(); it2++)
		{
			G4cout << *it2 << ", ";
		}
		G4cout << G4endl;
	}
}
