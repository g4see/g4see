// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "EventAction.hh"

#include "RunAction.hh"
#include "SensitiveVolume.hh"

#include "G4Event.hh"
#include "G4SDManager.hh"

EventAction::EventAction(RunAction* runAction) :
	G4UserEventAction(),
	eventID(-1),
	fRunAction(runAction)
{}

EventAction::~EventAction()
{}

void EventAction::BeginOfEventAction(const G4Event* event)
{
	eventID = event->GetEventID();
}

void EventAction::EndOfEventAction(const G4Event* event)
{
	SensitiveVolume* sensVol =
		static_cast<SensitiveVolume*>(G4SDManager::GetSDMpointer()->FindSensitiveDetector("SV"));

	fRunAction->AccumulateDose(sensVol->GetEdepPerEvent(), sensVol->GetNEdepPerEvent());
}
