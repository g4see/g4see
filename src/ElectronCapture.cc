// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ElectronCapture.hh"

#include "G4Electron.hh"
#include "G4ParticleDefinition.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"

ElectronCapture::ElectronCapture(const G4String& regName, G4double ekinlim) :
	G4VDiscreteProcess("eCapture", fElectromagnetic),
	kinEnergyThreshold(ekinlim),
	regionName(regName),
	region(0)
{
	if(regName == "" || regName == "world")
	{
		regionName = "DefaultRegionForTheWorld";
	}
	pParticleChange = &fParticleChange;
}

ElectronCapture::~ElectronCapture()
{}

void ElectronCapture::SetKinEnergyLimit(G4double val)
{
	kinEnergyThreshold = val;
	if(verboseLevel > 0)
	{
		G4cout << "### ElectronCapture: Tracking cut E(MeV) = " << kinEnergyThreshold / MeV
			   << G4endl;
	}
}

void ElectronCapture::BuildPhysicsTable(const G4ParticleDefinition&)
{
	region = (G4RegionStore::GetInstance())->GetRegion(regionName);
	if(region && verboseLevel > 0)
	{
		G4cout << "### ElectronCapture: Tracking cut E(MeV) = " << kinEnergyThreshold / MeV
			   << " is assigned to " << regionName << G4endl;
	}
}

G4bool ElectronCapture::IsApplicable(const G4ParticleDefinition&)
{
	return true;
}

G4double ElectronCapture::PostStepGetPhysicalInteractionLength(const G4Track& aTrack, G4double,
															   G4ForceCondition* condition)
{
	// condition is set to "Not Forced"
	*condition = NotForced;

	G4double limit = DBL_MAX;
	if(region)
	{
		if(aTrack.GetVolume()->GetLogicalVolume()->GetRegion() == region &&
		   aTrack.GetKineticEnergy() < kinEnergyThreshold)
		{
			limit = 0.0;
		}
	}
	return limit;
}

G4VParticleChange* ElectronCapture::PostStepDoIt(const G4Track& aTrack, const G4Step&)
{
	pParticleChange->Initialize(aTrack);
	pParticleChange->ProposeTrackStatus(fStopAndKill);
	pParticleChange->ProposeLocalEnergyDeposit(aTrack.GetKineticEnergy());
	fParticleChange.SetProposedKineticEnergy(0.0);
	return pParticleChange;
}

G4double ElectronCapture::GetMeanFreePath(const G4Track&, G4double, G4ForceCondition*)
{
	return DBL_MAX;
}
