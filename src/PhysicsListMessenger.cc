// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "PhysicsListMessenger.hh"

#include "PhysicsList.hh"

#include "G4SystemOfUnits.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIdirectory.hh"

/////////////////////////////////////////////////////////////////////////////
PhysicsListMessenger::PhysicsListMessenger(PhysicsList* pPhys) :
	pPhysicsList(pPhys)
{
	cutDir = new G4UIdirectory("/SEE/cuts/");
	cutDir->SetGuidance("Commands to set production cuts");

	gammaCutCmd = new G4UIcmdWithADoubleAndUnit("/SEE/cuts/gammaCutWorld", this);
	gammaCutCmd->SetGuidance("Set default gamma production range cut.");
	gammaCutCmd->SetParameterName("Gcut", false);
	gammaCutCmd->SetUnitCategory("Length");
	gammaCutCmd->SetRange("Gcut>=0.");
	gammaCutCmd->AvailableForStates(G4State_PreInit);

	electCutCmd = new G4UIcmdWithADoubleAndUnit("/SEE/cuts/electronCutWorld", this);
	electCutCmd->SetGuidance("Set default electron production range cut.");
	electCutCmd->SetParameterName("Ecut", false);
	electCutCmd->SetUnitCategory("Length");
	electCutCmd->SetRange("Ecut>=0.");
	electCutCmd->AvailableForStates(G4State_PreInit);

	positCutCmd = new G4UIcmdWithADoubleAndUnit("/SEE/cuts/positronCutWorld", this);
	positCutCmd->SetGuidance("Set default positron production range cut.");
	positCutCmd->SetParameterName("Pcut", false);
	positCutCmd->SetUnitCategory("Length");
	positCutCmd->SetRange("Pcut>=0.");
	positCutCmd->AvailableForStates(G4State_PreInit);

	hadroCutCmd = new G4UIcmdWithADoubleAndUnit("/SEE/cuts/hadronsCutWorld", this);
	hadroCutCmd->SetGuidance("Set default hadron production range cut.");
	hadroCutCmd->SetParameterName("Hcut", false);
	hadroCutCmd->SetUnitCategory("Length");
	hadroCutCmd->SetRange("Hcut>=0.");
	hadroCutCmd->AvailableForStates(G4State_PreInit);

	allCutCmd = new G4UIcmdWithADoubleAndUnit("/SEE/cuts/allCutsWorld", this);
	allCutCmd->SetGuidance("Set default production range cut for all particles.");
	allCutCmd->SetParameterName("cut", false);
	allCutCmd->SetUnitCategory("Length");
	allCutCmd->SetRange("cut>=0.");
	allCutCmd->AvailableForStates(G4State_PreInit);

	physDir = new G4UIdirectory("/SEE/physics/");
	physDir->SetGuidance("Commands to activate physics models");

	pListCmd = new G4UIcmdWithAString("/SEE/physics/addPhysics", this);
	pListCmd->SetGuidance("Add physics list.");
	pListCmd->SetParameterName("PList", false);
	pListCmd->AvailableForStates(G4State_PreInit);
	pListCmd->SetCandidates("G4EmStandardPhysics "
							"G4EmStandardPhysics_option4 "
							"G4EmStandardPhysics_option3 "
							"G4EmStandardPhysics_option2 "
							"G4EmStandardPhysics_option1 "
							"G4EmLivermorePhysics "
							"G4EmPenelopePhysics "
							"G4MicroElecPhysics "
							"G4DecayPhysics "
							"G4RadioactiveDecayPhysics "
							"G4HadronElasticPhysics "
							"G4HadronElasticPhysicsHP "
							"G4IonElasticPhysics "
							"G4HadronDElasticPhysics "
							"G4HadronHElasticPhysics "
							"G4ThermalNeutrons "
							"G4HadronInelasticQBBC "
							"G4HadronPhysicsFTF_BIC "
							"G4HadronPhysicsFTFP_BERT_ATL "
							"G4HadronPhysicsFTFP_BERT "
							"G4HadronPhysicsFTFP_BERT_HP "
							"G4HadronPhysicsFTFQGSP_BERT "
							"G4HadronPhysicsINCLXX "
							"G4HadronPhysicsQGS_BIC "
							"G4HadronPhysicsQGSP_BERT "
							"G4HadronPhysicsQGSP_BERT_HP "
							"G4HadronPhysicsQGSP_BIC "
							"G4HadronPhysicsQGSP_BIC_HP "
							"G4HadronPhysicsQGSP_FTFP_BERT "
							"G4IonINCLXXPhysics "
							"G4IonPhysics "
							"G4IonQMDPhysics "
							"none ");

	remProcCmd = new G4UIcmdWithAString("/SEE/physics/removeProcess", this);
	remProcCmd->SetGuidance("Remove a process of a particle.");
	remProcCmd->SetParameterName("process", false);
	remProcCmd->AvailableForStates(G4State_PreInit);
}

PhysicsListMessenger::~PhysicsListMessenger()
{
	delete cutDir;
	delete physDir;
	delete gammaCutCmd;
	delete electCutCmd;
	delete positCutCmd;
	delete hadroCutCmd;
	delete allCutCmd;
	delete pListCmd;
	delete remProcCmd;
}

void PhysicsListMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if(command == gammaCutCmd)
	{
		pPhysicsList->SetCutForGamma(gammaCutCmd->GetNewDoubleValue(newValue));
	}

	if(command == electCutCmd)
	{
		pPhysicsList->SetCutForElectron(electCutCmd->GetNewDoubleValue(newValue));
	}

	if(command == positCutCmd)
	{
		pPhysicsList->SetCutForPositron(positCutCmd->GetNewDoubleValue(newValue));
	}

	if(command == hadroCutCmd)
	{
		pPhysicsList->SetCutForProton(hadroCutCmd->GetNewDoubleValue(newValue));
	}

	if(command == allCutCmd)
	{
		pPhysicsList->SetCutForAllParticles(allCutCmd->GetNewDoubleValue(newValue));
	}

	if(command == pListCmd)
	{
		if(newValue.compare("none") != 0)
			pPhysicsList->AddPhysicsModule(newValue);
	}

	if(command == remProcCmd)
	{
		if(newValue.compare("none") != 0)
			pPhysicsList->MarkProcessToRemove(newValue);
	}
}
