// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "PhysicsList.hh"

#include "PhysicsListMessenger.hh"

#include "G4GenericBiasingPhysics.hh"
#include "G4LossTableManager.hh"
#include "G4ParticleTable.hh"
#include "G4PhysListFactory.hh"
#include "G4ProcessManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4VPhysicsConstructor.hh"

// EM physics constructors:
#include "MicroElecPhysics.hh"

#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
// decay physics constructors:
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
// hadron elastic physics constructors:
#include "G4HadronDElasticPhysics.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4HadronHElasticPhysics.hh"
#include "G4IonElasticPhysics.hh"
#include "G4ThermalNeutrons.hh"
// hadron inelastic physics constructors:
#include "G4HadronInelasticQBBC.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4HadronPhysicsFTFP_BERT_ATL.hh"
#include "G4HadronPhysicsFTFP_BERT_HP.hh"
#include "G4HadronPhysicsFTFQGSP_BERT.hh"
#include "G4HadronPhysicsFTF_BIC.hh"
#include "G4HadronPhysicsINCLXX.hh"
#include "G4HadronPhysicsQGSP_BERT.hh"
#include "G4HadronPhysicsQGSP_BERT_HP.hh"
#include "G4HadronPhysicsQGSP_BIC.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronPhysicsQGSP_FTFP_BERT.hh"
#include "G4HadronPhysicsQGS_BIC.hh"
// ion physics constructors:
#include "G4IonINCLXXPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4IonQMDPhysics.hh"

#include <vector>

/////////////////////////////////////////////////////////////////////////////
PhysicsList::PhysicsList() :
	G4VModularPhysicsList()
{
	G4LossTableManager::Instance();

	//	G4double lowEdge = 10 * eV;
	//	G4double highEdge = G4ProductionCutsTable::GetProductionCutsTable()->GetHighEdgeEnergy();
	//	G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(lowEdge, highEdge);

	// Production range cut for all the particles by default
	defaultCutValue = 1. * meter;
	// Production range cut by default
	SetCutValue(1. * meter, "gamma");
	SetCutValue(1. * meter, "e-");
	SetCutValue(1. * meter, "e+");
	SetCutValue(1. * millimeter, "proton");

	helIsRegisted = false;
	hinelIsRegisted = false;
	ionIsRegisted = false;

	pMessenger = new PhysicsListMessenger(this);

	SetVerboseLevel(1);

	// DEFAULT PHYSICS CONSTRUCTORS
	// EM physics
	G4int em_verbosity = 1;
	emPhysicsList = new G4EmStandardPhysics_option4(em_verbosity);

	// Decay physics and all particles
	decPhysicsList = new G4DecayPhysics();

	//  particleToBias = "";
	//  particleWeight = 1.;
	//  particleToBias.clear();
	//  processesToBias.clear();
	//  particleWeight.clear();

	removeProcesses = false;
}

/////////////////////////////////////////////////////////////////////////////
PhysicsList::~PhysicsList()
{
	delete pMessenger;
	delete emPhysicsList;
	delete decPhysicsList;
	for(size_t i = 0; i < hadronPhys.size(); i++)
	{
		delete hadronPhys[i];
	}
}

/////////////////////////////////////////////////////////////////////////////
void PhysicsList::ConstructParticle()
{
	G4cout << "PhysicsList::ConstructParticle" << G4endl;
	// Define all particles at once:
	decPhysicsList->ConstructParticle();
}

/////////////////////////////////////////////////////////////////////////////
void PhysicsList::ConstructProcess()
{
	G4cout << "\nTHE FOLLOWING PHYSICS LISTS HAVE BEEN ACTIVATED:" << G4endl;
	// transportation
	G4cout << "--> Transportation" << G4endl;
	AddTransportation();

	// decay physics list
	G4cout << "--> " << decPhysicsList->GetPhysicsName() << G4endl;
	decPhysicsList->ConstructProcess();

	// electromagnetic physics list
	G4cout << "--> " << emPhysicsList->GetPhysicsName() << G4endl;
	emPhysicsList->ConstructProcess();

	// hadronic physics lists
	for(size_t i = 0; i < hadronPhys.size(); i++)
	{
		G4cout << "--> " << hadronPhys[i]->GetPhysicsName() << G4endl;
		hadronPhys[i]->ConstructProcess();
	}

	// XS BIASING
	if(!biasParticleProcesses.empty())
	{
		G4GenericBiasingPhysics* biasingPhysics = new G4GenericBiasingPhysics();
		G4cout << "--> " << biasingPhysics->GetPhysicsName() << G4endl;
		G4cout << "    Generic cross-section (XS) biasing was requested for process(es):" << G4endl;

		std::map<G4String, std::vector<G4String>>::iterator it;
		for(it = biasParticleProcesses.begin(); it != biasParticleProcesses.end(); ++it)
		{
			G4String particleToBias = it->first;
			std::vector<G4String> processesToBias = it->second;
			biasingPhysics->Bias(particleToBias, processesToBias);
			// biasingPhysics->Bias(particleToBias);     // todo
			for(size_t i = 0; i < processesToBias.size(); i++)
			{
				G4cout << "      -> " << particleToBias << " : " << processesToBias[i] << G4endl;
			}
		}
		biasingPhysics->ConstructProcess();
	}

	if(removeProcesses)
		RemoveProcesses();

	ListProcesses();
}

/////////////////////////////////////////////////////////////////////////////
void PhysicsList::MarkProcessToRemove(const G4String& name)
{
	G4String particle_name = name.substr(0, name.find(","));
	G4String process_name = name.substr(name.find(",") + 1);
	if(process_name.front() == ' ')
		process_name.replace(0, 1, "");

	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
	G4ParticleDefinition* pd = particleTable->FindParticle(particle_name);
	if(pd)
	{
		removeProcesses = true;
		std::map<G4String, std::vector<G4String>>::iterator it =
			particle_processes_to_remove.find(particle_name);
		if(it ==
		   particle_processes_to_remove.end()) // no process has been defined for this particle yet
		{
			std::vector<G4String> to_remove;
			particle_processes_to_remove[particle_name] = to_remove;
		}
		particle_processes_to_remove[particle_name].push_back(process_name);
	}
	else
		G4cout << "Particle does not exist with name: '" << particle_name
			   << "'. Ignore removing process: '" << process_name << "'" << G4endl;
}

/////////////////////////////////////////////////////////////////////////////
void PhysicsList::AddPhysicsModule(const G4String& name)
{
	if(verboseLevel > 1)
	{
		G4cout << "\nPhysicsList::AddPhysicsModule: <" << name << ">" << G4endl;
	}

	/////////////////////////////////////////////////////////////////////////////
	//   ELECTROMAGNETIC MODELS
	/////////////////////////////////////////////////////////////////////////////
	if(name == "G4EmStandardPhysics_option4")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmStandardPhysics_option4();
	}
	else if(name == "G4EmStandardPhysics_option3")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmStandardPhysics_option3();
	}
	else if(name == "G4EmStandardPhysics_option2")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmStandardPhysics_option2();
	}
	else if(name == "G4EmStandardPhysics_option1")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmStandardPhysics_option1();
	}
	else if(name == "G4EmStandardPhysics")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmStandardPhysics();
	}
	else if(name == "G4EmLivermorePhysics")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmLivermorePhysics();
	}
	else if(name == "G4EmPenelopePhysics")
	{
		delete emPhysicsList;
		emPhysicsList = new G4EmPenelopePhysics();
	}
	else if(name == "G4MicroElecPhysics")
	{ // TODO : WE NEED 2 EM PHYSICS IN THIS SPECIAL CASE
		delete emPhysicsList;
		emPhysicsList = new MicroElecPhysics();

		/////////////////////////////////////////////////////////////////////////////
		//   HADRON ELASTIC MODELS
		/////////////////////////////////////////////////////////////////////////////
	}
	else if(name == "G4HadronElasticPhysics" && !helIsRegisted)
	{
		hadronPhys.push_back(new G4HadronElasticPhysics());
		helIsRegisted = true;
	}
	else if(name == "G4HadronElasticPhysicsHP" && !helIsRegisted)
	{ // child of G4HadronElasticPhysics
		hadronPhys.push_back(new G4HadronElasticPhysicsHP());
		helIsRegisted = true;
	}
	else if(name == "G4HadronDElasticPhysics" && !helIsRegisted)
	{ // Diffusive models
		hadronPhys.push_back(new G4HadronDElasticPhysics());
		helIsRegisted = true;
	}
	else if(name == "G4HadronHElasticPhysics" && !helIsRegisted)
	{
		hadronPhys.push_back(new G4HadronHElasticPhysics());
		helIsRegisted = true;
	}
	else if(name == "G4IonElasticPhysics")
	{
		hadronPhys.push_back(new G4IonElasticPhysics());
	}
	else if(name == "G4ThermalNeutrons")
	{
		hadronPhys.push_back(new G4ThermalNeutrons());

		//  } else if (name == "G4HadronElasticPhysicsXS" && !helIsRegisted) {        // SAME as
		//  G4HadronElasticPhysics
		//    hadronPhys.push_back(new G4HadronElasticPhysicsXS());
		////    helIsRegisted = true;
		//  } else if (name == "G4HadronElasticPhysicsPHP" && !helIsRegisted) {       // SAME as
		//  G4HadronElasticPhysicsHP
		//    hadronPhys.push_back(new G4HadronElasticPhysicsPHP());                  // child of
		//    G4HadronElasticPhysics
		////    helIsRegisted = true;
		//  } else if (name == "G4HadronElasticPhysicsLEND" && !helIsRegisted) {      // NO LEND
		//  DATA AVAILABLE
		//    hadronPhys.push_back(new G4HadronElasticPhysicsLEND());                 // child of
		//    G4HadronElasticPhysics
		////    helIsRegisted = true;

		/////////////////////////////////////////////////////////////////////////////
		//   HADRON INELASTIC MODELS
		/////////////////////////////////////////////////////////////////////////////
	}
	else if(name == "G4HadronInelasticQBBC" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronInelasticQBBC());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsFTF_BIC" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsFTF_BIC());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsFTFP_BERT_ATL" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_ATL());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsFTFP_BERT" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsFTFP_BERT_HP" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_HP());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsFTFQGSP_BERT" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsFTFQGSP_BERT());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsINCLXX" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsINCLXX());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGS_BIC" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGS_BIC());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGSP_BERT" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGSP_BERT());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGSP_BERT_HP" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGSP_BERT_HP());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGSP_BIC" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGSP_BIC_HP" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC_HP());
		hinelIsRegisted = true;
	}
	else if(name == "G4HadronPhysicsQGSP_FTFP_BERT" && !hinelIsRegisted)
	{
		hadronPhys.push_back(new G4HadronPhysicsQGSP_FTFP_BERT());
		hinelIsRegisted = true;

		//  } else if (name == "G4HadronPhysicsFTFP_BERT_TRV" && !hinelIsRegisted) {    // Similar
		//  to G4HadronPhysicsFTFP_BERT
		//    hadronPhys.push_back(new G4HadronPhysicsFTFP_BERT_TRV());
		////    hinelIsRegisted = true;
		//  } else if (name == "G4HadronPhysicsQGSP_BIC_AllHP" && !hinelIsRegisted) {    // no HP
		//  data for proton, etc.
		//    hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC_AllHP());
		////    hinelIsRegisted = true;

		/////////////////////////////////////////////////////////////////////////////
		//   ION MODELS
		/////////////////////////////////////////////////////////////////////////////
	}
	else if(name == "G4IonINCLXXPhysics" && !ionIsRegisted)
	{
		hadronPhys.push_back(new G4IonINCLXXPhysics());
		ionIsRegisted = true;
	}
	else if(name == "G4IonPhysics" && !ionIsRegisted)
	{
		hadronPhys.push_back(new G4IonPhysics());
		ionIsRegisted = true;
	}
	else if(name == "G4IonQMDPhysics" && !ionIsRegisted)
	{
		hadronPhys.push_back(new G4IonQMDPhysics());
		ionIsRegisted = true;

		//  } else if (name == "G4IonPhysicsPHP" && !ionIsRegisted) {   // no HP data for deuteron,
		//  etc.
		//    hadronPhys.push_back(new G4IonPhysicsPHP());
		////    ionIsRegisted = true;
		//  } else if (name == "G4IonPhysicsXS" && !ionIsRegisted) {    // Same as G4IonPhysics
		//    hadronPhys.push_back(new G4IonPhysicsXS());
		////    ionIsRegisted = true;
		//  } else if (name == "G4IonBinaryCascadePhysics" && !ionIsRegisted) {   // Same as
		//  G4IonPhysics
		//    hadronPhys.push_back(new G4IonBinaryCascadePhysics());
		////    ionIsRegisted = true;

		/////////////////////////////////////////////////////////////////////////////
		//   DECAY MODELS
		/////////////////////////////////////////////////////////////////////////////
	}
	else if(name == "G4RadioactiveDecayPhysics")
	{
		hadronPhys.push_back(new G4RadioactiveDecayPhysics());
	}
	else
	{
		G4cout << "PhysicsList::AddPhysicsModule:  Adding <" << name << ">"
			   << " is ignored" << G4endl;
	}
}

void PhysicsList::ListProcesses()
{
	G4cout << "\nPhysicsList::ListProcesses: " << G4endl;

	auto particleIterator = GetParticleIterator();
	particleIterator->reset();
	while((*particleIterator)())
	{
		G4ParticleDefinition* particle = particleIterator->value();
		G4ProcessManager* pmanager = particle->GetProcessManager();
		G4ProcessVector* list = pmanager->GetProcessList();

		G4cout << std::setw(20) << particle->GetParticleName() << " :  ";
		for(size_t idx = 0; idx < list->size(); idx++)
		{
			G4String proc_name = ((*list)[idx])->GetProcessName();
			G4cout << proc_name << " ";

			G4ProcessType proc_type = ((*list)[idx])->GetProcessType();
			G4cout << "(" << proc_type << "); ";
		}
		G4cout << G4endl;
	}
}

void PhysicsList::RemoveProcesses()
{
	G4cout << "\nPhysicsList::RemoveProcesses: " << G4endl;
	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

	std::map<G4String, std::vector<G4String>>::iterator it;
	for(it = particle_processes_to_remove.begin(); it != particle_processes_to_remove.end(); ++it)
	{
		G4String particle_name = it->first;
		std::vector<G4String> proc_to_remove = it->second;

		G4ParticleDefinition* pd = particleTable->FindParticle(particle_name);
		G4ProcessManager* pmanager = pd->GetProcessManager();
		G4ProcessVector* list = pmanager->GetProcessList();
		for(size_t idx = 0; idx < list->size(); idx++)
		{
			G4VProcess* proc = (*list)[idx];
			G4String proc_name = proc->GetProcessName();
			if(std::find(proc_to_remove.begin(), proc_to_remove.end(), proc_name) !=
			   proc_to_remove.end())
			{
				G4VProcess* removed_proc = pmanager->RemoveProcess(proc);
				G4cout << "Removed process: " << particle_name << " / "
					   << removed_proc->GetProcessName() << G4endl;
				idx--;
			}
		}
	}
}

void PhysicsList::SetCuts()
{
	if(verboseLevel > 0)
		DumpCutValuesTable();
}

void PhysicsList::SetCutForGamma(G4double cut)
{
	if(verboseLevel > 0)
	{
		G4cout << "PhysicsList::SetCutForGamma:";
		G4cout << "CutLength : " << G4BestUnit(cut, "Length") << G4endl;
	}
	SetParticleCuts(cut, G4Gamma::Gamma());
}

void PhysicsList::SetCutForElectron(G4double cut)
{
	if(verboseLevel > 0)
	{
		G4cout << "PhysicsList::SetCutForElectron:";
		G4cout << "CutLength : " << G4BestUnit(cut, "Length") << G4endl;
	}
	SetParticleCuts(cut, G4Electron::Electron());
}

void PhysicsList::SetCutForPositron(G4double cut)
{
	if(verboseLevel > 0)
	{
		G4cout << "PhysicsList::SetCutForPositron:";
		G4cout << "CutLength : " << G4BestUnit(cut, "Length") << G4endl;
	}
	SetParticleCuts(cut, G4Positron::Positron());
}

void PhysicsList::SetCutForProton(G4double cut)
{
	if(verboseLevel > 0)
	{
		G4cout << "PhysicsList::SetCutForProton:";
		G4cout << "CutLength : " << G4BestUnit(cut, "Length") << G4endl;
	}
	SetParticleCuts(cut, G4Proton::Proton());
}

void PhysicsList::SetCutForAllParticles(G4double cut)
{
	if(verboseLevel > 0)
	{
		G4cout << "PhysicsList::SetCutForAllParticles:";
		G4cout << "CutLength : " << G4BestUnit(cut, "Length") << G4endl;
	}
	defaultCutValue = cut;
	SetCutsWithDefault();
}
