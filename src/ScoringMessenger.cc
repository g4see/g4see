// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
// Contributor: Eva Fialová (CERN)
//
// clang-format on

#include "ScoringMessenger.hh"

#include "SensitiveVolume.hh"

#include "G4SystemOfUnits.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcommand.hh"
#include "G4UIdirectory.hh"
#include "G4UnitsTable.hh"

ScoringMessenger::ScoringMessenger(SensitiveVolume* SV) :
	G4UImessenger(),
	fSensitiveVolume(SV)
{
	new G4UnitDefinition("eV/nm", "eV/nm", "Energy/Length", electronvolt / nanometer);
	new G4UnitDefinition("keV/nm", "keV/nm", "Energy/Length", kiloelectronvolt / nanometer);

	new G4UnitDefinition("eV/um", "eV/um", "Energy/Length", electronvolt / micrometer);
	new G4UnitDefinition("keV/um", "keV/um", "Energy/Length", kiloelectronvolt / micrometer);
	new G4UnitDefinition("MeV/um", "MeV/um", "Energy/Length", megaelectronvolt / micrometer);
	new G4UnitDefinition("GeV/um", "GeV/um", "Energy/Length", gigaelectronvolt / micrometer);

	new G4UnitDefinition("keV/mm", "keV/mm", "Energy/Length", kiloelectronvolt / millimeter);
	new G4UnitDefinition("MeV/mm", "MeV/mm", "Energy/Length",
						 megaelectronvolt / millimeter); // default unit
	new G4UnitDefinition("GeV/mm", "GeV/mm", "Energy/Length", gigaelectronvolt / millimeter);

	new G4UnitDefinition("keV/cm", "keV/cm", "Energy/Length", kiloelectronvolt / centimeter);
	new G4UnitDefinition("MeV/cm", "MeV/cm", "Energy/Length", megaelectronvolt / centimeter);
	new G4UnitDefinition("GeV/cm", "GeV/cm", "Energy/Length", gigaelectronvolt / centimeter);

	fScoreDirectory = new G4UIdirectory("/SEE/scoring/");
	fScoreDirectory->SetGuidance("Scoring and histogram control");
	fStdScoreDirectory = new G4UIdirectory("/SEE/scoring/standard/");
	fStdScoreDirectory->SetGuidance("Standard histogram scoring");

	fEdepScoreCmd = new G4UIcommand("/SEE/scoring/standard/Edep", this);
	fEdepScoreCmd->SetGuidance("Define event-by-event energy deposition (Edep) scoring.");

	G4UIparameter* idEdPrm = new G4UIparameter("id", 'i', false);
	idEdPrm->SetGuidance("Edep histogram ID");
	idEdPrm->SetParameterRange("id>=0");
	fEdepScoreCmd->SetParameter(idEdPrm);

	fEkinScoreCmd = new G4UIcommand("/SEE/scoring/standard/Ekin", this);
	fEkinScoreCmd->SetGuidance("Define particle kinetic energy (Ekin) scoring.");

	G4UIparameter* idEkPrm = new G4UIparameter("id", 'i', false);
	idEkPrm->SetGuidance("Ekin histogram ID");
	idEkPrm->SetParameterRange("id>=0");
	fEkinScoreCmd->SetParameter(idEkPrm);

	G4UIparameter* partEkPrm = new G4UIparameter("particle", 's', true);
	partEkPrm->SetGuidance("Select particle for Ekin scoring.");
	partEkPrm->SetDefaultValue("primary");
	fEkinScoreCmd->SetParameter(partEkPrm);

	G4UIparameter* stepEkPrm = new G4UIparameter("step", 's', true);
	stepEkPrm->SetGuidance("Select step for Ekin scoring.");
	stepEkPrm->SetParameterCandidates("first last all");
	stepEkPrm->SetDefaultValue("first");
	fEkinScoreCmd->SetParameter(stepEkPrm);

	fLETScoreCmd = new G4UIcommand("/SEE/scoring/standard/LET", this);
	fLETScoreCmd->SetGuidance("Define Linear Energy Transfer (LET) scoring.");

	G4UIparameter* idLETPrm = new G4UIparameter("id", 'i', false);
	idLETPrm->SetGuidance("LET histogram ID");
	idLETPrm->SetParameterRange("id>=0");
	fLETScoreCmd->SetParameter(idLETPrm);

	G4UIparameter* optLETPrm = new G4UIparameter("option", 's', true);
	optLETPrm->SetGuidance("Select LET option for LET scoring.");
	optLETPrm->SetParameterCandidates("Opt1 Opt2 Opt3");
	optLETPrm->SetDefaultValue("Opt1");
	fLETScoreCmd->SetParameter(optLETPrm);

	G4UIparameter* partLETPrm = new G4UIparameter("particle", 's', true);
	partLETPrm->SetGuidance("Select particle for LET scoring.");
	partLETPrm->SetDefaultValue("primary");
	fLETScoreCmd->SetParameter(partLETPrm);

	G4UIparameter* stepLETPrm = new G4UIparameter("step", 's', true);
	stepLETPrm->SetGuidance("Select step for LET scoring.");
	stepLETPrm->SetParameterCandidates("first last all");
	stepLETPrm->SetDefaultValue("first");
	fLETScoreCmd->SetParameter(stepLETPrm);

	fEdepScoreCmd->AvailableForStates(G4State_Idle);
	fEkinScoreCmd->AvailableForStates(G4State_Idle);
	fLETScoreCmd->AvailableForStates(G4State_Idle);
	////////

	fHistCmd = new G4UIcommand("/SEE/scoring/setHistogram", this);
	fHistCmd->SetGuidance("Define histogram.");

	G4UIparameter* scalePrm = new G4UIparameter("scale", 's', false);
	scalePrm->SetGuidance("Select histogram x scale: logarithmic or linear.");
	scalePrm->SetParameterCandidates("log lin");
	fHistCmd->SetParameter(scalePrm);
	//
	G4UIparameter* binsPrm = new G4UIparameter("bins", 'i', false);
	binsPrm->SetGuidance("Set number of histogram bins.");
	binsPrm->SetParameterRange("bins>0");
	fHistCmd->SetParameter(binsPrm);
	//
	G4UIparameter* lolimPrm = new G4UIparameter("lolim", 'd', false);
	lolimPrm->SetGuidance("Set lower limit of histogram range.");
	lolimPrm->SetParameterRange("lolim>=0.");
	fHistCmd->SetParameter(lolimPrm);
	//
	G4UIparameter* uplimPrm = new G4UIparameter("uplim", 'd', false);
	uplimPrm->SetGuidance("Set upper limit of histogram range.");
	uplimPrm->SetParameterRange("uplim>0.");
	fHistCmd->SetParameter(uplimPrm);
	//
	G4UIparameter* unitPrm = new G4UIparameter("unit", 's', true);
	unitPrm->SetGuidance("Unit of histogram range limits.");
	unitPrm->SetParameterCandidates("meV eV keV MeV GeV TeV");
	fHistCmd->SetParameter(unitPrm);

	fHistCmd->AvailableForStates(G4State_Idle);
	///////

	fHistPrecCmd = new G4UIcmdWithAnInteger("/SEE/scoring/setPrecision", this);
	fHistPrecCmd->SetGuidance("Set floating point precision of histogram bin and count values.");
	fHistPrecCmd->SetParameterName("Prec", false);
	fHistPrecCmd->SetRange("Prec>0");
	fHistPrecCmd->AvailableForStates(G4State_Idle);

	fHistDumpCmd = new G4UIcmdWithAnInteger("/SEE/scoring/dumpHistogramsAfter", this);
	fHistDumpCmd->SetGuidance("Set number of events after histograms should be printed to files.");
	fHistDumpCmd->SetParameterName("Dump", false);
	fHistDumpCmd->SetRange("Dump>0");
	fHistDumpCmd->AvailableForStates(G4State_Idle);

	/// DETAILED SCORING COMMANDS
	fDSCmd = new G4UIcmdWithABool("/SEE/scoring/detailed", this);
	fDSCmd->SetGuidance("Enable or disable Detailed Scoring.");
	fDSCmd->AvailableForStates(G4State_Idle);

	fDSCsvCmd = new G4UIcmdWithABool("/SEE/scoring/detailed/setCSVFormat", this);
	fDSCsvCmd->SetGuidance("Set CSV file format for Detailed Scoring output file.");
	fDSCsvCmd->AvailableForStates(G4State_Idle);

	fDSEventLimCmd = new G4UIcmdWithAnInteger("/SEE/scoring/detailed/dumpEventLimit", this);
	fDSEventLimCmd->SetGuidance(
		"Set number of events after Detailed Scoring data should be printed to files.");
	fDSEventLimCmd->SetParameterName("DSevents", false);
	fDSEventLimCmd->SetRange("DSevents>0");
	fDSEventLimCmd->AvailableForStates(G4State_Idle);

	fDSMemLimCmd = new G4UIcommand("/SEE/scoring/detailed/dumpMemoryLimit", this);
	fDSMemLimCmd->SetGuidance(
		"Set memory limit in bytes when Detailed Scoring data should be printed to files.");
	G4UIparameter* memValPrm = new G4UIparameter("memval", 'i', false);
	memValPrm->SetGuidance("");
	memValPrm->SetParameterRange("memval>0");
	fDSMemLimCmd->SetParameter(memValPrm);
	G4UIparameter* memUnitPrm = new G4UIparameter("memunit", 's', false);
	memUnitPrm->SetGuidance("");
	memUnitPrm->SetParameterCandidates("B KB MB GB");
	fDSMemLimCmd->SetParameter(memUnitPrm);
	fDSMemLimCmd->AvailableForStates(G4State_Idle);

	fElecThrCmd = new G4UIcmdWithADoubleAndUnit("/SEE/scoring/detailed/e-/setThreshold", this);
	fElecThrCmd->SetGuidance("Set energy threshold for individual electron scoring.");
	fElecThrCmd->SetParameterName("eCut", false);
	fElecThrCmd->SetUnitCategory("Energy");
	fElecThrCmd->SetRange("eCut>=0.");
	fElecThrCmd->AvailableForStates(G4State_Idle);
	fPosiThrCmd = new G4UIcmdWithADoubleAndUnit("/SEE/scoring/detailed/e+/setThreshold", this);
	fPosiThrCmd->SetGuidance("Set energy threshold for individual positron scoring.");
	fPosiThrCmd->SetParameterName("eCut", false);
	fPosiThrCmd->SetUnitCategory("Energy");
	fPosiThrCmd->SetRange("eCut>=0.");
	fPosiThrCmd->AvailableForStates(G4State_Idle);
	fGammThrCmd = new G4UIcmdWithADoubleAndUnit("/SEE/scoring/detailed/gamma/setThreshold", this);
	fGammThrCmd->SetGuidance("Set energy threshold for individual electron scoring.");
	fGammThrCmd->SetParameterName("eCut", false);
	fGammThrCmd->SetUnitCategory("Energy");
	fGammThrCmd->SetRange("eCut>=0.");
	fGammThrCmd->AvailableForStates(G4State_Idle);

	fElecGrpCmd = new G4UIcmdWithABool("/SEE/scoring/detailed/e-/groupByAncestor", this);
	fElecGrpCmd->SetGuidance("Group non-primary electrons based on their ancestor particles.");
	fElecGrpCmd->AvailableForStates(G4State_Idle);
	fPosiGrpCmd = new G4UIcmdWithABool("/SEE/scoring/detailed/e+/groupByAncestor", this);
	fPosiGrpCmd->SetGuidance("Group non-primary positrons based on their ancestor particles.");
	fPosiGrpCmd->AvailableForStates(G4State_Idle);
	fGammGrpCmd = new G4UIcmdWithABool("/SEE/scoring/detailed/gamma/groupByAncestor", this);
	fGammGrpCmd->SetGuidance("Group non-primary gamma photons based on their ancestor particles.");
	fGammGrpCmd->AvailableForStates(G4State_Idle);

	fPrimCmd = new G4UIcmdWithABool("/SEE/scoring/detailed/printPrimary", this);
	fPrimCmd->SetGuidance("Print primary particle data to Hits file.");
	fPrimCmd->AvailableForStates(G4State_Idle);

	fDSCmdTrack = new G4UIcmdWithABool("/SEE/scoring/detailed/addTrack", this);
	fDSCmdTrack->SetGuidance("Print particle Track ID to Hits file.");
	fDSCmdTrack->AvailableForStates(G4State_Idle);
	fDSCmdParent = new G4UIcmdWithABool("/SEE/scoring/detailed/addParent", this);
	fDSCmdParent->SetGuidance("Print particle Parent ID to Hits file.");
	fDSCmdParent->AvailableForStates(G4State_Idle);
	fDSCmdEkin = new G4UIcmdWithABool("/SEE/scoring/detailed/addEkin", this);
	fDSCmdEkin->SetGuidance("Print particle Kinetic energy to Hits file.");
	fDSCmdEkin->AvailableForStates(G4State_Idle);
	fDSCmdPos = new G4UIcmdWithABool("/SEE/scoring/detailed/addPosition", this);
	fDSCmdPos->SetGuidance("Print particle Position to Hits file.");
	fDSCmdPos->AvailableForStates(G4State_Idle);
	fDSCmdMom = new G4UIcmdWithABool("/SEE/scoring/detailed/addMomentum", this);
	fDSCmdMom->SetGuidance("Print particle Momentum to Hits file.");
	fDSCmdMom->AvailableForStates(G4State_Idle);
	fDSCmdProcess = new G4UIcmdWithABool("/SEE/scoring/detailed/addProcess", this);
	fDSCmdProcess->SetGuidance("Print particle Process to Hits file.");
	fDSCmdProcess->AvailableForStates(G4State_Idle);
	fDSCmdEdep = new G4UIcmdWithABool("/SEE/scoring/detailed/addEdep", this);
	fDSCmdEdep->SetGuidance("Print particle Total energy deposited to Hits file.");
	fDSCmdEdep->AvailableForStates(G4State_Idle);
	fDSCmdNdep = new G4UIcmdWithABool("/SEE/scoring/detailed/addNdep", this);
	fDSCmdNdep->SetGuidance("Print particle Non-ionizing energy deposited to Hits file.");
	fDSCmdNdep->AvailableForStates(G4State_Idle);
	fDSCmdCounts = new G4UIcmdWithABool("/SEE/scoring/detailed/addCounts", this);
	fDSCmdCounts->SetGuidance("Print particle Counts to Hits file.");
	fDSCmdCounts->AvailableForStates(G4State_Idle);
	fDSCmdZ = new G4UIcmdWithABool("/SEE/scoring/detailed/addZ", this);
	fDSCmdZ->SetGuidance("Print particle atomic number to Hits file.");
	fDSCmdZ->AvailableForStates(G4State_Idle);
	fDSCmdA = new G4UIcmdWithABool("/SEE/scoring/detailed/addA", this);
	fDSCmdA->SetGuidance("Print particle mass (nucleon) number to Hits file.");
	fDSCmdA->AvailableForStates(G4State_Idle);
	fDSCmdVol = new G4UIcmdWithABool("/SEE/scoring/detailed/addVolume", this);
	fDSCmdVol->SetGuidance("Print volume where particle produced to Hits file.");
	fDSCmdVol->AvailableForStates(G4State_Idle);
	fDSCmdEexc = new G4UIcmdWithABool("/SEE/scoring/detailed/addEexc", this);
	fDSCmdEexc->SetGuidance("Print excitation energy of ions to Hits file.");
	fDSCmdEexc->AvailableForStates(G4State_Idle);
}

ScoringMessenger::~ScoringMessenger()
{
	delete fScoreDirectory;
	delete fStdScoreDirectory;
	delete fEdepScoreCmd;
	delete fEkinScoreCmd;
	delete fLETScoreCmd;
	delete fHistCmd;
	delete fHistPrecCmd;
	delete fHistDumpCmd;

	delete fDSCmd;
	delete fDSEventLimCmd;
	delete fDSMemLimCmd;
	delete fDSCsvCmd;
	delete fElecGrpCmd;
	delete fPosiGrpCmd;
	delete fGammGrpCmd;
	delete fElecThrCmd;
	delete fPosiThrCmd;
	delete fGammThrCmd;
	delete fPrimCmd;
	delete fDSCmdTrack;
	delete fDSCmdParent;
	delete fDSCmdEkin;
	delete fDSCmdPos;
	delete fDSCmdMom;
	delete fDSCmdProcess;
	delete fDSCmdEdep;
	delete fDSCmdNdep;
	delete fDSCmdCounts;
	delete fDSCmdZ;
	delete fDSCmdA;
	delete fDSCmdVol;
	delete fDSCmdEexc;
}

void ScoringMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if(command == fEdepScoreCmd)
	{
		G4int histID;
		std::istringstream is(newValue);
		is >> histID;
		fSensitiveVolume->AddScoring("event", histID, "Edep");
	}

	if(command == fEkinScoreCmd)
	{
		G4int histID;
		G4String scoredParticle;
		G4String scoredStep;
		std::istringstream is(newValue);
		is >> histID >> scoredParticle >> scoredStep;
		fSensitiveVolume->AddScoring("hit", histID, "Ekin", scoredParticle, scoredStep, "");
	}

	if(command == fLETScoreCmd)
	{
		G4int histID;
		G4String scoringOption;
		G4String scoredParticle;
		G4String scoredStep;
		std::istringstream is(newValue);
		is >> histID >> scoringOption >> scoredParticle >> scoredStep;
		if(scoringOption.compare("Opt1") == 0)
		{
			fSensitiveVolume->AddScoring("event", histID, "LET", "", scoredStep, scoringOption);
		}
		else if(scoringOption.compare("Opt2") == 0 || scoringOption.compare("Opt3") == 0)
		{
			fSensitiveVolume->AddScoring("hit", histID, "LET", scoredParticle, scoredStep,
										 scoringOption);
		}
	}

	if(command == fHistCmd)
	{
		G4double lowlim;
		G4double uplim;
		G4String scale;
		G4String unit;
		G4int bins;
		std::istringstream is(newValue);
		is >> scale >> bins >> lowlim >> uplim >> unit;
		Scoring* lastScore = fSensitiveVolume->GetLastScoring();
		if((lastScore->IsEdepScoring() || lastScore->IsEkinScoring()) && !unit.empty())
		{
			lowlim *= G4UIcommand::ValueOf(unit);
			uplim *= G4UIcommand::ValueOf(unit);
		}
		if(scale.compare("log") == 0 && lowlim == 0.)
			G4cerr
				<< "Illegal parameter in macro : LOG histogram binning needs non-zero lower limit!"
				<< G4endl;
		fSensitiveVolume->GetLastScoring()->SetHistogram(scale, lowlim, uplim, bins);
	}

	if(command == fHistPrecCmd)
	{
		fSensitiveVolume->GetLastScoring()->SetHistogramPrecision(
			fHistPrecCmd->GetNewIntValue(newValue));
	}
	if(command == fHistDumpCmd)
	{
		fSensitiveVolume->SetHistogramDumping(fHistDumpCmd->GetNewIntValue(newValue));
	}

	// Detailed Scoring commands below
	if(command == fDSCmd)
	{
		fSensitiveVolume->SetDetailedScoring(fDSCmd->GetNewBoolValue(newValue));
	}

	if(command == fDSCsvCmd)
	{
		fSensitiveVolume->SetCSVFileFormat(fDSCsvCmd->GetNewBoolValue(newValue));
	}
	if(command == fDSEventLimCmd)
	{
		fSensitiveVolume->SetDSEventLimit(fDSEventLimCmd->GetNewIntValue(newValue));
	}
	if(command == fDSMemLimCmd)
	{
		G4int mem_val;
		G4String mem_unit;
		std::istringstream mem(newValue);
		mem >> mem_val >> mem_unit;
		if(mem_unit.compare("B") == 0)
			mem_val *= 1;
		else if(mem_unit.compare("KB") == 0)
			mem_val *= 1000;
		else if(mem_unit.compare("MB") == 0)
			mem_val *= 1000000;
		else if(mem_unit.compare("GB") == 0)
			mem_val *= 1000000000;
		fSensitiveVolume->SetDSMemoryLimit(mem_val);
	}

	if(command == fElecGrpCmd)
	{
		fSensitiveVolume->SetElecGroupingByAncestors(fElecGrpCmd->GetNewBoolValue(newValue));
	}
	if(command == fPosiGrpCmd)
	{
		fSensitiveVolume->SetPosiGroupingByAncestors(fPosiGrpCmd->GetNewBoolValue(newValue));
	}
	if(command == fGammGrpCmd)
	{
		fSensitiveVolume->SetGammGroupingByAncestors(fGammGrpCmd->GetNewBoolValue(newValue));
	}

	if(command == fPrimCmd)
	{
		fSensitiveVolume->SetPrimaryPrinting(fPrimCmd->GetNewBoolValue(newValue));
	}

	if(command == fElecThrCmd)
	{
		fSensitiveVolume->SetElectronThreshold(fElecThrCmd->GetNewDoubleValue(newValue));
	}
	if(command == fPosiThrCmd)
	{
		fSensitiveVolume->SetPositronThreshold(fPosiThrCmd->GetNewDoubleValue(newValue));
	}
	if(command == fGammThrCmd)
	{
		fSensitiveVolume->SetGammaThreshold(fGammThrCmd->GetNewDoubleValue(newValue));
	}

	if(command == fDSCmdTrack)
	{
		fSensitiveVolume->SetDS_Track(fDSCmdTrack->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdParent)
	{
		fSensitiveVolume->SetDS_Parent(fDSCmdParent->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdEkin)
	{
		fSensitiveVolume->SetDS_Ekin(fDSCmdEkin->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdPos)
	{
		fSensitiveVolume->SetDS_Position(fDSCmdPos->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdMom)
	{
		fSensitiveVolume->SetDS_Momentum(fDSCmdMom->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdProcess)
	{
		fSensitiveVolume->SetDS_Process(fDSCmdProcess->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdEdep)
	{
		fSensitiveVolume->SetDS_Edep(fDSCmdEdep->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdNdep)
	{
		fSensitiveVolume->SetDS_Ndep(fDSCmdNdep->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdCounts)
	{
		fSensitiveVolume->SetDS_Counts(fDSCmdCounts->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdZ)
	{
		fSensitiveVolume->SetDS_Z(fDSCmdZ->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdA)
	{
		fSensitiveVolume->SetDS_A(fDSCmdA->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdVol)
	{
		fSensitiveVolume->SetDS_Volume(fDSCmdVol->GetNewBoolValue(newValue));
	}
	if(command == fDSCmdEexc)
	{
		fSensitiveVolume->SetDS_Eexc(fDSCmdEexc->GetNewBoolValue(newValue));
	}
}
