// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ParticleGroup.hh"

#include <sstream>
#include <vector>

ParticleGroup::ParticleGroup(G4int grpTrackID, G4int grpParentID) :
	fTrackID(grpTrackID),
	fParentID(grpParentID)
{
	if(fParentID == 0)
		isGroupedByAncestors = false; // fParentID -> "nan"
	else
		isGroupedByAncestors = true;

	if(fTrackID >= 0)
		G4cerr << "ERROR: Particle group trackID should be a negative integer!" << G4endl;
}

ParticleGroup::~ParticleGroup()
{
	std::vector<G4int>().swap(fTrackIDVector);
}

void ParticleGroup::CreateGroup(std::vector<Particle*> pVector)
{
	fParticleName = pVector.front()->GetName();
	if(isGroupedByAncestors)
		fName = "ga(" + fParticleName + ")";
	else
		fName = "g(" + fParticleName + ")";

	fEventID = pVector.front()->GetEventID();
	fZ = pVector.front()->GetZ();
	fA = pVector.front()->GetA();

	fProc = pVector.front()->GetProcess();
	fVol = pVector.front()->GetVolume();

	fEexc = 0.;

	fEdep = 0.;
	fNdep = 0.;
	fCounts = 0;

	AddToGroup(pVector);
}

void ParticleGroup::AddToGroup(std::vector<Particle*> particles_to_add)
{
	std::vector<Particle*>::iterator pit;
	for(pit = particles_to_add.begin(); pit != particles_to_add.end(); ++pit)
	{
		fEdep += (*pit)->GetEdep();
		fNdep += (*pit)->GetNdep();
		fCounts += 1;

		// this may change particle by particle, so only print value to file if all particles have
		// the same
		if(fProc.compare((*pit)->GetProcess()) != 0)
			fProc = "nan";
		if(fVol.compare((*pit)->GetVolume()) != 0)
			fVol = "nan";

		fTrackIDVector.push_back((*pit)->GetTrackID());
	}
}

G4bool ParticleGroup::FindTrackID(G4int track)
{
	return std::find(fTrackIDVector.begin(), fTrackIDVector.end(), track) != fTrackIDVector.end();
}

void ParticleGroup::PrintGroupToStream(std::stringstream& ofs, G4bool csv_format,
									   std::map<G4String, G4bool> fDS)
{
	int width = 12;
	int width_s = 8;
	int width_l = 18;
	int width_e = 24;
	G4String sep = "";
	if(csv_format)
	{
		width = 0;
		width_s = 0;
		width_l = 0;
		width_e = 0;
		sep = ",";
	}

	// compulsory
	ofs << std::fixed;
	ofs << std::setw(width_s) << fEventID << sep;
	ofs << std::setw(width_l) << fName << sep;
	ofs << std::setw(width) << "nan" << sep; // fWeight
	// optional
	if(fDS["Z"])
	{
		ofs << std::setw(width_s) << fZ << sep;
	}
	if(fDS["A"])
	{
		ofs << std::setw(width_s) << fA << sep;
	}
	if(fDS["Track"])
	{
		ofs << std::setw(width) << fTrackID << sep;
	}
	if(fDS["Parent"])
	{
		if(isGroupedByAncestors)
			ofs << std::setw(width) << fParentID << sep;
		else
			ofs << std::setw(width) << "nan" << sep;
	}
	if(fDS["Ekin"])
	{
		ofs << std::setw(width) << "nan" << sep;
	}
	if(fDS["Position"])
	{
		ofs << std::setw(width) << "nan" << sep << std::setw(width) << "nan" << sep
			<< std::setw(width) << "nan" << sep;
	}
	if(fDS["Momentum"])
	{
		ofs << std::setw(width) << "nan" << sep << std::setw(width) << "nan" << sep
			<< std::setw(width) << "nan" << sep;
	}
	if(fDS["Process"])
	{
		ofs << std::setw(width_e) << fProc << sep;
	}
	if(fDS["Volume"])
	{
		ofs << std::setw(width_l) << fVol << sep;
	}
	if(fDS["Edep"])
	{
		SetDoubleFormat(ofs, fEdep, 4);
		ofs << std::setw(width) << fEdep << sep;
	}
	if(fDS["Ndep"])
	{
		SetDoubleFormat(ofs, fNdep, 4);
		ofs << std::setw(width) << fNdep << sep;
	}
	if(fDS["Counts"])
	{
		ofs << std::setw(width_s) << fCounts << sep;
	}
	if(fDS["Eexc"])
	{
		SetDoubleFormat(ofs, fEexc, 3);
		ofs << std::setw(width) << fEexc << sep;
	}

	ofs << std::endl;
}

void ParticleGroup::SetDoubleFormat(std::stringstream& out, G4double fVal, int prec)
{
	if(fVal == 0.)
	{
		out << std::fixed;
		out << std::setprecision(0);
	}
	else
	{
		out << std::scientific;
		out << std::setprecision(prec);
	}
}
