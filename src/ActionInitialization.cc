// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ActionInitialization.hh"

#include "EventAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "SteppingAction.hh"
#include "TrackingAction.hh"

#include "G4GeneralParticleSource.hh"
#include "G4RunManager.hh"

ActionInitialization::ActionInitialization(const G4String& outputDir) :
	G4VUserActionInitialization(),
	fOutputDir(outputDir)
{}

ActionInitialization::~ActionInitialization()
{}

void ActionInitialization::BuildForMaster() const
{
	RunAction* runAction = new RunAction(fOutputDir);
	SetUserAction(runAction);

	// In MT mode, to be clearer, the RunAction class for the master thread might be
	// different than the one used for the workers.
	// This RunAction will be called before and after starting the
	// workers.
}

void ActionInitialization::Build() const
{
	// Initialize the primary particles
	PrimaryGeneratorAction* primary = new PrimaryGeneratorAction();
	SetUserAction(primary);

	RunAction* run = new RunAction(fOutputDir);

	SetUserAction(run);

	EventAction* event = new EventAction(run);
	SetUserAction(event);

	TrackingAction* track = new TrackingAction(event);
	SetUserAction(track);

	SteppingAction* stepping = new SteppingAction();
	SetUserAction(stepping);
}
