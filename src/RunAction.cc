// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
// Contributor: Eva Fialová (CERN)
//
// clang-format on

#include "RunAction.hh"

#include "DeviceConstruction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4ios.hh"

#include <fstream>
#include <iostream>

RunAction::RunAction(const G4String& outputDir) :
	G4UserRunAction(),
	fOutputDir(outputDir)
{
	// add new units for dose
	new G4UnitDefinition("megagray", "MGy", "Dose", 1.e+6 * gray);
	new G4UnitDefinition("kilogray", "kGy", "Dose", 1.e+3 * gray);
	new G4UnitDefinition("milligray", "mGy", "Dose", 1.e-3 * gray);
	new G4UnitDefinition("microgray", "uGy", "Dose", 1.e-6 * gray);
	new G4UnitDefinition("nanogray", "nGy", "Dose", 1.e-9 * gray);
	new G4UnitDefinition("picogray", "pGy", "Dose", 1.e-12 * gray);

#if defined(G4MULTITHREADED) && defined(APP_MULTITHREADED)
	fTHREAD = G4Threading::G4GetThreadId();
#else
	fTHREAD = 0;
#endif
}

RunAction::~RunAction()
{}

void RunAction::BeginOfRunAction(const G4Run* aRun)
{
	G4int run_number = aRun->GetRunID();
	G4cout << "### Run " << run_number << " start." << G4endl;

	fDose = 0.;
	fNDose = 0.;
	fIDose = 0.;

	fNumberOfEvents = 0;
}

void RunAction::EndOfRunAction(const G4Run* aRun)
{
	fNumberOfEvents = aRun->GetNumberOfEvent();
	G4cout << "Number of events = " << fNumberOfEvents << G4endl;

	const DeviceConstruction* deviceConstruct = static_cast<const DeviceConstruction*>(
		G4RunManager::GetRunManager()->GetUserDetectorConstruction());
	const G4double mass = deviceConstruct->GetScoringVolume()->GetMass();

	fDose /= mass;
	fNDose /= mass;
	fIDose = fDose - fNDose;

	CreateFile();
}

void RunAction::AccumulateDose(G4double edep, G4double nedep)
{
	fDose += edep;
	fNDose += nedep;
}

void RunAction::CreateFile()
{
	if(fTHREAD > -1)
	{
		std::ostringstream os;
		G4String file_name = fOutputDir + "/Dose_t";
		os << fTHREAD;
		file_name.append(os.str());
		file_name.append(".out");

		std::ofstream ofs;
		ofs.open(file_name, std::ofstream::out);
		if(ofs.is_open())
		{
			ofs << "Doses deposited in Sensitive Volume (SV) by " << fNumberOfEvents << " events"
				<< std::endl
				<< "-------------------------------------------------------------------------"
				<< std::endl;
			ofs << "  Total Dose:                   " << G4BestUnit(fDose, "Dose") << std::endl;
			ofs << "  -> Total Ionizing Dose:       " << G4BestUnit(fIDose, "Dose") << std::endl;
			ofs << "  -> Total Non-Ionizing Dose:   " << G4BestUnit(fNDose, "Dose") << std::endl;
			ofs.close();
		}
	}
}
