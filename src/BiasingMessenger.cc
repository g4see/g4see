// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "BiasingMessenger.hh"

#include "DeviceConstruction.hh"
#include "PhysicsList.hh"

#include "G4ParticleTable.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIdirectory.hh"

/////////////////////////////////////////////////////////////////////////////
BiasingMessenger::BiasingMessenger(DeviceConstruction* pDet, PhysicsList* pPhys) :
	pDeviceConstruction(pDet),
	pPhysicsList(pPhys)
{
	biasDir = new G4UIdirectory("/SEE/biasing/");
	biasDir->SetGuidance("Commands to activate biasing");

	biasParticleCmd = new G4UIcmdWithAString("/SEE/biasing/biasParticle", this);
	biasParticleCmd->SetGuidance("Define particle to bias.");
	biasParticleCmd->AvailableForStates(G4State_PreInit);
	G4String candidateList;
	G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
	for(G4int i = 0; i < particleTable->entries(); i++)
	{
		candidateList += particleTable->GetParticleName(i);
		candidateList += " ";
	}
	candidateList += "ion ";
	biasParticleCmd->SetCandidates(candidateList);

	biasProcessCmd = new G4UIcmdWithAString("/SEE/biasing/biasProcess", this);
	biasProcessCmd->SetGuidance("Define process of particle to bias.");
	biasProcessCmd->AvailableForStates(G4State_PreInit);

	biasFactorCmd = new G4UIcmdWithADouble("/SEE/biasing/biasFactor", this);
	biasFactorCmd->SetGuidance("Set biasing factor for target volume.");
	biasFactorCmd->AvailableForStates(G4State_PreInit);

	biasNonPrimCmd = new G4UIcmdWithABool("/SEE/biasing/biasNonPrimariesToo", this);
	biasNonPrimCmd->SetGuidance("Biasing also non-primary particles.");
	biasNonPrimCmd->AvailableForStates(G4State_PreInit);
}

BiasingMessenger::~BiasingMessenger()
{
	delete biasDir;
	delete biasParticleCmd;
	delete biasProcessCmd;
	delete biasFactorCmd;
	delete biasNonPrimCmd;
}

void BiasingMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if(command == biasParticleCmd)
	{
		pDeviceConstruction->AddBiasedParticle(newValue);
		pPhysicsList->AddBiasedParticle(newValue);
	}
	if(command == biasProcessCmd)
	{
		pPhysicsList->AddBiasedProcess(newValue);
	}
	if(command == biasFactorCmd)
	{
		pDeviceConstruction->AddBiasWeight(biasFactorCmd->GetNewDoubleValue(newValue));
	}
	if(command == biasNonPrimCmd)
	{
		pDeviceConstruction->SetBiasForNonPrimaryParticles(
			biasNonPrimCmd->GetNewBoolValue(newValue));
	}
}
