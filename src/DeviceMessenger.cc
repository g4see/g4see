// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "DeviceMessenger.hh"

#include "DeviceConstruction.hh"

#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcommand.hh"
#include "G4UIdirectory.hh"

DeviceMessenger::DeviceMessenger(DeviceConstruction* Det) :
	G4UImessenger(),
	fDeviceConstruction(Det)
{
	fMatDirectory = new G4UIdirectory("/SEE/material/");
	fMatDirectory->SetGuidance("Material construction control");

	G4String unitListDens = G4UIcommand::UnitsList(G4UIcommand::CategoryOf("g/cm3"));

	fElemCmd = new G4UIcommand("/SEE/material/addElement", this);

	G4UIparameter* ElemName = new G4UIparameter("name", 's', false);
	ElemName->SetGuidance("Element name");
	fElemCmd->SetParameter(ElemName);

	G4UIparameter* ElemSymb = new G4UIparameter("symbol", 's', false);
	ElemSymb->SetGuidance("Element chemical symbol");
	fElemCmd->SetParameter(ElemSymb);

	G4UIparameter* ElemIsot = new G4UIparameter("isotopes", 's', false);
	ElemIsot->SetGuidance("Element isotope nucleon number and abundance pairs");
	fElemCmd->SetParameter(ElemIsot);

	fElemCmd->AvailableForStates(G4State_PreInit);
	fElemCmd->SetToBeBroadcasted(false); // not send to worker threads
	///
	fMatCmd = new G4UIcommand("/SEE/material/addMaterial", this);

	G4UIparameter* MatName = new G4UIparameter("name", 's', false);
	MatName->SetGuidance("Material name");
	fMatCmd->SetParameter(MatName);

	G4UIparameter* MatDens = new G4UIparameter("density", 'd', false);
	MatDens->SetGuidance("Material density");
	fMatCmd->SetParameter(MatDens);

	G4UIparameter* MatUnit = new G4UIparameter("unit", 's', false);
	MatUnit->SetGuidance("Material density unit");
	MatUnit->SetParameterCandidates(unitListDens);
	fMatCmd->SetParameter(MatUnit);

	G4UIparameter* MatElem = new G4UIparameter("elements", 's', false);
	MatElem->SetGuidance("Material element and stoichiometric ratio pairs");
	fMatCmd->SetParameter(MatElem);

	fMatCmd->AvailableForStates(G4State_PreInit);
	fMatCmd->SetToBeBroadcasted(false); // not send to worker threads
	///
	fMixCmd = new G4UIcommand("/SEE/material/addMixture", this);

	G4UIparameter* MixName = new G4UIparameter("name", 's', false);
	MixName->SetGuidance("Mixture name");
	fMixCmd->SetParameter(MixName);

	G4UIparameter* MixDens = new G4UIparameter("density", 'd', false);
	MixDens->SetGuidance("Mixture density");
	fMixCmd->SetParameter(MixDens);

	G4UIparameter* MixUnit = new G4UIparameter("unit", 's', false);
	MixUnit->SetGuidance("Mixture density unit");
	MixUnit->SetParameterCandidates(unitListDens);
	fMixCmd->SetParameter(MixUnit);

	G4UIparameter* MixMate = new G4UIparameter("materials", 's', false);
	MixMate->SetGuidance("Mixture material and ratio pairs");
	fMixCmd->SetParameter(MixMate);

	fMixCmd->AvailableForStates(G4State_PreInit);
	fMixCmd->SetToBeBroadcasted(false); // not send to worker threads
	///

	fDetDirectory = new G4UIdirectory("/SEE/geometry/");
	fDetDirectory->SetGuidance("Geometry construction control");

	G4String unitListLength = G4UIcommand::UnitsList(G4UIcommand::CategoryOf("mm"));

	///// BULK VOLUME
	fBulkCmd = new G4UIcommand("/SEE/geometry/Bulk", this);

	G4UIparameter* MatPrmBulk = new G4UIparameter("material", 's', false);
	MatPrmBulk->SetGuidance("Bulk material name");
	fBulkCmd->SetParameter(MatPrmBulk);
	//
	G4UIparameter* WidthPrmBulk = new G4UIparameter("width", 'd', false);
	WidthPrmBulk->SetGuidance("width of Bulk");
	WidthPrmBulk->SetParameterRange("width>0.");
	fBulkCmd->SetParameter(WidthPrmBulk);
	//
	G4UIparameter* unit1PrmBulk = new G4UIparameter("unit1", 's', false);
	unit1PrmBulk->SetGuidance("unit of width");
	unit1PrmBulk->SetParameterCandidates(unitListLength);
	fBulkCmd->SetParameter(unit1PrmBulk);
	//
	G4UIparameter* ThickPrmBulk = new G4UIparameter("thickness", 'd', false);
	ThickPrmBulk->SetGuidance("thickness of Bulk");
	ThickPrmBulk->SetParameterRange("thickness>0.");
	fBulkCmd->SetParameter(ThickPrmBulk);
	//
	G4UIparameter* unit2PrmBulk = new G4UIparameter("unit2", 's', false);
	unit2PrmBulk->SetGuidance("unit of thickness");
	unit2PrmBulk->SetParameterCandidates(unitListLength);
	fBulkCmd->SetParameter(unit2PrmBulk);
	//
	G4UIparameter* BiasPrmBulk = new G4UIparameter("bias", 'b', true);
	BiasPrmBulk->SetGuidance("activate biasing for Bulk");
	BiasPrmBulk->SetDefaultValue(false);
	fBulkCmd->SetParameter(BiasPrmBulk);
	//
	fBulkCmd->AvailableForStates(G4State_PreInit);
	fBulkCmd->SetToBeBroadcasted(false);

	///// SENSITIVE VOLUME
	fSensVolCmd = new G4UIcommand("/SEE/geometry/SV", this);
	//
	G4UIparameter* PosXPrmSV = new G4UIparameter("pos_x", 'd', false);
	G4UIparameter* PosYPrmSV = new G4UIparameter("pos_y", 'd', false);
	G4UIparameter* PosZPrmSV = new G4UIparameter("pos_z", 'd', false);
	PosXPrmSV->SetGuidance("X position of Sensitive Volume");
	PosYPrmSV->SetGuidance("Y position of Sensitive Volume");
	PosZPrmSV->SetGuidance("Z position of Sensitive Volume");
	fSensVolCmd->SetParameter(PosXPrmSV);
	fSensVolCmd->SetParameter(PosYPrmSV);
	fSensVolCmd->SetParameter(PosZPrmSV);
	G4UIparameter* unit0PrmSV = new G4UIparameter("unit0", 's', false);
	unit0PrmSV->SetGuidance("unit of position");
	unit0PrmSV->SetParameterCandidates(unitListLength);
	fSensVolCmd->SetParameter(unit0PrmSV);
	//
	G4UIparameter* XwidthPrmSV = new G4UIparameter("Xwidth", 'd', false);
	XwidthPrmSV->SetGuidance("width of Sensitive Volume along X axis");
	XwidthPrmSV->SetParameterRange("Xwidth>0.");
	fSensVolCmd->SetParameter(XwidthPrmSV);
	G4UIparameter* YwidthPrmSV = new G4UIparameter("Ywidth", 'd', false);
	YwidthPrmSV->SetGuidance("width of Sensitive Volume along Y axis");
	YwidthPrmSV->SetParameterRange("Ywidth>0.");
	fSensVolCmd->SetParameter(YwidthPrmSV);
	//
	G4UIparameter* unit1PrmSV = new G4UIparameter("unit1", 's', false);
	unit1PrmSV->SetGuidance("unit of width");
	unit1PrmSV->SetParameterCandidates(unitListLength);
	fSensVolCmd->SetParameter(unit1PrmSV);
	//
	G4UIparameter* ThickPrmSV = new G4UIparameter("thickness", 'd', false);
	ThickPrmSV->SetGuidance("thickness of Sensitive Volume");
	ThickPrmSV->SetParameterRange("thickness>0.");
	fSensVolCmd->SetParameter(ThickPrmSV);
	//
	G4UIparameter* unit2PrmSV = new G4UIparameter("unit2", 's', false);
	unit2PrmSV->SetGuidance("unit of thickness");
	unit2PrmSV->SetParameterCandidates(unitListLength);
	fSensVolCmd->SetParameter(unit2PrmSV);
	//
	G4UIparameter* BiasPrmSV = new G4UIparameter("bias", 'b', true);
	BiasPrmSV->SetGuidance("activate biasing for Sensitive Volume");
	BiasPrmSV->SetDefaultValue(false);
	fSensVolCmd->SetParameter(BiasPrmSV);
	//
	fSensVolCmd->AvailableForStates(G4State_PreInit);
	fSensVolCmd->SetToBeBroadcasted(false);

	///// BEOL LAYERS
	fBEOLCmd = new G4UIcommand("/SEE/geometry/BEOL/addLayer", this);

	G4UIparameter* MatPrm = new G4UIparameter("material", 's', false);
	MatPrm->SetGuidance("material name");
	fBEOLCmd->SetParameter(MatPrm);
	//
	G4UIparameter* WidthPrm = new G4UIparameter("width", 'd', false);
	WidthPrm->SetGuidance("width of layer");
	WidthPrm->SetParameterRange("width>0.");
	fBEOLCmd->SetParameter(WidthPrm);
	//
	G4UIparameter* unit1Prm = new G4UIparameter("unit1", 's', false);
	unit1Prm->SetGuidance("unit of width");
	unit1Prm->SetParameterCandidates(unitListLength);
	fBEOLCmd->SetParameter(unit1Prm);
	//
	G4UIparameter* ThickPrm = new G4UIparameter("thickness", 'd', false);
	ThickPrm->SetGuidance("thickness of layer");
	ThickPrm->SetParameterRange("thickness>=0.");
	fBEOLCmd->SetParameter(ThickPrm);
	//
	G4UIparameter* unit2Prm = new G4UIparameter("unit2", 's', false);
	unit2Prm->SetGuidance("unit of thickness");
	unit2Prm->SetParameterCandidates(unitListLength);
	fBEOLCmd->SetParameter(unit2Prm);
	//
	G4UIparameter* NamePrm = new G4UIparameter("name", 's', false);
	NamePrm->SetGuidance("layer name");
	fBEOLCmd->SetParameter(NamePrm);
	//
	G4UIparameter* BiasPrm = new G4UIparameter("bias", 'b', true);
	BiasPrm->SetGuidance("activate biasing for layer");
	BiasPrm->SetDefaultValue(false);
	fBEOLCmd->SetParameter(BiasPrm);
	//
	fBEOLCmd->AvailableForStates(G4State_PreInit);
	fBEOLCmd->SetToBeBroadcasted(false);

	fCylCmd = new G4UIcmdWithABool("/SEE/geometry/setCylindricalGeometry", this);
	fCylCmd->SetGuidance("Use cylindrical volumes instead of default cuboid volumes.");
	fCylCmd->AvailableForStates(G4State_PreInit);
	fCylCmd->SetToBeBroadcasted(false);

	fWorldMatCmd = new G4UIcmdWithAString("/SEE/geometry/setWorldMaterial", this);
	fWorldMatCmd->SetGuidance("Set material of World volume.");
	fWorldMatCmd->SetDefaultValue("VACUUM");
	fWorldMatCmd->AvailableForStates(G4State_PreInit);
	fWorldMatCmd->SetToBeBroadcasted(false);

	fWorldSVCmd = new G4UIcmdWithABool("/SEE/geometry/setWorldAsMotherOfSV", this);
	fWorldSVCmd->SetGuidance("Use World volume as mother of SV, instead of Bulk volume.");
	fWorldSVCmd->AvailableForStates(G4State_PreInit);
	fWorldSVCmd->SetToBeBroadcasted(false);

	cutsDir = new G4UIdirectory("/SEE/cuts/");
	cutsDir->SetGuidance("Commands to set production range cuts for regions.");

	gammaCutCmd = new G4UIcommand("/SEE/cuts/gamma", this);
	gammaCutCmd->SetGuidance("Set gamma production range cut for a region.");
	gammaCutCmd->AvailableForStates(G4State_PreInit);
	G4UIparameter* gRegPrm = new G4UIparameter("region", 's', false);
	gRegPrm->SetGuidance("Select region");
	gRegPrm->SetParameterCandidates("SV Bulk BEOL");
	gammaCutCmd->SetParameter(gRegPrm);
	G4UIparameter* gCutPrm = new G4UIparameter("cut", 'd', false);
	gCutPrm->SetGuidance("Set cut value");
	gCutPrm->SetParameterRange("cut>=0.");
	gammaCutCmd->SetParameter(gCutPrm);
	G4UIparameter* gUnitPrm = new G4UIparameter("unit", 's', false);
	gUnitPrm->SetGuidance("Set range unit");
	gUnitPrm->SetParameterCandidates(unitListLength);
	gammaCutCmd->SetParameter(gUnitPrm);

	electCutCmd = new G4UIcommand("/SEE/cuts/electron", this);
	electCutCmd->SetGuidance("Set electron production range cut for a region.");
	electCutCmd->AvailableForStates(G4State_PreInit);
	G4UIparameter* eRegPrm = new G4UIparameter("region", 's', false);
	eRegPrm->SetGuidance("Select region");
	eRegPrm->SetParameterCandidates("SV Bulk BEOL");
	electCutCmd->SetParameter(eRegPrm);
	G4UIparameter* eCutPrm = new G4UIparameter("cut", 'd', false);
	eCutPrm->SetGuidance("Set cut value");
	eCutPrm->SetParameterRange("cut>=0.");
	electCutCmd->SetParameter(eCutPrm);
	G4UIparameter* eUnitPrm = new G4UIparameter("unit", 's', false);
	eUnitPrm->SetGuidance("Set range unit");
	eUnitPrm->SetParameterCandidates(unitListLength);
	electCutCmd->SetParameter(eUnitPrm);

	positCutCmd = new G4UIcommand("/SEE/cuts/positron", this);
	positCutCmd->SetGuidance("Set positron production range cut for a region.");
	positCutCmd->AvailableForStates(G4State_PreInit);
	G4UIparameter* pRegPrm = new G4UIparameter("region", 's', false);
	pRegPrm->SetGuidance("Select region");
	pRegPrm->SetParameterCandidates("SV Bulk BEOL");
	positCutCmd->SetParameter(pRegPrm);
	G4UIparameter* pCutPrm = new G4UIparameter("cut", 'd', false);
	pCutPrm->SetGuidance("Set cut value");
	pCutPrm->SetParameterRange("cut>=0.");
	positCutCmd->SetParameter(pCutPrm);
	G4UIparameter* pUnitPrm = new G4UIparameter("unit", 's', false);
	pUnitPrm->SetGuidance("Set range unit");
	pUnitPrm->SetParameterCandidates(unitListLength);
	positCutCmd->SetParameter(pUnitPrm);

	hadroCutCmd = new G4UIcommand("/SEE/cuts/hadrons", this);
	hadroCutCmd->SetGuidance("Set hadron production range cut for a region.");
	hadroCutCmd->AvailableForStates(G4State_PreInit);
	G4UIparameter* hRegPrm = new G4UIparameter("region", 's', false);
	hRegPrm->SetGuidance("Select region");
	hRegPrm->SetParameterCandidates("SV Bulk BEOL");
	hadroCutCmd->SetParameter(hRegPrm);
	G4UIparameter* hCutPrm = new G4UIparameter("cut", 'd', false);
	hCutPrm->SetGuidance("Set cut value");
	hCutPrm->SetParameterRange("cut>=0.");
	hadroCutCmd->SetParameter(hCutPrm);
	G4UIparameter* hUnitPrm = new G4UIparameter("unit", 's', false);
	hUnitPrm->SetGuidance("Set range unit");
	hUnitPrm->SetParameterCandidates(unitListLength);
	hadroCutCmd->SetParameter(hUnitPrm);
}

DeviceMessenger::~DeviceMessenger()
{
	delete fElemCmd;
	delete fMatCmd;
	delete fMixCmd;

	delete fBulkCmd;
	delete fSensVolCmd;
	delete fBEOLCmd;

	delete fCylCmd;
	delete fWorldMatCmd;
	delete fWorldSVCmd;

	delete gammaCutCmd;
	delete electCutCmd;
	delete positCutCmd;
	delete hadroCutCmd;

	delete cutsDir;
	delete fMatDirectory;
	delete fDetDirectory;
}

void DeviceMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
	if(command == fElemCmd)
	{
		std::map<G4int, G4double> isotopes;
		G4int mass_number;
		G4double abundance;
		G4String name, symbol;
		std::istringstream is(newValue);
		is >> name >> symbol;
		while(is >> mass_number >> abundance)
		{
			isotopes[mass_number] = abundance;
		}
		// todo : throw error if cannot read int or double values here
		fDeviceConstruction->AddNewElement(name, symbol, isotopes);
	}
	if(command == fMatCmd)
	{
		std::map<G4String, G4int> elements;
		G4String name, d_unit, element;
		G4double density;
		G4int stoic_ratio;
		std::istringstream is(newValue);
		is >> name >> density >> d_unit;
		while(is >> element >> stoic_ratio)
		{
			elements[element] = stoic_ratio;
		}
		density *= G4UIcommand::ValueOf(d_unit);
		// todo : throw error if cannot read int or double values here
		fDeviceConstruction->AddNewMaterial(name, density, elements);
	}
	if(command == fMixCmd)
	{
		std::map<G4String, G4double> materials;
		G4String name, d_unit, material;
		G4double density, ratio;
		std::istringstream is(newValue);
		is >> name >> density >> d_unit;
		while(is >> material >> ratio)
		{
			materials[material] = ratio;
		}
		density *= G4UIcommand::ValueOf(d_unit);
		// todo : throw error if cannot read int or double values here
		fDeviceConstruction->AddNewMixture(name, density, materials);
	}

	if(command == fBulkCmd)
	{
		G4double thick, width;
		G4String material, t_unit, w_unit;
		G4String bias_str;
		std::istringstream is(newValue);
		is >> material >> width >> w_unit >> thick >> t_unit >> bias_str;
		width *= G4UIcommand::ValueOf(w_unit);
		thick *= G4UIcommand::ValueOf(t_unit);
		G4bool bias = G4UIcommand::ConvertToBool(bias_str);
		fDeviceConstruction->AddNewLayer("Bulk", thick, width, bias, material);
	}
	if(command == fSensVolCmd)
	{
		G4double thick, x_width, y_width;
		G4String t_unit, w_unit, bias_str;
		G4String posX, posY, posZ, p_unit;
		std::istringstream is(newValue);
		is >> posX >> posY >> posZ >> p_unit >> x_width >> y_width >> w_unit >> thick >> t_unit >>
			bias_str;
		x_width *= G4UIcommand::ValueOf(w_unit);
		y_width *= G4UIcommand::ValueOf(w_unit);
		thick *= G4UIcommand::ValueOf(t_unit);
		G4bool bias = G4UIcommand::ConvertToBool(bias_str);
		G4String pos(posX + " " + posY + " " + posZ + " " + p_unit);
		G4ThreeVector position = G4UIcommand::ConvertToDimensioned3Vector(pos);
		fDeviceConstruction->AddNewLayer("Sensitive", thick, x_width, bias, "", position, y_width);
	}
	if(command == fBEOLCmd)
	{
		G4double thick, width;
		G4String name, material, t_unit, w_unit;
		G4String bias_str;
		std::istringstream is(newValue);
		is >> material >> width >> w_unit >> thick >> t_unit >> name >> bias_str;
		width *= G4UIcommand::ValueOf(w_unit);
		thick *= G4UIcommand::ValueOf(t_unit);
		G4bool bias = G4UIcommand::ConvertToBool(bias_str);
		if(thick > 0.)
			fDeviceConstruction->AddNewLayer(name, thick, width, bias, material);
	}
	if(command == fCylCmd)
	{
		fDeviceConstruction->UseCylindricalGeometry(fCylCmd->GetNewBoolValue(newValue));
	}
	if(command == fWorldMatCmd)
	{
		fDeviceConstruction->SetWorldMaterial(newValue);
	}
	if(command == fWorldSVCmd)
	{
		fDeviceConstruction->UseWorldAsMotherOfSV(fWorldSVCmd->GetNewBoolValue(newValue));
	}

	///////////////////////////////////////
	if(command == gammaCutCmd)
	{
		G4String region;
		G4double cut;
		G4String unit;
		std::istringstream is(newValue);
		is >> region >> cut >> unit;
		cut *= G4UIcommand::ValueOf(unit);
		fDeviceConstruction->SetCutForGamma(region, cut);
	}
	if(command == electCutCmd)
	{
		G4String region;
		G4double cut;
		G4String unit;
		std::istringstream is(newValue);
		is >> region >> cut >> unit;
		cut *= G4UIcommand::ValueOf(unit);
		fDeviceConstruction->SetCutForElectron(region, cut);
	}
	if(command == positCutCmd)
	{
		G4String region;
		G4double cut;
		G4String unit;
		std::istringstream is(newValue);
		is >> region >> cut >> unit;
		cut *= G4UIcommand::ValueOf(unit);
		fDeviceConstruction->SetCutForPositron(region, cut);
	}
	if(command == hadroCutCmd)
	{
		G4String region;
		G4double cut;
		G4String unit;
		std::istringstream is(newValue);
		is >> region >> cut >> unit;
		cut *= G4UIcommand::ValueOf(unit);
		fDeviceConstruction->SetCutForProton(region, cut);
	}
}
