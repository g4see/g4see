// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "Scoring.hh"

#include "G4SystemOfUnits.hh"
#include "G4Threading.hh"
#include "G4UnitsTable.hh"

#include <fstream>
#include <math.h>

Scoring::Scoring() :
	quantity(""),
	hist_key(""),
	prec(3),
	edep_hist(false),
	ekin_hist(false),
	let_hist(false),
	let_opt(0),
	first_step(true),
	last_step(false),
	all_step(false),
	selectedParticle("")
{}

Scoring::~Scoring()
{}

void Scoring::CreateHistogram()
{
	underflow = 0.;
	overflow = 0.;
	for(int i = 0; i < bins; i++)
	{
		histogram[i] = 0.;
	}
}

void Scoring::AddToHistogram(G4double value, G4double counts)
{
	if(value >= 0.)
	{
		if(value < llim)
			underflow += counts;
		else if(ulim <= value)
			overflow += counts;
		else
		{
			if(log)
				histogram[FindBinLogarithmic(value)] += counts;
			else
				histogram[FindBinLinear(value)] += counts;
		}
	}
}

G4int Scoring::FindBinLogarithmic(G4double value)
{
	return int((log10(value) - llim_log) / ((ulim_log - llim_log) / bins));
}

G4int Scoring::FindBinLinear(G4double value)
{
	return int((value - llim) / ((ulim - llim) / bins));
}

G4double Scoring::GetBinLimitLogarithmic(G4int bin)
{
	return pow(10, bin * ((ulim_log - llim_log) / bins) + llim_log);
}

G4double Scoring::GetBinLimitLinear(G4int bin)
{
	return bin * ((ulim - llim) / bins) + llim;
}

G4String Scoring::GetHistogramFileName(G4String OutputDir, G4int Thread)
{
	std::ostringstream os;
	G4String filename = OutputDir + "/";
	os << hist_key;
	os << "_hist_t";
	os << Thread;
	filename.append(os.str());
	filename.append(".out");
	return filename;
}

void Scoring::PrintHistogramToFile(G4String folder_path, G4int thread)
{
	G4String file_path = GetHistogramFileName(folder_path, thread);
	int width = 15;
	std::ofstream ofs;
	ofs.open(file_path, std::ofstream::out | std::ofstream::trunc);
	if(ofs.is_open())
	{
		ofs << std::setw(width) << quantity << std::setw(width) << "Counts" << std::endl;
		ofs << std::setw(width) << "underflow";
		if(underflow != 0.)
			ofs << std::scientific << std::setprecision(prec) << std::setw(width) << underflow;
		else
			ofs << std::fixed << std::setprecision(0) << std::setw(width) << 0.;
		ofs << std::endl;
		ofs << std::setw(width) << "overflow";
		if(overflow != 0.)
			ofs << std::scientific << std::setprecision(prec) << std::setw(width) << overflow;
		else
			ofs << std::fixed << std::setprecision(0) << std::setw(width) << 0.;
		ofs << std::endl;

		std::map<G4int, G4double>::iterator it;
		for(it = histogram.begin(); it != histogram.end(); ++it)
		{
			G4double Bin = it->first;
			G4double Cnt = it->second;

			ofs << std::scientific << std::setprecision(prec);
			if(log)
				ofs << std::setw(width) << GetBinLimitLogarithmic(Bin);
			else
				ofs << std::setw(width) << GetBinLimitLinear(Bin);

			if(Cnt != 0.)
				ofs << std::scientific << std::setprecision(prec) << std::setw(width) << Cnt;
			else
				ofs << std::fixed << std::setprecision(0) << std::setw(width) << 0.;

			ofs << std::endl;
		}
		ofs << std::scientific << std::setprecision(prec);
		ofs << std::setw(width) << ulim;

		ofs.close();
	}
}
