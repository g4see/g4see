// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "DeviceConstruction.hh"

#include "BiasingMultiParticleXS.hh"
#include "DeviceMessenger.hh"
#include "SensitiveVolume.hh"

#include "G4Box.hh"
#include "G4GeometryManager.hh"
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4MTRunManager.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4ProductionCuts.hh"
#include "G4Region.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4SolidStore.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4Tubs.hh"
#include "globals.hh"

DeviceConstruction::DeviceConstruction(const G4String& outputDir) :
	fOutputDir(outputDir),
	fMessenger(NULL),
	fLogicWorld(NULL),
	fLogicBlack(NULL),
	fLogicBulk(NULL),
	fLogicSensitive(NULL),
	fLogicMotherSV(NULL)
{
	Defaults();
	fMessenger = new DeviceMessenger(this);
}

DeviceConstruction::~DeviceConstruction()
{
	delete fMessenger;
}

void DeviceConstruction::Defaults()
{
	// Default default materials
	G4double vacuumDensity = 1.e-25 * g / cm3;
	G4double pressure = 3.e-18 * pascal;
	G4double temperature = 2.73 * kelvin;
	G4double Z = 1.;
	G4double A = 1.01 * g / mole;
	G4Material* Vacuum =
		new G4Material("VACUUM", Z, A, vacuumDensity, kStateGas, temperature, pressure);
	fMaterialStore["VACUUM"] = Vacuum;

	fWorldMaterialName = "VACUUM";

	// Define DEFAULT geometry

	fBiasForNonPrimaries = false;

	fCylindricalGeom = false;

	fWorldAsMotherSV = false;

	gammCutSV = 1. * millimeter;
	gammCutBulk = 1. * millimeter;
	gammCutBEOL = 1. * millimeter;

	elecCutSV = 1. * micrometer;
	elecCutBulk = 1. * micrometer;
	elecCutBEOL = 1. * micrometer;
	posiCutSV = 1. * micrometer;
	posiCutBulk = 1. * micrometer;
	posiCutBEOL = 1. * micrometer;

	hadrCutSV = 1. * nanometer;
	hadrCutBulk = 1. * nanometer;
	hadrCutBEOL = 1. * nanometer;
}

void DeviceConstruction::AddNewMixture(G4String name, G4double density,
									   std::map<G4String, G4double> materials)
{
	G4NistManager* manager = G4NistManager::Instance();

	G4Material* new_mixture = new G4Material(name, density, materials.size());

	std::map<G4String, G4double>::iterator it;
	for(it = materials.begin(); it != materials.end(); ++it)
	{
		G4String mat_name = it->first;
		G4double ratio = it->second;

		G4Material* mat;
		std::map<G4String, G4Material*>::iterator it2 = fMaterialStore.find(mat_name);
		if(it2 != fMaterialStore.end())
			mat = it2->second;
		else
			mat = manager->FindOrBuildMaterial(mat_name);
		if(!mat)
			G4cerr << "ERROR from AddNewMixture : Material '" << mat_name << "' not found"
				   << G4endl;

		new_mixture->AddMaterial(mat, ratio);

		//        G4cout << "added material : " << mat_name << ", " <<  ratio << G4endl;
	}

	fMaterialStore[name] = new_mixture;
}

void DeviceConstruction::AddNewMaterial(G4String name, G4double density,
										std::map<G4String, G4int> elements)
{
	G4NistManager* manager = G4NistManager::Instance();

	G4Material* new_material = new G4Material(name, density, elements.size());

	std::map<G4String, G4int>::iterator it;
	for(it = elements.begin(); it != elements.end(); ++it)
	{
		G4String element_symbol = it->first;
		G4int stoic_ratio = it->second;

		G4Element* element;
		std::map<G4String, G4Element*>::iterator it2 = fElementStore.find(element_symbol);
		if(it2 != fElementStore.end())
			element = it2->second;
		else
			element = manager->FindOrBuildElement(element_symbol);
		if(!element)
			G4cerr << "ERROR from AddNewMaterial : Element '" << element_symbol << "' not found"
				   << G4endl;

		new_material->AddElement(element, stoic_ratio);

		//        G4cout << "added element : " << element_symbol << ", " <<  stoic_ratio << G4endl;
	}

	fMaterialStore[name] = new_material;
}

void DeviceConstruction::AddNewElement(G4String name, G4String symbol,
									   std::map<G4int, G4double> isotopes)
{
	G4NistManager* manager = G4NistManager::Instance();
	G4Element* nat_element = manager->FindOrBuildElement(symbol);

	G4Element* enriched_element = new G4Element(name, symbol, isotopes.size());

	std::map<G4int, G4double>::iterator it;
	for(it = isotopes.begin(); it != isotopes.end(); ++it)
	{
		G4int isotope_mass_number = it->first;
		G4double isotope_abundance = it->second;

		for(std::size_t ii = 0; ii < nat_element->GetIsotopeVector()->size(); ii++)
		{
			G4int isotope_n = nat_element->GetIsotope(ii)->GetN();

			//            G4cout << "" << isotope_mass_number << ", " <<  isotope_abundance << " =?=
			//            " << isotope_n << G4endl;

			if(isotope_mass_number == isotope_n)
			{
				G4String isotope_name = nat_element->GetIsotope(ii)->GetName();
				G4int isotope_z = nat_element->GetIsotope(ii)->GetZ();
				G4double isotope_molar_mass = nat_element->GetIsotope(ii)->GetA();

				G4Isotope* iso =
					new G4Isotope(isotope_name, isotope_z, isotope_n, isotope_molar_mass);
				enriched_element->AddIsotope(iso, isotope_abundance);

				//                G4cout << "added isotope : " << isotope_name << ", " <<
				//                isotope_mass_number << ", " << isotope_molar_mass << ", " <<
				//                isotope_abundance << G4endl;
				break;
			}
		}
	}

	fElementStore[name] = enriched_element;
}

void DeviceConstruction::AddNewLayer(G4String name, G4double thickness, G4double xwidth,
									 G4bool bias, G4String material, G4ThreeVector position,
									 G4double ywidth)
{
	Layer new_layer;

	if(!material.empty())
	{
		G4Material* newMaterial;
		std::map<G4String, G4Material*>::iterator it = fMaterialStore.find(name);
		if(it != fMaterialStore.end())
			newMaterial = it->second;
		else
		{
			G4NistManager* manager = G4NistManager::Instance();
			newMaterial = manager->FindOrBuildMaterial(material);
		}
		if(newMaterial)
			new_layer.material = newMaterial;
		else
			G4cerr << "ERROR from AddNewLayer : Material '" << material << "' not found" << G4endl;
	}

	new_layer.name = name;
	new_layer.thickness = thickness;
	new_layer.xside = xwidth;
	new_layer.yside = ywidth;
	new_layer.bias = bias;
	new_layer.position = position;

	fTargetLayers.push_back(new_layer);
}

G4VPhysicalVolume* DeviceConstruction::Construct()
{
	G4Material* WorldMat;
	std::map<G4String, G4Material*>::iterator it = fMaterialStore.find(fWorldMaterialName);
	if(it != fMaterialStore.end())
		WorldMat = it->second;
	else
	{
		G4NistManager* manager = G4NistManager::Instance();
		WorldMat = manager->FindOrBuildMaterial(fWorldMaterialName);
	}
	if(!WorldMat)
		G4cerr << "ERROR : Material '" << fWorldMaterialName << "' not found for World volume!"
			   << G4endl;

	G4cout << *(G4Material::GetMaterialTable()) << G4endl;

	////////////////////
	// Parameters //////
	////////////////////

	G4double target_thickness_sum = 0.;
	G4double target_side_max = 0.;
	for(std::vector<Layer>::iterator l = fTargetLayers.begin(); l != fTargetLayers.end(); ++l)
	{
		target_thickness_sum += (*l).thickness;
		target_side_max = std::max((*l).xside, target_side_max);
		target_side_max = std::max((*l).yside, target_side_max);
	}
	G4double WorldSizeX = target_side_max / 2 * 1.5;
	G4double WorldSizeY = WorldSizeX;
	G4double WorldSizeZ = target_thickness_sum * 1.5;

	G4double innerRadius = 0. * um;
	G4double startAngle = 0. * deg;
	G4double spanningAngle = 360. * deg;

	/////////////////
	// Volumes //////
	/////////////////

	G4Box* WorldVol = new G4Box("WorldVol_box", WorldSizeX, WorldSizeY, WorldSizeZ);

	fLogicWorld = new G4LogicalVolume(WorldVol, WorldMat, "World", 0, 0, 0);

	G4VPhysicalVolume* fPhysWorld =
		new G4PVPlacement(0, G4ThreeVector(), fLogicWorld, "World", 0, false, 0);

	//
	// BEOL LAYERS
	//
	Layer bulk, sv;
	G4double prev_z_top = 0.;
	std::vector<Layer>::iterator layer;
	for(layer = fTargetLayers.begin(); layer != fTargetLayers.end(); ++layer)
	{
		Layer ll = *layer;
		if(ll.name.compare("Bulk") == 0)
		{
			bulk = ll;
			continue;
		}
		else if(ll.name.compare("Sensitive") == 0)
		{
			sv = ll;
			continue;
		}
		else
		{
			G4CSGSolid* solidBEOL;
			solidBEOL = new G4Box(ll.name, ll.xside / 2., ll.xside / 2., ll.thickness / 2.);
			if(fCylindricalGeom)
			{
				solidBEOL = new G4Tubs(ll.name, innerRadius, ll.xside / 2., ll.thickness / 2.,
									   startAngle, spanningAngle);
			}

			G4LogicalVolume* logicBEOL = new G4LogicalVolume(solidBEOL, // solid
															 ll.material, // material
															 ll.name); // name
			fLogicBEOL.push_back(logicBEOL);

			G4double z_center = prev_z_top + ll.thickness / 2.;
			prev_z_top += ll.thickness;

			new G4PVPlacement(0, // no rotation
							  G4ThreeVector(0., 0., z_center), // position
							  logicBEOL, // logical volume
							  ll.name, // name
							  fLogicWorld, // mother
							  false, // no boolean operation
							  0, // copy number
							  true); // surface check
		}
	}

	G4CSGSolid* bulkSolid;
	bulkSolid = new G4Box(bulk.name, bulk.xside / 2., bulk.xside / 2., bulk.thickness / 2.);
	if(fCylindricalGeom)
	{
		bulkSolid = new G4Tubs(bulk.name, innerRadius, bulk.xside / 2., bulk.thickness / 2.,
							   startAngle, spanningAngle);
	}

	fLogicBulk = new G4LogicalVolume(bulkSolid, // solid
									 bulk.material, // material
									 bulk.name); // name

	new G4PVPlacement(0, // no rotation
					  G4ThreeVector(0., 0., -1. * bulk.thickness / 2.),
					  fLogicBulk, // its logical volume
					  bulk.name, // its name
					  fLogicWorld, // its mother volume, logical!
					  false, // no boolean operation
					  0, // copy number
					  true); // surface check

	G4CSGSolid* sensitiveSolid;
	sensitiveSolid = new G4Box(sv.name, sv.xside / 2., sv.yside / 2., sv.thickness / 2.);
	if(fCylindricalGeom)
	{
		sensitiveSolid = new G4Tubs(sv.name, innerRadius, sv.xside / 2., sv.thickness / 2.,
									startAngle, spanningAngle);
	}

	if(fWorldAsMotherSV)
	{
		fLogicMotherSV = fLogicWorld;
		sv.material = WorldMat;
		sv.position[2] -= sv.thickness / 2.; // relative to the center of its mother WORLD volume!
	}
	else // by default
	{
		fLogicMotherSV = fLogicBulk;
		sv.material = bulk.material;
		sv.position[2] += bulk.thickness / 2. -
						  sv.thickness / 2.; // relative to the center of its mother BULK volume!
	}

	fLogicSensitive = new G4LogicalVolume(sensitiveSolid, // solid
										  sv.material, // material
										  sv.name); // name

	new G4PVPlacement(0, // no rotation
					  sv.position, // relative to the center of mother volume!
					  fLogicSensitive, // its logical volume
					  sv.name, // its name
					  fLogicMotherSV, // its mother volume, logical!
					  false, // no boolean operation
					  0, // copy number
					  true); // surface check

	// Create regions
	if(!fLogicBEOL.empty())
		fRegionBEOL = new G4Region("BEOL");
	fRegionBulk = new G4Region("Bulk");
	fRegionSV = new G4Region("Sensitive");

	for(std::vector<Layer>::iterator lay = fTargetLayers.begin(); lay != fTargetLayers.end(); ++lay)
	{
		G4LogicalVolume* lv = G4LogicalVolumeStore::GetInstance()->GetVolume((*lay).name);
		if((*lay).name.compare("Bulk") == 0)
			fRegionBulk->AddRootLogicalVolume(lv);
		else if((*lay).name.compare("Sensitive") == 0)
			fRegionSV->AddRootLogicalVolume(lv);
		else
			fRegionBEOL->AddRootLogicalVolume(lv);
	}

	G4ProductionCuts* cutsSV = new G4ProductionCuts();
	cutsSV->SetProductionCut(gammCutSV, "gamma");
	cutsSV->SetProductionCut(elecCutSV, "e-");
	cutsSV->SetProductionCut(posiCutSV, "e+");
	cutsSV->SetProductionCut(hadrCutSV, "proton");
	fRegionSV->SetProductionCuts(cutsSV);

	G4ProductionCuts* cutsBulk = new G4ProductionCuts();
	cutsBulk->SetProductionCut(gammCutBulk, "gamma");
	cutsBulk->SetProductionCut(elecCutBulk, "e-");
	cutsBulk->SetProductionCut(posiCutBulk, "e+");
	cutsBulk->SetProductionCut(hadrCutBulk, "proton");
	fRegionBulk->SetProductionCuts(cutsBulk);

	if(fRegionBEOL)
	{
		G4ProductionCuts* cutsBEOL = new G4ProductionCuts();
		cutsBEOL->SetProductionCut(gammCutBEOL, "gamma");
		cutsBEOL->SetProductionCut(elecCutBEOL, "e-");
		cutsBEOL->SetProductionCut(posiCutBEOL, "e+");
		cutsBEOL->SetProductionCut(hadrCutBEOL, "proton");
		fRegionBEOL->SetProductionCuts(cutsBEOL);
	}

	//////////////////////////////////////////
	G4CSGSolid* blackSolid = new G4Box("BlackVol", WorldSizeX, WorldSizeY, 1 * um);

	fLogicBlack = new G4LogicalVolume(blackSolid, // solid
									  fMaterialStore["VACUUM"], // material
									  "Blackbody"); // name

	new G4PVPlacement(0, // no rotation
					  G4ThreeVector(0., 0., -1. * bulk.thickness - 2 * um),
					  fLogicBlack, // its logical volume
					  "Blackbody", // its name
					  fLogicWorld, // its mother volume, logical!
					  false, // no boolean operation
					  0, // copy number
					  true); // surface check
	//////////////////////////////////////////

	return fPhysWorld;
}

void DeviceConstruction::ConstructSDandField()
{
	SensitiveVolume* SV = new SensitiveVolume("SV", "SVHitsCollection", fOutputDir);
	G4SDManager::GetSDMpointer()->AddNewDetector(SV);
	SetSensitiveDetector(fLogicSensitive, SV);

	if(!biasParticleWeights.empty())
	{
		// Set biasing for different volumes
		BiasingMultiParticleXS* BiasMany = new BiasingMultiParticleXS();

		if(fBiasForNonPrimaries)
			BiasMany->ExtendBiasForNonPrimaries();

		G4cout << "Biasing particle" << G4endl;
		std::map<G4String, G4double>::iterator it;
		for(it = biasParticleWeights.begin(); it != biasParticleWeights.end(); ++it)
		{
			G4String particleToBias = it->first;
			G4double particleBiasWeight = it->second;
			BiasMany->AddParticle(particleToBias, particleBiasWeight);
			G4cout << "    " << particleToBias << " : " << particleBiasWeight << G4endl;
		}

		std::vector<Layer>::iterator lay;
		for(lay = fTargetLayers.begin(); lay != fTargetLayers.end(); ++lay)
		{
			Layer ll = *lay;
			if(ll.bias)
			{
				G4LogicalVolume* lv = G4LogicalVolumeStore::GetInstance()->GetVolume(ll.name);
				BiasMany->AttachTo(lv);
				G4cout << "    to logical volume " << lv->GetName() << G4endl;
			}
		}
	}
}
