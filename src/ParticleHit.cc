// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "ParticleHit.hh"

#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4Threading.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

#include <fstream>
#include <iomanip>

// THIS IS NECESSARY FOR MT MODE
G4ThreadLocal G4Allocator<ParticleHit>* ParticleHitAllocator = 0;

ParticleHit::ParticleHit() :
	G4VHit(),
	fIsFirstStep(false),
	fIsLastStep(false),
	fEventID(-1),
	fTrackID(-1),
	fParentID(-1),
	fName(""),
	fEdep(0.),
	fNdep(0.),
	fStepLen(0.),
	fLETcalc(0.),
	fLETstep(0.),
	fPosFirst(G4ThreeVector()),
	fPosLast(G4ThreeVector()),
	fMomFirst(G4ThreeVector()),
	fMomLast(G4ThreeVector()),
	fEkin(0.),
	fProc(""),
	fWeight(-1.),
	fWeightPreStep(-1.),
	fWeightPostStep(-1.),
	fZ(-1),
	fA(-1),
	fVol("")
{}

ParticleHit::~ParticleHit()
{}

ParticleHit::ParticleHit(const ParticleHit& right) :
	G4VHit()
{
	fIsFirstStep = right.fIsFirstStep;
	fIsLastStep = right.fIsLastStep;
	fEventID = right.fEventID;
	fTrackID = right.fTrackID;
	fParentID = right.fParentID;
	fName = right.fName;
	fEdep = right.fEdep;
	fNdep = right.fNdep;
	fStepLen = right.fStepLen;
	fLETcalc = right.fLETcalc;
	fLETstep = right.fLETstep;
	fPosFirst = right.fPosFirst;
	fPosLast = right.fPosLast;
	fMomFirst = right.fMomFirst;
	fMomLast = right.fMomLast;
	fEkin = right.fEkin;
	fProc = right.fProc;
	fWeight = right.fWeight;
	fZ = right.fZ;
	fA = right.fA;
	fVol = right.fVol;
}

const ParticleHit& ParticleHit::operator=(const ParticleHit& right)
{
	fIsFirstStep = right.fIsFirstStep;
	fIsLastStep = right.fIsLastStep;
	fEventID = right.fEventID;
	fTrackID = right.fTrackID;
	fParentID = right.fParentID;
	fName = right.fName;
	fEdep = right.fEdep;
	fNdep = right.fNdep;
	fStepLen = right.fStepLen;
	fLETcalc = right.fLETcalc;
	fLETstep = right.fLETstep;
	fPosFirst = right.fPosFirst;
	fPosLast = right.fPosLast;
	fMomFirst = right.fMomFirst;
	fMomLast = right.fMomLast;
	fEkin = right.fEkin;
	fProc = right.fProc;
	fWeight = right.fWeight;
	fZ = right.fZ;
	fA = right.fA;
	fVol = right.fVol;
	return *this;
}

G4bool ParticleHit::operator==(const ParticleHit& right) const
{
	return (this == &right) ? true : false;
}

void ParticleHit::Draw()
{}

void ParticleHit::Print()
{
	G4cout << "HIT: Edep: " << std::setw(7) << G4BestUnit(fEdep, "Energy") << G4endl;
}
