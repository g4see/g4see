// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "TrackingAction.hh"

#include "EventAction.hh"
#include "TrackInformation.hh"

#include "G4Track.hh"

TrackingAction::TrackingAction(EventAction* event) :
	G4UserTrackingAction(),
	fEventAction(event),
	eventID(-1)
{}

void TrackingAction::PreUserTrackingAction(const G4Track* track)
{
	//    G4cout << "fEventAction->GetEventID(): " << fEventAction->GetEventID() << G4endl;
	track->SetUserInformation(new TrackInformation(fEventAction->GetEventID()));
}

void TrackingAction::PostUserTrackingAction(const G4Track*)
{}
