// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "Particle.hh"

#include "G4SystemOfUnits.hh"

#include <regex>
#include <sstream>

Particle::Particle(const ParticleHit* hit)
{
	fEventID = hit->GetEventID();
	fTrackID = hit->GetTrackID();
	fParentID = hit->GetParentID();
	fName = RenameParticles(hit->GetName());
	fEdep = hit->GetEdep();
	fNdep = hit->GetNdep();
	fLETcalc = hit->GetLET(2);
	fLETstep = hit->GetLET(3);
	fPos = hit->GetFirstPos(); // first step
	fMom = hit->GetFirstMom(); // first step
	fEkin = hit->GetEkin();
	fProc = RenameProcesses(hit->GetProcess());
	fWeight = hit->GetWeight();
	fZ = hit->GetZ();
	fA = hit->GetA();
	fVol = hit->GetVolume();

	fCounts = 1;

	if(fTrackID == 1)
		isPrimaryParticle = true;
	else
		isPrimaryParticle = false;
	if(fEexc > 0.)
		hasExcitation = true;
	else
		hasExcitation = false;
}

Particle::~Particle()
{}

void Particle::AddHit(const ParticleHit* hit)
{
	fEdep += hit->GetEdep();
	fNdep += hit->GetNdep();
}

G4String Particle::RenameParticles(const G4String ParticleName)
{
	G4String newParticleName = ParticleName;
	fEexc = 0.;

	std::regex nu_pattern("\\w*(nu)\\_\\w+");
	std::smatch nu_match;
	if(std::regex_search(ParticleName.begin(), ParticleName.end(), nu_match, nu_pattern))
	{
		newParticleName = nu_match[1];
	}

	std::regex ion_pattern("([A-z][a-z]?\\d{1,3})\\[(\\d*\\.?\\d*)[A-Z]?\\]");
	std::smatch ion_match;
	if(std::regex_search(ParticleName.begin(), ParticleName.end(), ion_match, ion_pattern))
	{
		newParticleName = ion_match[1].str() + "m"; // metastable excited nuclei
		fEexc = std::stod(ion_match[2]); // keV
		fEexc *= keV; // MeV
	}

	return newParticleName;
}

G4String Particle::RenameProcesses(G4String ProcessName)
{
	std::string biasStr = "biasWrapper";
	std::size_t found = ProcessName.find(biasStr);
	if(found != std::string::npos)
		ProcessName.replace(found, biasStr.length(), "b");
	return ProcessName;
}

void Particle::PrintToStream(std::stringstream& ofs, G4bool csv_format,
							 std::map<G4String, G4bool> fDS)
{
	int width = 12;
	int width_s = 8;
	int width_l = 18;
	int width_e = 24;
	G4String sep = "";
	if(csv_format)
	{
		width = 0;
		width_s = 0;
		width_l = 0;
		width_e = 0;
		sep = ",";
	}

	// compulsory
	ofs << std::fixed;
	ofs << std::setw(width_s) << fEventID << sep;
	ofs << std::setw(width_l) << fName << sep;
	SetDoubleFormat(ofs, fWeight, 3, 1.);
	ofs << std::setw(width) << fWeight << sep;
	// optional
	if(fDS["Z"])
	{
		ofs << std::setw(width_s) << fZ << sep;
	}
	if(fDS["A"])
	{
		ofs << std::setw(width_s) << fA << sep;
	}
	if(fDS["Track"])
	{
		ofs << std::setw(width) << fTrackID << sep;
	}
	if(fDS["Parent"])
	{
		ofs << std::setw(width) << fParentID << sep;
	}
	if(fDS["Ekin"])
	{
		SetDoubleFormat(ofs, fEkin, 4);
		ofs << std::setw(width) << fEkin << sep;
	}
	if(fDS["Position"])
	{
		SetDoubleFormat(ofs, fPos[0], 2);
		ofs << std::setw(width) << fPos[0] << sep;
		SetDoubleFormat(ofs, fPos[1], 2);
		ofs << std::setw(width) << fPos[1] << sep;
		SetDoubleFormat(ofs, fPos[2], 2);
		ofs << std::setw(width) << fPos[2] << sep;
	}
	if(fDS["Momentum"])
	{
		SetDoubleFormat(ofs, fMom[0], 2);
		ofs << std::setw(width) << fMom[0] << sep;
		SetDoubleFormat(ofs, fMom[1], 2);
		ofs << std::setw(width) << fMom[1] << sep;
		SetDoubleFormat(ofs, fMom[2], 2);
		ofs << std::setw(width) << fMom[2] << sep;
	}
	if(fDS["Process"])
	{
		ofs << std::setw(width_e) << fProc << sep;
	}
	if(fDS["Volume"])
	{
		ofs << std::setw(width_l) << fVol << sep;
	}
	if(fDS["Edep"])
	{
		SetDoubleFormat(ofs, fEdep, 4);
		ofs << std::setw(width) << fEdep << sep;
	}
	if(fDS["Ndep"])
	{
		SetDoubleFormat(ofs, fNdep, 4);
		ofs << std::setw(width) << fNdep << sep;
	}
	if(fDS["Counts"])
	{
		ofs << std::setw(width_s) << fCounts << sep;
	}
	if(fDS["Eexc"])
	{
		SetDoubleFormat(ofs, fEexc, 3);
		ofs << std::setw(width) << fEexc << sep;
	}

	ofs << std::endl;
}

void Particle::SetDoubleFormat(std::stringstream& out, G4double fVal, int prec, G4double defVal)
{
	if(fVal == defVal)
	{
		out << std::fixed;
		out << std::setprecision(0);
	}
	else
	{
		out << std::scientific;
		out << std::setprecision(prec);
	}
}
