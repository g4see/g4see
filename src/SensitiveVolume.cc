// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This software uses Geant4 developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later
//  
// Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
// Contributor: Eva Fialová (CERN)
//
// clang-format on

#include "SensitiveVolume.hh"

#include "Particle.hh"
#include "ParticleGrouping.hh"
#include "ScoringMessenger.hh"
#include "Templates.hh"
#include "TrackInformation.hh"

#include "G4EmCalculator.hh"
#include "G4HCofThisEvent.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4Material.hh"
#include "G4SDManager.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4Threading.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4UImanager.hh"
#include "G4UnitsTable.hh"
#include "G4VProcess.hh"
#include "G4ios.hh"

#include <set>

SensitiveVolume::SensitiveVolume(const G4String& name, const G4String& hitsCollectionName,
								 const G4String& outputDir) :
	G4VSensitiveDetector(name),
	SensitiveVolumeName(name),
	fOutputDir(outputDir),
	fHitsCollection(NULL),
	fEdepPerEvent(0.),
	fNEdepPerEvent(0.),
	fWeightPerEvent(0.),
	fPrimStepLengthPerEvent(0.),
	firstPrimPos(G4ThreeVector()),
	lastPrimPos(G4ThreeVector()),
	fEdepPerEventWeighted(0.),
	fNEdepPerEventWeighted(0.),
	fDetailedScoring(false),
	fElecGroupAncestor(false),
	fPosiGroupAncestor(false),
	fGammGroupAncestor(false),
	fPrintPrimary(true),
	fElectronThreshold(10 * keV),
	fPositronThreshold(10 * keV),
	fGammaThreshold(100 * keV),
	nEvents(0),
	nDump(-1),
	fDSEventLimit(-1),
	fDSMemoryLimit(10000000) // 10 MB
{
	collectionName.insert(hitsCollectionName);
	fScoringMessenger = new ScoringMessenger(this);

#if defined(G4MULTITHREADED) && defined(APP_MULTITHREADED)
	fTHREAD = G4Threading::G4GetThreadId();
#else
	fTHREAD = 0;
#endif

	// Optional Detailed Scoring quantities
	fDS = {{"Track", true},		{"Parent", true}, {"Ekin", true},	  {"Edep", true},
		   {"Counts", true},	{"Ndep", false},  {"Process", false}, {"Position", false},
		   {"Momentum", false}, {"Z", false},	  {"A", false},		  {"Volume", false},
		   {"Eexc", false}};

	/// DETAILED SCORING
	fCSVformat = true;
	CreateFile();
}

SensitiveVolume::~SensitiveVolume()
{
	DumpScoringObjsToFile();
	DumpEventNumberToFile();

	/// DETAILED SCORING
	if(fDetailedScoring)
		DumpHitsToFile();
	else
		remove(GetHitsFileName().c_str());

	ClearVector(fEventScoringVector);
	ClearVector(fHitScoringVector);
	delete fScoringMessenger;
}

void SensitiveVolume::AddScoring(G4String scoringType, G4int histID, G4String hist,
								 G4String particle, G4String step, G4String option)

{
	lastScoring = new Scoring();
	lastScoring->SetScoring(hist, histID);

	if(hist.compare("LET") == 0)
	{
		if(!option.empty())
		{
			lastScoring->SetOption(option);
		}
	}
	if(!particle.empty())
	{
		lastScoring->SetParticle(particle);
	}
	if(!step.empty())
	{
		lastScoring->SetStep(step);
	}

	if(scoringType.compare("event") == 0)
	{
		fEventScoringVector.push_back(lastScoring);
	}
	else if(scoringType.compare("hit") == 0)
	{
		fHitScoringVector.push_back(lastScoring);
	}
}

void SensitiveVolume::DumpHitsToFile()
{
	if(fTHREAD > -1)
	{
		std::ofstream ofs;
		ofs.open(GetHitsFileName(), std::ofstream::out | std::ofstream::app);
		if(ofs.is_open())
		{
			ofs << fStream.rdbuf();
			ofs.close();
			fStream.str(std::string());
		}
	}
}

void SensitiveVolume::DumpEventNumberToFile()
{
	if(fTHREAD > -1)
	{
		std::ostringstream os;
		G4String filename = fOutputDir + "/n_event_t";
		os << fTHREAD;
		filename.append(os.str());
		filename.append(".out");

		std::ofstream ofs;
		ofs.open(filename, std::ofstream::out | std::ofstream::trunc);
		if(ofs.is_open())
		{
			ofs << nEvents << std::endl;
			ofs.close();
		}
	}
}

void SensitiveVolume::DumpScoringObjsToFile()
{
	if(fTHREAD > -1)
	{
		std::vector<Scoring*>::iterator ese;
		for(ese = fEventScoringVector.begin(); ese != fEventScoringVector.end(); ++ese)
		{
			(*ese)->PrintHistogramToFile(fOutputDir, fTHREAD);
		}
		std::vector<Scoring*>::iterator hse;
		for(hse = fHitScoringVector.begin(); hse != fHitScoringVector.end(); ++hse)
		{
			(*hse)->PrintHistogramToFile(fOutputDir, fTHREAD);
		}
	}
}

void SensitiveVolume::Initialize(G4HCofThisEvent* hce)
{
	fEdepPerEvent = 0.;
	fNEdepPerEvent = 0.;
	fEdepPerEventWeighted = 0.;
	fNEdepPerEventWeighted = 0.;
	fWeightPerEvent = 1.;
	fPrimStepLengthPerEvent = 0.;

	firstPrimPos = G4ThreeVector();
	lastPrimPos = G4ThreeVector();
	fHitsPerTrackMap.clear();

	// Create hits collection
	fHitsCollection = new ParticleHitsCollection(SensitiveVolumeName, collectionName[0]);

	// Add this collection in hce
	G4int hcID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
	hce->AddHitsCollection(hcID, fHitsCollection);
}

G4bool SensitiveVolume::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
	const G4double edep = aStep->GetTotalEnergyDeposit();
	const G4double nedep = aStep->GetNonIonizingEnergyDeposit();

	const G4double stepLen = aStep->GetStepLength();

	G4bool isFirstStep = aStep->IsFirstStepInVolume();
	G4bool isLastStep = aStep->IsLastStepInVolume();

	G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
	G4StepPoint* postStepPoint = aStep->GetPostStepPoint();
	G4ThreeVector preStepPointPosition = preStepPoint->GetPosition();
	G4ThreeVector postStepPointPosition = postStepPoint->GetPosition();

	const G4double ekin = preStepPoint->GetKineticEnergy();

	G4Track* track = aStep->GetTrack();

	const G4ParticleDefinition* particle = track->GetParticleDefinition();

	G4Material* material = track->GetMaterial();
	G4double density = material->GetDensity();

	// LET per step length
	G4double stepLET;
	if(edep > 0. && stepLen > 0.)
	{
		stepLET = (edep / stepLen) * milligram / (MeV * cm2 * density); // [MeV*cm^2/mg]
		// const G4double stepLET = (edep / stepLen) * (mm/MeV);  // [MeV/mm]
	}
	else
		stepLET = 0.;

	// LET by EM calculator
	G4EmCalculator emCal;
	const G4double calcLET = emCal.ComputeTotalDEDX(ekin, particle, material) * milligram /
							 (MeV * cm2 * density); // [MeV*cm^2/mg]
	// emCal.ComputeTotalDEDX(ekin, particle, material) * (mm/MeV);  // [MeV/mm]

	G4String process;
	const G4VProcess* creatorproc = track->GetCreatorProcess();
	if(creatorproc)
	{
		process = creatorproc->GetProcessName();
	}
	else
	{
		process = "primary";
	}

	ParticleHit* newHit = new ParticleHit();

	G4int eventid = static_cast<TrackInformation*>(track->GetUserInformation())->GetEventID();

	G4String particleName = particle->GetParticleName();

	// track level information
	newHit->SetEventID(eventid);
	newHit->SetTrackID(track->GetTrackID());
	newHit->SetParentID(track->GetParentID());
	newHit->SetName(particleName);
	newHit->SetZ(particle->GetAtomicNumber());
	newHit->SetA(particle->GetAtomicMass());
	newHit->SetProcess(process);
	newHit->SetVolume(track->GetLogicalVolumeAtVertex()->GetName());

	// step level information
	newHit->SetEdep(edep);
	newHit->SetNdep(nedep);
	newHit->SetEkin(ekin);
	newHit->SetStepLength(stepLen);
	newHit->SetLETcalc(calcLET);
	newHit->SetLETstep(stepLET);

	isFirstStep = isFirstStep || (preStepPoint->GetStepStatus() == 1);
	newHit->SetFirstStep(isFirstStep);
	if(isFirstStep)
	{
		newHit->SetFirstPos(preStepPointPosition);
		newHit->SetFirstMom(preStepPoint->GetMomentum());

		if(track->GetTrackID() == 1)
		{
			firstPrimPos = preStepPointPosition;
		}
	}
	isLastStep = isLastStep || (track->GetTrackStatus() != 0);
	newHit->SetLastStep(isLastStep);
	if(isLastStep)
	{
		newHit->SetLastPos(postStepPointPosition);
		newHit->SetLastMom(postStepPoint->GetMomentum());

		if(track->GetTrackID() == 1)
		{
			lastPrimPos = postStepPointPosition;
		}
	}

	if(track->GetTrackID() == 1)
	{
		// Total step length:
		fPrimStepLengthPerEvent += (stepLen / mm); // [mm]
	}

	fHitsPerTrackMap[track->GetTrackID()] += 1;

	const G4double weight = track->GetWeight();
	newHit->SetWeight(weight);
	//	const G4double weightPreStep = preStepPoint->GetWeight();   // todo
	//	const G4double weightPostStep = postStepPoint->GetWeight();
	//	newHit->SetWeightPreStep(weightPreStep);
	//	newHit->SetWeightPostStep(weightPostStep);

	fHitsCollection->insert(newHit);

	if(fWeightPerEvent != 1. && weight != fWeightPerEvent)
		fWeightPerEvent = std::min(fWeightPerEvent, weight);
	else
		fWeightPerEvent = weight;

	// SUMS PER EVENT
	// Energy deposition sum per event for event-by-event scoring:
	fEdepPerEvent += edep;
	fNEdepPerEvent += nedep;

	// Weight corrected sum per event for accumulated dose scoring:
	fEdepPerEventWeighted += (edep * weight);
	fNEdepPerEventWeighted += (nedep * weight);

	return true;
}

void SensitiveVolume::EndOfEvent(G4HCofThisEvent*)
{
	G4int entries = fHitsCollection->entries();
	std::vector<ParticleHit*>* vec = fHitsCollection->GetVector();
	ParticleHit* hit = NULL;

	// G4ThreeVector primVector = lastPrimPos - firstPrimPos;

	/// STANDARD SCORING - EVENT SCORING
	std::vector<Scoring*>::iterator ese;
	for(ese = fEventScoringVector.begin(); ese != fEventScoringVector.end(); ++ese)
	{
		/// STANDARD SCORING: Edep per event
		if((*ese)->IsEdepScoring())
		{
			(*ese)->AddToHistogram(fEdepPerEvent, fWeightPerEvent);
		}
		/// STANDARD SCORING: LET per event
		if((*ese)->IsLETScoring() && (*ese)->GetLETOption() == 1)
		{
			G4double density = G4LogicalVolumeStore::GetInstance()
								   ->GetVolume("Sensitive")
								   ->GetMaterial()
								   ->GetDensity();

			G4double LETPerEvent;
			if(fEdepPerEvent > 0. && fPrimStepLengthPerEvent > 0.)
			{
				LETPerEvent = (fEdepPerEvent / fPrimStepLengthPerEvent) * milligram /
							  (MeV * cm2 * density); // [MeV*cm^2/mg]
				// LETPerEvent = fEdepPerEvent / fPrimStepLengthPerEvent;   // [MeV/mm]
				// LETPerEvent = fEdepPerEvent / primVector.mag();
			}
			else
			{
				LETPerEvent = 0.;
			}
			(*ese)->AddToHistogram(LETPerEvent, fWeightPerEvent);
		}
	}

	/// STANDARD SCORING - HIT SCORING
	std::vector<Scoring*>::iterator hse;
	for(hse = fHitScoringVector.begin(); hse != fHitScoringVector.end(); ++hse)
	{
		G4String selected = (*hse)->GetSelectedParticle();

		///  STANDARD SCORING: quantities per step (first or last step)
		for(G4int i = 0; i < entries; i++)
		{
			hit = vec->at(i);

			// Filtering by primary/secondary
			G4int trackID = hit->GetTrackID();

			// Filtering by particle
			bool partFilter = false;
			if(selected.compare("primary") == 0)
			{
				partFilter = (trackID == 1);
			}
			else if(selected.compare("non-primary") == 0)
			{
				partFilter = (trackID != 1);
			}
			else if(selected.compare("all") == 0)
			{
				partFilter = true;
			}
			else
			{
				G4String particleName = hit->GetName();
				partFilter = (particleName.compare(selected) == 0);
			}

			// STANDARD SCORING: LET
			if((*hse)->IsLETScoring() && partFilter)
			{
				G4int LET_opt = (*hse)->GetLETOption(); // Opt2 or Opt3
				// Scoring only FIRST STEP in SV
				if((*hse)->IsFirstStepScoring() && hit->IsFirstStep())
				{
					(*hse)->AddToHistogram(hit->GetLET(LET_opt), hit->GetWeight());
				}
				// Scoring only LAST STEP in SV
				else if((*hse)->IsLastStepScoring() && hit->IsLastStep())
				{
					(*hse)->AddToHistogram(hit->GetLET(LET_opt), hit->GetWeight());
				}
				// Scoring ALL STEPS in SV
				else if((*hse)->IsAllStepScoring())
				{
					double WeightPerHit = 1. / fHitsPerTrackMap[trackID];
					(*hse)->AddToHistogram(hit->GetLET(LET_opt), hit->GetWeight() * WeightPerHit);
				}
			}

			/// STANDARD SCORING: Ekin per particle
			if((*hse)->IsEkinScoring() && partFilter)
			{
				// Scoring only FIRST STEP in SV
				if((*hse)->IsFirstStepScoring() && hit->IsFirstStep())
				{
					(*hse)->AddToHistogram(hit->GetEkin(), hit->GetWeight());
				}
				// Scoring only LAST STEP in SV
				else if((*hse)->IsLastStepScoring() && hit->IsLastStep())
				{
					(*hse)->AddToHistogram(hit->GetEkin(), hit->GetWeight());
				}
				// Scoring ALL STEPS in SV
				else if((*hse)->IsAllStepScoring())
				{
					double WeightPerHit = 1. / fHitsPerTrackMap[trackID];
					(*hse)->AddToHistogram(hit->GetEkin(), hit->GetWeight() * WeightPerHit);
				}
			}
		}
	}

	/// DETAILED SCORING - BEGIN
	if(fDetailedScoring)
	{
		G4int prevTrackID = -1;

		std::vector<Particle*> particleVector, electronVector, positronVector, gammaVector;

		std::map<G4int, std::vector<Particle*>> restElectronMap, restPositronMap, restGammaMap,
			restToGroupMap;

		std::vector<ParticleGroup*> elecGroups, posiGroups, gammGroups, allGroups;

		GroupType grp_type = none;
		Particle* pp;

		for(G4int i = 0; i < entries; i++)
		{
			hit = vec->at(i);

			G4int currTrackID = hit->GetTrackID();

			if(currTrackID != prevTrackID)
			{
				pp = new Particle(hit); // First hit of a new particle in SV

				if(currTrackID != 1) // excluding primary particles e-, e+ and gamma groups
				{
					G4String currName = hit->GetName();
					G4double currEkin = hit->GetEkin();

					if(currName.compare("e-") == 0 && currEkin < fElectronThreshold)
						grp_type = electron;
					else if(currName.compare("e+") == 0 && currEkin < fPositronThreshold)
						grp_type = positron;
					else if(currName.compare("gamma") == 0 && currEkin < fGammaThreshold)
						grp_type = gamma;
					else
						grp_type = individual;
				}
				else
					grp_type = individual;

				switch(grp_type)
				{
					case individual:
						particleVector.push_back(pp);
						break;
					case electron:
						electronVector.push_back(pp);
						break;
					case positron:
						positronVector.push_back(pp);
						break;
					case gamma:
						gammaVector.push_back(pp);
						break;
					case none:
						G4cerr << "error";
				}
			}
			else if(currTrackID == prevTrackID)
			{
				switch(grp_type)
				{
					case individual:
						particleVector.back()->AddHit(hit);
						break;
					case electron:
						electronVector.back()->AddHit(hit);
						break;
					case positron:
						positronVector.back()->AddHit(hit);
						break;
					case gamma:
						gammaVector.back()->AddHit(hit);
						break;
					case none:
						G4cerr << "error";
				}
			}
			prevTrackID = currTrackID;
		}

		G4int initTrackID = -1;
		G4int nGroups = 0;

		// Grouping e- particles below Ekin threshold
		if(!electronVector.empty())
		{
			ParticleGrouping* electron_grp =
				new ParticleGrouping(initTrackID); // Grouping electrons below Ekin threshold

			if(fElecGroupAncestor)
			{
				elecGroups = electron_grp->GroupByAncestors(electronVector, particleVector);
				restElectronMap = electron_grp->GetOrphanParticles();
			}
			else
				elecGroups = electron_grp->GroupParticles(electronVector);
			nGroups += elecGroups.size();

			delete electron_grp;
		}
		// Grouping e+ particles below Ekin threshold
		if(!positronVector.empty())
		{
			ParticleGrouping* positron_grp = new ParticleGrouping(
				initTrackID - nGroups); // Grouping positrons below Ekin threshold

			if(fPosiGroupAncestor)
			{
				posiGroups = positron_grp->GroupByAncestors(positronVector, particleVector);
				restPositronMap = positron_grp->GetOrphanParticles();
			}
			else
				posiGroups = positron_grp->GroupParticles(positronVector);
			nGroups += posiGroups.size();

			delete positron_grp;
		}
		// Grouping gamma particles below Ekin threshold
		if(!gammaVector.empty())
		{
			ParticleGrouping* gamma_grp =
				new ParticleGrouping(initTrackID - nGroups); // Grouping gamma below Ekin threshold

			if(fGammGroupAncestor)
			{
				gammGroups = gamma_grp->GroupByAncestors(gammaVector, particleVector);
				restGammaMap = gamma_grp->GetOrphanParticles();
			}
			else
				gammGroups = gamma_grp->GroupParticles(gammaVector);
			nGroups += gammGroups.size();

			delete gamma_grp;
		}

		allGroups = elecGroups;
		allGroups.insert(allGroups.end(), posiGroups.begin(), posiGroups.end());
		allGroups.insert(allGroups.end(), gammGroups.begin(), gammGroups.end());

		restToGroupMap = restElectronMap;
		restToGroupMap.insert(restPositronMap.begin(), restPositronMap.end());
		restToGroupMap.insert(restGammaMap.begin(), restGammaMap.end());

		// Grouping rest of e-, e+ and gamma below Ekin threshold which were created by other groups
		// OR particles not passing through SV !
		ParticleGroup* grpd;
		if(!restToGroupMap.empty())
		{
			std::map<G4int, std::vector<Particle*>>::iterator ret;
			for(ret = restToGroupMap.begin(); ret != restToGroupMap.end(); ret++)
			{
				G4int missing_parent = ret->first;
				std::vector<Particle*> to_be_grouped = ret->second;

				ParticleGroup* GroupParent = SearchInAllGroupTracks(missing_parent, allGroups);
				if(GroupParent) // parent particle is another ParticleGroup
				{
					G4int grp_parent_id = GroupParent->GetGrpTrackID();
					G4String grp_parent_particle = GroupParent->GetGrpParticleName();
					if(grp_parent_id < 0 &&
					   grp_parent_particle.compare(to_be_grouped.front()->GetName()) == 0)
					{
						GroupParent->AddToGroup(to_be_grouped);
					}
					else
					{
						grpd = new ParticleGroup(initTrackID - nGroups, grp_parent_id);
						grpd->CreateGroup(to_be_grouped);
						allGroups.push_back(grpd);
						nGroups += 1;
					}
				}
				else // no GroupParent, since parent particle was not passing through SV
				{
					grpd = new ParticleGroup(initTrackID - nGroups, missing_parent);
					grpd->CreateGroup(to_be_grouped);
					allGroups.push_back(grpd);
					nGroups += 1;
				}
			}
		}

		if(!particleVector.empty())
		{
			std::vector<Particle*>::iterator first_particle = particleVector.begin();
			if((*first_particle)->isPrimary())
			{
				// Print primary particle to output Hits file (optional)
				if(fPrintPrimary)
					(*first_particle)->PrintToStream(fStream, fCSVformat, fDS);
				delete *first_particle;
				particleVector.erase(first_particle);
			}
			// Print individual particles to output Hits file
			for(std::vector<Particle*>::reverse_iterator ji = particleVector.rbegin();
				ji != particleVector.rend(); ++ji)
			{
				(*ji)->PrintToStream(fStream, fCSVformat, fDS);
				delete *ji;
			}
			particleVector.clear();
		}
		if(!allGroups.empty())
		{
			// Print particle groups to output Hits file
			for(std::vector<ParticleGroup*>::iterator ja = allGroups.begin(); ja != allGroups.end();
				ja++)
			{
				(*ja)->PrintGroupToStream(fStream, fCSVformat, fDS);
				delete *ja;
			}
		}
		allGroups.clear();

		ClearVector(electronVector);
		ClearVector(positronVector);
		ClearVector(gammaVector);

		/// DUMP DATA TO DETAILED SCORING HITS FILE
		if(fDSEventLimit > 0)
		{
			if(nEvents > 0 && nEvents % fDSEventLimit == 0)
			{
				//                G4cout << "Event: " << nEvents << ", " << "Data buffer size: " <<
				//                buffer_size << " bytes. Buffer dumped to file." << G4endl;
				DumpHitsToFile();
			}
		}
		else // default option
		{
			fStream.seekg(0, std::ios::end);
			int buffer_size = fStream.tellg();
			fStream.seekg(0, std::ios::beg);
			if(nEvents > 0 && buffer_size >= fDSMemoryLimit)
			{
				//                G4cout << "Event: " << nEvents << ", " << "Data buffer size: " <<
				//                buffer_size << " bytes. Buffer dumped to file." << G4endl;
				DumpHitsToFile();
			}
		}
	}
	/// DETAILED SCORING - END

	if(nDump > 0 && nEvents > 0)
	{
		if(nEvents % nDump == 0)
		{
			DumpScoringObjsToFile();
			DumpEventNumberToFile();
		}
	}

	nEvents++;
}

ParticleGroup* SensitiveVolume::SearchInAllGroupTracks(G4int track_to_find,
													   std::vector<ParticleGroup*> all_groups)
{
	for(std::vector<ParticleGroup*>::iterator it = all_groups.begin(); it != all_groups.end(); it++)
	{
		if((*it)->FindTrackID(track_to_find))
			return (*it);
	}
	return NULL;
}

G4String SensitiveVolume::GetHitsFileName()
{
	std::ostringstream os;
	G4String filename = fOutputDir + "/Hits_t";
	os << fTHREAD;
	filename.append(os.str());
	if(fCSVformat)
		filename.append(".csv");
	else
		filename.append(".out");
	return filename;
}

void SensitiveVolume::CreateFile()
{
	if(fTHREAD > -1)
	{
		remove(GetHitsFileName().c_str());
		fCSVformat = !fCSVformat;
		remove(GetHitsFileName().c_str());
		fCSVformat = !fCSVformat;
		CreateAsciiFileHits(GetHitsFileName());
	}
}

void SensitiveVolume::CreateAsciiFileHits(G4String filename)
{
	int width = 12;
	int width_s = 8;
	int width_l = 18;
	int width_e = 24;
	G4String sep = "";
	if(fCSVformat)
	{
		width = 0;
		width_s = 0;
		width_l = 0;
		width_e = 0;
		sep = ",";
	}

	std::ofstream ofs;
	ofs.open(filename, std::ofstream::out | std::ofstream::trunc);
	if(ofs.is_open())
	{
		// compulsory
		ofs << std::setw(width_s) << "event" << sep;
		ofs << std::setw(width_l) << "particle" << sep;
		ofs << std::setw(width) << "weight" << sep;

		// optional
		if(fDS["Z"])
		{
			ofs << std::setw(width_s) << "Z" << sep;
		}
		if(fDS["A"])
		{
			ofs << std::setw(width_s) << "A" << sep;
		}
		if(fDS["Track"])
		{
			ofs << std::setw(width) << "track" << sep;
		}
		if(fDS["Parent"])
		{
			ofs << std::setw(width) << "parent" << sep;
		}
		if(fDS["Ekin"])
		{
			ofs << std::setw(width) << "E_kin" << sep;
		}
		if(fDS["Position"])
		{
			ofs << std::setw(width) << "pos_x" << sep;
			ofs << std::setw(width) << "pos_y" << sep;
			ofs << std::setw(width) << "pos_z" << sep;
		}
		if(fDS["Momentum"])
		{
			ofs << std::setw(width) << "mom_x" << sep;
			ofs << std::setw(width) << "mom_y" << sep;
			ofs << std::setw(width) << "mom_z" << sep;
		}
		if(fDS["Process"])
		{
			ofs << std::setw(width_e) << "process" << sep;
		}
		if(fDS["Volume"])
		{
			ofs << std::setw(width_l) << "volume" << sep;
		}
		if(fDS["Edep"])
		{
			ofs << std::setw(width) << "E_dep" << sep;
		}
		if(fDS["Ndep"])
		{
			ofs << std::setw(width) << "N_dep" << sep;
		}
		if(fDS["Counts"])
		{
			ofs << std::setw(width_s) << "counts" << sep;
		}
		if(fDS["Eexc"])
		{
			ofs << std::setw(width) << "E_exc" << sep;
		}

		ofs << std::endl;
		ofs.close();
	}
}
