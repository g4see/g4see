// clang-format off
//  
// G4SEE Single Event Effect simulation toolkit
// ============================================
// SPDX-FileCopyrightText: © 1994 Copyright Holders of the Geant4 Collaboration <https://cern.ch/geant4/license>
// SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
//
// This software is distributed under the terms of the GNU General Public License version 3
// (GPL Version 3) or any later version and the Geant4 Software License, copied verbatim in the
// files "LICENSES/GPL-3.0-or-later.txt" and "LICENSES/LicenseRef-Geant4.txt" respectively.
//
// In applying this license, CERN does not waive the privileges and immunities granted to it
// by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
//
// This file includes and uses software developed by Members of the Geant4 Collaboration (https://cern.ch/geant4).
//
// SPDX-License-Identifier: GPL-3.0-or-later AND LicenseRef-Geant4
//  
// Author: Geant4 Collaboration
// Contributor: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
//
// clang-format on

#include "BiasingMultiParticleXS.hh"

#include "BiasingXS.hh"

#include "G4BiasingProcessInterface.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"

BiasingMultiParticleXS::BiasingMultiParticleXS() :
	G4VBiasingOperator("TestManyExponentialTransform")
{
	fBiasOnlyPrimaries = true;
}

void BiasingMultiParticleXS::AddParticle(G4String particleName, G4double biasFactor)
{
	const G4ParticleDefinition* particle =
		G4ParticleTable::GetParticleTable()->FindParticle(particleName);

	if(particle == 0)
	{
		G4ExceptionDescription ed;
		ed << "Particle '" << particleName << "' not found !" << G4endl;
		G4Exception("BiasingMultiParticleXS::AddParticle(...)", "exGB01.02", JustWarning, ed);
		return;
	}

	BiasingXS* optr = new BiasingXS(particleName);
	optr->SetXSBiasFactor(biasFactor);
	fParticlesToBias.push_back(particle);
	fBOptrForParticle[particle] = optr;
}

G4VBiasingOperation* BiasingMultiParticleXS::ProposeOccurenceBiasingOperation(
	const G4Track* track, const G4BiasingProcessInterface* callingProcess)
{
	// -- examples of limitations imposed to apply the biasing:
	// -- limit application of biasing to primary particles only:
	if(fBiasOnlyPrimaries && track->GetParentID() != 0)
		return 0;
	// -- limit to at most 5 biased interactions:
	if(fnInteractions > 4)
		return 0;
	// -- and limit to a weight of at least 0.05:
	if(track->GetWeight() < 0.05)
		return 0;

	if(fCurrentOperator)
		return fCurrentOperator->GetProposedOccurenceBiasingOperation(track, callingProcess);
	else
		return 0;
}

void BiasingMultiParticleXS::StartTracking(const G4Track* track)
{
	// -- fetch the underneath biasing operator, if any, for the current particle type:
	const G4ParticleDefinition* definition = track->GetParticleDefinition();
	std::map<const G4ParticleDefinition*, BiasingXS*>::iterator it =
		fBOptrForParticle.find(definition);
	fCurrentOperator = 0;
	if(it != fBOptrForParticle.end())
	{
		fCurrentOperator = (*it).second;
	}

	// -- reset count for number of biased interactions:
	fnInteractions = 0;
}

void BiasingMultiParticleXS::OperationApplied(const G4BiasingProcessInterface* callingProcess,
											  G4BiasingAppliedCase biasingCase,
											  G4VBiasingOperation* occurenceOperationApplied,
											  G4double weightForOccurenceInteraction,
											  G4VBiasingOperation* finalStateOperationApplied,
											  const G4VParticleChange* particleChangeProduced)
{
	// -- count number of biased interactions:
	fnInteractions++;

	// -- inform the underneath biasing operator that a biased interaction occured:
	if(fCurrentOperator)
		fCurrentOperator->ReportOperationApplied(
			callingProcess, biasingCase, occurenceOperationApplied, weightForOccurenceInteraction,
			finalStateOperationApplied, particleChangeProduced);
}
