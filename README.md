<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# G4SEE Single Event Effect simulation toolkit 

![logo](doc/source/_static/logos/G4SEE-logo-full.png)

## [Website](https://cern.ch/g4see)

## [Documentation](https://g4see-docs.web.cern.ch/)

## [Releases](https://gitlab.cern.ch/g4see/g4see/-/releases)

## [Docker images](https://gitlab.cern.ch/g4see/g4see/container_registry/14735)

## [Changelog](CHANGELOG.md)

## [Contribution guide](CONTRIBUTING.md)

## [Copyright](COPYRIGHT.md)

## [Licenses](LICENSES)

## [Authors](AUTHORS.md)

<br>
<br>

> **Acknowledgement**
> 
> [![hearts](https://hearts-project.eu/img/logo/hearts-logo.png){height=100px}](https://hearts-project.eu) [![radnext](https://radnext.web.cern.ch/images/logo/radnext_logo_short_transp.png){height=50px}](https://cern.ch/radnext) [![cern](https://g4see.web.cern.ch/img/CERN-logo.png){height=100px}](https://home.cern) [![r2e](https://g4see.web.cern.ch/img/R2E-logo.png){height=100px}](https://cern.ch/r2e)
> 
> *This activity has received funding from the European Union under Grant Agreement No. 101082402, corresponding to the HEARTS project,  through the Space Work Programme of the European Commission.*
>  
> *This activity has received funding from the European Union’s 2020 research and innovation programme under grant agreement No. 101008126, corresponding to the RADNEXT project.*
>
> - **[HEARTS project (hearts-project.eu)](https://hearts-project.eu)**
> - **[RADNEXT project (cern.ch/radnext)](https://cern.ch/radnext)**
> - **[CERN Radiation To Electronics (cern.ch/r2e)](https://cern.ch/r2e)**
