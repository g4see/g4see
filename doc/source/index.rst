..
.. SPDX-FileCopyrightText: © 2022 CERN
.. SPDX-License-Identifier: CC0-1.0
..


.. image:: _static/logos/G4SEE-logo-full.png
   :width: 75%
   :align: center

.. rst-class:: center

Single Event Effect simulation toolkit
========================================

|img1| |img2|

.. |img1| image:: _static/figures/G4SEE_HEARTS_CERN.jpg
   :width: 64%
   :align: middle

.. |img2| image:: _static/figures/G4SEE_example_geometry.png
   :width: 34%
   :align: middle


.. important::
   The use of G4SEE toolkit (`https://cern.ch/g4see <https://cern.ch/g4see>`_) must be acknowledged explicitly by citing the open-access G4SEE publication:

   Dávid Lucsányi, Rubén García Alía, Kacper Biłko, Matteo Cecchetto, Salvatore Fiore, Elisa Pirovano, "G4SEE: A Geant4-Based Single Event Effect Simulation Toolkit and Its Validation Through Monoenergetic Neutron Measurements," in IEEE Transactions on Nuclear Science, vol. 69, no. 3, pp. 273-281, March 2022, `doi:10.1109/TNS.2022.3149989 <https://doi.org/10.1109/TNS.2022.3149989>`_.


----

G4SEE Documentation
---------------------

.. toctree::
   :maxdepth: 2

   Index <self>

   installation_guide.md
   getting_started.md
   input_files.md
   geometry.md
   physics.md
   source.md
   scoring.md
   output_files.md
   visualization.md
   contributing.rst
   copyright.rst

.. toctree::
   :maxdepth: 1

   authors.rst
   changelog.rst


----

.. admonition:: Acknowledgement

   |logo1|  |logo2|  |logo3|   |logo4|

   *This activity has received funding from the European Union under Grant Agreement No. 101082402, corresponding to the HEARTS project, through the Space Work Programme of the European Commission.*

   *This activity has received funding from the European Union’s 2020 research and innovation programme under grant agreement No. 101008126, corresponding to the RADNEXT project.*

   - `HEARTS project (hearts-project.eu) <https://hearts-project.eu>`_
   - `RADNEXT project (cern.ch/radnext) <https://cern.ch/radnext>`_
   - `CERN Radiation To Electronics (cern.ch/r2e) <https://cern.ch/r2e>`_

.. |logo1| image:: https://hearts-project.eu/img/logo/hearts-logo.png
   :height: 100px
   :align: middle

.. |logo2| image:: https://radnext.web.cern.ch/images/logo/radnext_logo_short_transp.png
   :height: 50px
   :align: middle

.. |logo3| image:: https://g4see.web.cern.ch/img/CERN-logo.png
   :height: 100px
   :align: middle

.. |logo4| image:: https://g4see.web.cern.ch/img/R2E-logo.png
   :height: 100px
   :align: middle
