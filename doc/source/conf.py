#  
# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
# copied verbatim in the file "LICENSES/CC-BY-4.0.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: CC-BY-4.0
#  
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
#  

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

from pathlib import Path
from datetime import datetime

date = datetime.now()  # type: datetime

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'G4SEE'
copyright = '2022 CERN for the benefit of the G4SEE Collaboration'
author = 'Dávid Lucsányi'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx_multiversion",
]

myst_enable_extensions = [
    "dollarmath",
    "amsmath"
]
myst_heading_anchors = 3

templates_path = ['_templates']

# Whitelist pattern for branches
smv_branch_whitelist = r'^(master|main|devel)$'
# Whitelist pattern for tags
smv_tag_whitelist = r'^.*$'     # all tags
# Pattern for released versions
smv_released_pattern = r'^refs/tags/.*$'    # each tag is a release
# Use the branch/tag name
smv_outputdir_format = '{ref.name}'
# this is needed for Gitlab CI jobs in Docker containers
smv_remote_whitelist = r'^(origin)$'
# Latest release
smv_latest_version = 'master'
# default value, which should be overwritten during build like this: `-D smv_latest_version='v0.5'`

exclude_patterns = []

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = 'alabaster'
html_theme = "sphinx_book_theme"

html_static_path = ['_static']
html_css_files = ['style.css']

root_folder = Path(__file__).parent.parent
html_title = "G4SEE"

html_logo = "_static/logos/G4SEE-logo-full.png"
html_favicon = "_static/logos/G4SEE-Favicon.png"

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.

html_sidebars = {
   "**": [
       'navbar-logo.html',     # Displays the logo and site title.
       'title.html',
       'icon-links.html',      # Displays icon links
       'search-button-field.html',        # A bootstrap-based search bar (from the PyData Sphinx Theme)
       'sbt-sidebar-nav.html',  # A bootstrap-based navigation menu for your book.
       'versioning.html',          # to include different versions (tags, branches)
       'license.html',          # to include different versions (tags, branches)
    ],
}

html_theme_options = {
    "repository_url": "https://gitlab.cern.ch/g4see/g4see",
    "use_edit_page_button": False,
    "use_repository_button": True,
    "use_issues_button": True,
    "use_download_button": False,
    "collapse_navbar": True,
    "home_page_in_toc": False,
    "extra_footer": f"""<p>Last updated on {date:%Y %B %d}</p>""",
    "toc_title": "Contents",  # Control the right sidebar items
}

html_context = {
    "default_mode": "light",
    "current_version": "latest-version",
    "latest_version": "latest-version",
}
