<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->
# Getting Started

## Running G4SEE application

Run the `g4see` application from command-line.

```shell
g4see [-h] [-v] /path/to/input/macro.mac [-o OUTPUT_FOLDER]
```
For the list of arguments and help, please run `g4see -h`.

A macro file path is a mandatory input argument, see [Input files](input_files.md) section how to create macro files.

Output directory can be either relative or absolute path. 

Examples:
  * Without any optional arguments:                  `g4see input.mac`
  * Define output directory:                         `g4see input.mac -o output/`
  * Redirect standard output (stdout) into a file:   `g4see input.mac > log.txt`
  
## Merging histograms

For single runs only (not for parametric runs), one can run the compiled C++ tool `mergeHistograms`.
This tool sums *Edep* and *Ekin* standard scoring histogram files in single folder or multiple folders (see below) into one histogram, as well as calculates standard deviation of each bin value.  

```shell
mergeHistograms [-h] [-v] [/common/path/to/data/folder(_#)] [-o OUTPUT_FOLDER] [-d]
```
For the list of arguments and help, please run `mergeHistograms -h`.

- Merging histogram files located in a single folder: define the path of that single folder
- Merging histogram files located in multiple folders: define the common path ending with `#` placeholder symbol (e.g. `path/to/folder_#`)
- If no folder path provided, by default the application will look for `out_#` folders in current directory: `out_0`, `out_1`, `out_2`, etc.
- For details about output files and merging, see [Output Files](output_files.md)
- For merging output files of parametric runs recursively, see [Recursive histogram merging](#recursive-histogram-merging) below

```{figure} _static/figures/G4SEE-architecture.png
High-level architecture of the G4SEE toolkit
```

## Auxiliary G4SEE scripts 

In the build directory of *G4SEE*, there are useful auxiliary python3 scripts to start or delete jobs on a computer cluster, merge and visualize histogram data generated.

```shell
python3 g4see.py [-h] {submit,delete,view,merge,plot} ...
```

### Submitting cluster jobs & parametrization

Submitting G4SEE parallel jobs to a computer cluster queue for a single run using `g4see.py submit`.

Parallel jobs are jobs using the same macro file as `input`, but running on different computer cluster nodes independently 
(with different random seeds if no seed defined in macro), to enhance performance and make use of a computer cluster. 
Multi-threading and multi-processing are not necessarily supported on computer clusters, 
therefore this script does not start multi-threading or multi-processing simulation runs.

```shell
python3 g4see.py submit [-h] -o OUTPUT -j JOBS [-q QUEUE] [-G4 G4_PATH] [-G4SEE G4SEE_PATH] input
```
For the list of arguments and help, please run `python3 g4see.py submit -h`.

The `input` can be either standard macro (`.mac`) or a parametric YAML (`.yaml`) files as well.   
See [Input Files](input_files.md) how to create these macro files.

When using a parametric YAML (`.yaml`) file, a set of differently parametrized macro input files are automatically generated from a master macro file, 
in order to perform parametric studies changing a single or even combination of multiple parameters/settings in the macro.

- If `-q, --queue` argument is provided, the script will submit the jobs to the defined cluster queue, otherwise it only generates the parametrized macros and output folders 
- The number of parallel jobs defined via the `-j, --jobs` argument (in parametric runs this is not the total job number, but the jobs per parameter values) 
- Output folders defined with `-o, --output` are created automatically (in parametric runs folder names are based on the elements of the `value-list` keyword)
- Already existing output folders (including all files) will be overwritten
- If `g4see` is not installed in `PATH` env. variable (e.g. in `/bin/usr/local/`), then G4SEE app's absolute path should be provided via the `-G4SEE, --G4SEE_PATH` arg (default is `g4see`) 
- If `geant4.sh` script was not sourced (which is needed on cluster nodes), then Geant4 absolute path should be provided via the `-G4, --G4_PATH` arg (default is `None`)  

```shell
python3 g4see.py submit parametric_source.yaml -o outputs/ -j 10 -q normal
```
(This example submits 3*10=30 parametrized jobs in total to the cluster queue called "normal".)

- `examples_parametric/parametric_source.yaml`
- `examples_parametric/parametric_physics.yaml`
- `examples_parametric/parametric_SV.yaml`

### Monitoring cluster jobs

```{attention}
   This script is experimental!
```

To monitor a user's G4SEE jobs submitted to computer cluster nodes.

```shell
python3 g4see.py monitor [-h] [-o OUTPUT] [-a] [-st {R,running,Q,queue}] [-q QUEUE] [-ss SUBSTRING] [-id left [right ...]] user
```
For the list of arguments and help, please run `python3 g4see.py monitor -h`.

### Deleting cluster jobs 

```{warning}
   This script is experimental, use it cautiously!
```

To delete a set or all of a user's G4SEE jobs submitted to computer cluster nodes. Only those jobs affected, which names start with "g4see".

```shell
python3 g4see.py delete [-h] [-a] [-st {R,running,Q,queue}] [-q QUEUE] [-ss SUBSTRING] [-id left [right ...]] user
```
For the list of arguments and help, please run `python3 g4see.py delete -h`.

### Visualizing geometry

Providing a macro file as `macro`, you can visualize your geometry in 2D for checking or to include in documentation, presentation or paper.

```shell
python3 g4see.py view [-h] [-ns] [-eq] [-o OUTPUT] macro
```
For the list of arguments and help, please run `python3 g4see.py view -h`.

```{figure} _static/figures/SRAM_example_xz.png
X-Z side view of target defined in SRAM example macro
```

### Recursive histogram merging

After a parallel run, when you have many data files in separate folders, you need to merge histograms manually after all your jobs are finished.
For parametric runs or multiple parallel jobs, you have recursive folder structure (set of parallel jobs), so you need to run:

```shell
python3 g4see.py merge [-h] folder
```
For the list of arguments and help, please run `python3 g4see.py merge -h`.

The script will merge all the data per run locally, so only folders with names `out_#` within each subdirectory.
This script calls `mergeHistograms` C++ tool (see [Merging histograms](#merging-histograms) above). 

### Plotting histograms

Users can automatically generate a plot of one or multiple `<hist>_<id>_histogram.out` histogram files together. 

```shell
python3 g4see.py plot [-h] [-ht HIST] [-o OUTPUT] [-nf NORMFACTOR] [-nb] data
```
For the list of arguments and help, please run `python3 g4see.py plot -h`.

When generating a plot of multiple
histograms, use the `*` character after the common beginning of histogram name. For example, for *Ekin* histograms, this would be as follows 
```shell
python3 g4see.py plot -ht Ekin*
``` 

```{figure} _static/figures/ekin_histograms.png
Plotting all particle kinetic energy (Ekin) histograms of a run using `g4see.py plot`
```
