..
.. SPDX-FileCopyrightText: © 2022 CERN
.. SPDX-License-Identifier: CC0-1.0
..

.. include:: ../../COPYRIGHT.md
   :parser: myst_parser.sphinx_
