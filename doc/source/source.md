<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Particle Source

## Particle source macro commands

Macro commands related to the primary particle source definition.

All G4 General Particle Source (GPS) macro commands can be used to define primary particles, their attributes and distributions.

Here only few GPS commands are listed briefly. For more commands and details, please go to the [Geant4 GPS documentation](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/GettingStarted/generalParticleSource.html).

| Macro command                            | Type                  | M?  | Description                                                                                                                                                                                  | Example values                                                                                                                                                                                          | Default value     |
|------------------------------------------|-----------------------|-----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|
| `/gps/particle`                          | string                | y   | Incident particle type, using G4 naming convention                                                                                                                                           | `proton`, `neutron`, `ion`, `gamma`, `e+`, `e- `, `mu+`, `mu-`, `pi+`, `pi-`, `pi0`, `alpha`, `deuteron`, `triton`                                                                                      | geantino          |
| `/gps/ion`                               | int[2] (int) (double) | n   | Ion properties after `/gps/particle ion`, Parameters: atomic number *Z*, atomic mass *A*, ionic charge *Q* in $e^{-}$ (optional), excitation energy *E* in keV (optional)                    | `6 12 6`                                                                                                                                                                                                | 1 1 0 0           |
| `/gps/direction`                         | double[3]             | y   | Particle momentum direction (unit vector is not mandatory)                                                                                                                                   | `0 0 -1`                                                                                                                                                                                                | (1,0,0)           |
| `/gps/ene/type`                          | string                | n   | Energy distribution type                                                                                                                                                                     | `Mono` (mono-energetic), `Lin` (linear), `Pow` (power-law), `Exp` (exponential), `Gauss` (Gaussian), `User` (user-defined histogram), `Arb` (point-wise spectrum), `Epn` (energy-per-nucleon histogram) | Mono              |
| `/gps/ene/mono`                          | double u              | n   | Particle kinetic energy (Gaussian or Mono energy)                                                                                                                                            | `5 MeV`                                                                                                                                                                                                 | 1 MeV             |
| `/gps/ene/sigma`                         | double u              | n   | Standard deviation for Gaussian or Mono energy                                                                                                                                               | `100 keV`                                                                                                                                                                                               | 0 keV             |
| `/gps/pos/centre`                        | double[3] u           | y   | Source centre coordinates, Defined point has to be inside the World, but should be outside the Target volume!                                                                                | `0 0 10 um`                                                                                                                                                                                             | (0,0,0) cm        |
| `/gps/pos/type`                          | string                | n   | Source distribution type                                                                                                                                                                     | `Point`, `Beam`, `Plane`, `Surface`, `Volume`                                                                                                                                                           | Point             |
| `/gps/pos/shape`                         | string                | n   | Source shape for Plane, Surface or Volume type source                                                                                                                                        | `Circle`, `Annulus`, `Ellipse`, `Square`, `Rectangle`, `Sphere`, `Ellipsoid`, `Cylinder`, `Para`                                                                                                        | null              |
| `/gps/pos/halfx`; `/gps/pos/halfy`       | double u              | n   | Half length of source in x or y direction                                                                                                                                                    | `1.5 um`                                                                                                                                                                                                | 0 cm              |
| `/gps/pos/sigma_x`; `/gps/pos/sigma_y`   | double u              | n   | Standard deviation of beam position profile in x or y direction                                                                                                                              | `0.5 mm`                                                                                                                                                                                                | 0 cm              |
| `/gps/pos/radius`                        | double u              | n   | Radius of the source or the outer radius for annuli                                                                                                                                          | `1 mm`                                                                                                                                                                                                  | 0 cm              |
| `/gps/pos/sigma_r`                       | double u              | n   | Transverse (radial) standard deviation of beam position profile                                                                                                                              | `0.1 mm`                                                                                                                                                                                                | 0 cm              |
| `/gps/ang/type`                          | string                | n   | Angular distribution type                                                                                                                                                                    | `iso` (isotropic), `cos` (cosine-law), `planar`, `beam1d`, `beam2d`, `focused`, `user` (user-defined)                                                                                                   | iso               |
| `/gps/ang/mintheta`; `/gps/ang/maxtheta` | double u              | n   | Min and Max value for the *θ* distribution, unit: deg or rad                                                                                                                                 | `0.314 rad`                                                                                                                                                                                             | 0 rad; $\pi$ rad  |
| `/gps/ang/minphi`; `/gps/ang/maxphi`     | double u              | n   | Min and Max value for the *ϕ* distribution, unit: deg or rad                                                                                                                                 | `3.1415 rad`                                                                                                                                                                                            | 0 rad; $2\pi$ rad |
| `/gps/ang/sigma_x`; `/gps/ang/sigma_y`   | double u              | n   | Standard deviation of beam directional profile in x or y direction, unit: deg or rad                                                                                                         | `0.314 rad`                                                                                                                                                                                             | 0 rad             |
| `/gps/ang/sigma_r`                       | double u              | n   | Standard deviation of beam directional profile in radial, unit: deg or rad                                                                                                                   | `0.314 rad`                                                                                                                                                                                             | 0 rad             |
| `/gps/hist/type`                         | string                | n   | User-defined histogram type                                                                                                                                                                  | `theta`, `phi`, `energy`, `arb` (point-wise), `epn` (energy per nucleon)                                                                                                                                | -                 |
| `/gps/hist/reset`                        | string                | n   | Re-set the previously specified histogram, it has same options as `/gps/hist/type`                                                                                                           | `phi`                                                                                                                                                                                                   | -                 |
| `/gps/hist/file`                         | string                | n   | Import an arbitrary energy histogram from an ASCII file, the format should be one $E_{hi}\,Weight$ pair per line of the file, same applies for file content as for `/gps/hist/point` entries | `/path/to/hist.txt`                                                                                                                                                                                     | -                 |
| `/gps/hist/point`                        | double[2]             | n   | Specify one entry of a histogram: $E_{hi}\,Weight$                                                                                                                                           | `0.001   123.4`      `0.01    12.34`       `0.1     1.234`                                                                                                                                              | -                 |
| `/gps/hist/inter`                        | string                | n   | Interpolation type for point-wise (*arb* type) spectrum, this command must be issued immediately after the last data point!                                                                  | `Lin` (linear), `Log` (logarithmic), `Exp` (exponential), `Spline` (cubic spline)                                                                                                                       | -                 |
| `/gps/number`                            | int                   | n   | Number of primary particles to simulate per event                                                                                                                                            | `2`                                                                                                                                                                                                     | 1                 |

Please make sure that the centre of particle source (defined by `/gps/pos/centre`) is located outside your Target volume! See geometry visualization on the [Getting Started](getting_started.md#running-g4see-application) page.

Using `/gps/ion`, always define the ion charge too!

With `/gps/direction`, to have a mono-directional particle beam, define (0,0,-1) as direction! 

Regarding *User-defined histograms*: "Histograms are differential functions. Currently histograms are limited to 1024 bins. 
The first value of each user input data pair is treated as the upper edge of the histogram bin and the second value is the bin content. 
The exception is the very first data pair the user input whose first value is the treated as the lower edge of the first bin of the histogram, 
and the second value is not used. This rule applies to all distribution histograms."     
Within histogram file, start lines with `/gps/hist/point ` macro command and bin width has to be uniform either on lin or log scale.
Specify one entry with Weight: $E_{hi}\,Weight$ in a histogram, where $E_{hi}$ is the bin upper edge, and $E_{hi}$ must be in G4 default units (MeV for energy, rad for angle).
```
## User-defined histogram
/gps/ene/type       User        # User-defined histogram
/gps/hist/type      energy
/control/execute    /abs/path/to/spectrum/energy_histogram.mac
```
The `energy_histogram.mac`:
```
# hist points       E_hi(MeV) Weight
/gps/hist/point     0.        1.1
/gps/hist/point     1.        2.2 
/gps/hist/point     2.        3.3 
```


The other option is to define *point-wise spectrum* (differential or integral) with an ASCII file. Interpolation between data points has to be defined.
One entry in hist file for point-wise distribution: $E_{hi}\,Weight$. The abscissa $E_{hi}$ must be in G4 default units (MeV for energy, rad for angle).
```
## Pointwise data with interpolation between data points
/gps/ene/type       Arb         # Point-wise data
/gps/ene/diffspec   true        # true = differential, false = integral
/gps/hist/type      arb         # arb (point-wise histogram)
/gps/hist/file      /abs/path/to/spectrum/pointwise_energy.ascii
/gps/hist/inter     Lin         # interpolation: linear, logarithmic, exp, spline
``` 

When using `/gps/hist/file`, define hist file with an absolute path!

With GPS, multiple source specification is also possible at the same time.
