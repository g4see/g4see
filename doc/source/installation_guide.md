<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Installation Guide

There are two options how to run the G4SEE toolkit: 
- **Option A)** is to run it with Docker - see how to [Run with Docker](#a-run-with-docker). 
- **Option B)** is to clone CERN Gitlab repositories and build G4SEE from the source - see how to [Compile from source](#b-compile-from-source).

## A) Run with Docker
    
Automatically built Docker images are available for G4SEE toolkit in its [GitLab Container Registry](https://gitlab.cern.ch/g4see/g4see/container_registry). 
G4SEE Docker images are Debian-based images, with Geant4 and G4SEE compiled and installed inside, ready to use.

### Dependency

The only dependency is `Docker CE` or `Docker Desktop` (a.k.a Docker). 
Before installing Docker, check if you have all system requirements installed. 

Docker installation steps can be found here:
- [Install Docker Desktop on Linux](https://docs.docker.com/desktop/install/linux-install/)
- [Install Docker Desktop on Mac](https://docs.docker.com/desktop/install/mac-install/)
- [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install/) 

### Pull G4SEE Docker image

```{note}
   Depending on your Docker installation, you may need to use `sudo` for `docker` commands.
```

Pull the **g4see** Docker image, naming the specific Docker image tag you need:

```shell
docker pull gitlab-registry.cern.ch/g4see/g4see:<TAG>
```

Available **g4see** Docker image tags:

| Docker image TAG   | G4SEE version  | Geant4 version | Comment        | 
|--------------------|----------------|----------------|----------------|
| `v1.0_G4-11.2.2`   | v1.0           | 11.2.2         | LATEST RELEASE |
| `v1.0_G4-11.2.1`   | v1.0           | 11.2.1         | LATEST RELEASE |
| `v1.0_G4-11.2.0`   | v1.0           | 11.2.0         | LATEST RELEASE |
| `master_G4-11.2.2` | master         | 11.2.2         | development    |
| `master_G4-11.2.1` | master         | 11.2.1         | development    |
| `master_G4-11.2.0` | master         | 11.2.0         | development    |
| `v0.5.2_G4-11.1.3` | v0.5.2         | 11.1.3         | OLD RELEASE    |
| `v0.5.2_G4-11.1.2` | v0.5.2         | 11.1.2         | OLD RELEASE    |
| `v0.5.2_G4-11.1.1` | v0.5.2         | 11.1.1         | OLD RELEASE    |
| `v0.5.2_G4-11.1.0` | v0.5.2         | 11.1.0         | OLD RELEASE    |

### Run G4SEE Docker container

**To start a container locally** based on the image:
```shell
docker run -it -h g4see gitlab-registry.cern.ch/g4see/g4see:latest
```

**To start a container with a local folder shared between host and container** (mounted to container's `/home`):
```shell
docker run -it -h g4see \
           -v /host/path/to/shared_folder:/home \
           gitlab-registry.cern.ch/g4see/g4see:latest
```

**To start a container with GUI/visualization forwarding:**      

```{note}
   You need to have G4 visualization dependencies installed on your local computer in order to display 
   G4 GUI and visualization locally.
```

- On a Linux host:
```shell
export DISPLAY=:0.0
xhost +local:docker
docker run -it -h geant4 \ 
           -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix \
           gitlab-registry.cern.ch/g4see/g4see:latest
```
- On a Windows host:
  - Enable and use Windows Subsystem for Linux (WSL)    
  - Install *Xming* X11 display server for Windows to run graphical applications with WSL   
  - Allow WSL through Windows Firewall
  - Check IP address of your vEthernet (WSL) adapter with `ipconfig`, then `set-variable -name DISPLAY -value <YOUR_WSL_IP>:0.0`
```shell
docker run -it -h geant4 \
           -e DISPLAY=%DISPLAY% -v /tmp/.X11-unix:/tmp/.X11-unix \ 
           gitlab-registry.cern.ch/g4see/g4see:latest
```
- On a Mac host:
  - Install XQuartz: `brew install --cask xquartz`
  - Run XQuartz: `open -a XQuartz`
    - Open preferences, go to the "Security" tab and check "Allow connections from network clients"
    - Leave XQuartz running
  - Back in the terminal: `ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}'); xhost + $ip`
  - **NOTE:** There is a resolution limitation in XQuartz.
```shell
docker run -it -h geant4 \ 
           -e DISPLAY=$ip:0 -v /tmp/.X11-unix:/tmp/.X11-unix \
           gitlab-registry.cern.ch/g4see/g4see:latest
```

Paths inside container:
- G4SEE source directory: `/opt/g4see/` (`G4SEE_SRC` environment variable)
- G4SEE build directory: `/opt/g4see/build` (`G4SEE_BUILD` environment variable) 
- Geant4 installation: `/opt/geant4/install/lib/Geant4-<version>` (`G4LIB` environment variable)
- Geant4 examples: `/opt/geant4/install/share/Geant4-<version>/examples/`

Tools available in container:
- `G4SEE`
- `Geant4`
- `g++`, `gcc`, `cmake`, `make`
- `git`, `curl`, `nano`, `htop`
- `python3.10` with `numpy`, `pandas`, `matplotlib`, `pyYAML`

See [Getting Started](getting_started.md) section how to run the G4SEE toolkit.

--- 

## B) Compile from source

### Dependencies

* **Git** (recommended: latest stable version)
* **Geant4** (>= 11.0.0, recommended: latest stable version)
* **CMake** (>= 3.17, recommended: latest stable version)
* **Python** (>= 3.8, recommended: latest stable version)
* Python package dependencies can be found in `g4see-scripts/requirements.txt`

### Get the source code

Get the latest, stable version (master branch or a specific tag) from CERN GitLab:

* clone the repo (you are automatically on master branch):  
  ```shell
  git clone --recursive https://gitlab.cern.ch/g4see/g4see.git
  ```
  * Please note: `--recursive` option is needed to clone embedded git repositories (git submodules) as well
  * Other method:        
    ```shell
    git clone https://gitlab.cern.ch/g4see/g4see.git
    git submodule init
    git submodule update --remote
    ```
  * (optional) switch to a specific tag (release): ```git checkout tags/v1.0```
  * (optional) switch to a specific branch: ```git checkout <branch>```
  * pull latest changes from remote repository: ```git pull```

### Build & Compile

Build command-line with `cmake`, then compile with `make`. Starting from your local `g4see/` repository root folder:

```shell
source <Geant4_installation_path>/bin/geant4.sh
export G4LIB=<Geant4_installation_path>/lib64/Geant4-<version>/

mkdir build && cd build
cmake -DGeant4_DIR=$G4LIB ..
make -j [number_of_jobs]
sudo make install
```

- To compile a single-threaded G4SEE app (instead of a multi-threaded), use optional `-DMULTITHREADED=False` flag for `cmake` (default: `True`)
- `make install` is optional, it copies `g4see` and `mergeHistograms` executables into `/usr/local/bin/` directory

**Executables:**
- `build/g4see`
- `build/mergeHistograms`
- `build/scripts/g4see.py {submit,delete,view,merge,plot}`
  - `submit`              Submit jobs to cluster nodes
  - `delete`              Delete jobs submitted to cluster nodes
  - `view`                Visualize geometry from macro file
  - `merge`               Merge histogram files recursively
  - `plot`                Plot histograms from files

See [Getting Started](getting_started.md) section how to run the G4SEE toolkit.
