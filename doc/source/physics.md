<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Physics & Biasing

## Physics macro commands

Macro commands related to Electromagnetic and Hadronic physics of the simulation.

See brief descriptions of the different available physics modules and models in the [Physics List](#physics-list) section.

| Macro command                     | Type           | M?  | Description                                                   | Example value  | Default value                     |
|-----------------------------------|----------------|-----|---------------------------------------------------------------|----------------|-----------------------------------|
| `/SEE/physics/addPhysics`         | string         | n   | Adding a G4 Physics module                                    | `G4IonPhysics` | See [Physics List](#physics-list) |
| `/SEE/physics/removeProcess`      | string, string | n   | Remove a process of a particle type                           | `e-, eIoni`    | -                                 |

```{caution}
   Define physics with caution, since missing (not added or removed) physics models/processes can lead to wrong conclusions!
```

## Cut macro commands

### Production range cuts

Macro commands related to secondary particle production range cuts.

| Macro command                | Type             | M?  | Description                                                               | Example value | Default value                |
|------------------------------|------------------|-----|---------------------------------------------------------------------------|---------------|------------------------------|
| `/cuts/setLowEdge`           | double u         | n   | Lower limit of production threshold when range cut is converted to energy | `100 eV`      | 990 eV                       |
| `/SEE/cuts/gammaCutWorld`    | double u         | n   | Production range cut for $\gamma$ in World region                         | `2.5 mm`      | 1 m                          |
| `/SEE/cuts/electronCutWorld` | double u         | n   | Production range cut for $e^-$ in World region                            | `100 um`      | 1 m                          |
| `/SEE/cuts/positronCutWorld` | double u         | n   | Production range cut for $e^+$ in World region                            | `10 um`       | 1 m                          |
| `/SEE/cuts/hadronsCutWorld`  | double u         | n   | Production range cut for $p$ and other hadrons in World region            | `200 nm`      | 1 mm                         |
| `/SEE/cuts/allCutsWorld`     | double u         | n   | Production range cut for all particles in World region                    | `1 um`        | -                            |
| `/SEE/cuts/gamma`            | string, double u | n   | Production range cut for $\gamma$ in the selected region                  | `SV 2.5 mm`   | 1 mm for Bulk, SV & BEOL     |
| `/SEE/cuts/electron`         | string, double u | n   | Production range cut for $e^-$ in the selected region                     | `Bulk 100 um` | 1 $\mu$m for Bulk, SV & BEOL |
| `/SEE/cuts/positron`         | string, double u | n   | Production range cut for $e^+$ in the selected region                     | `BEOL 10 um`  | 1 $\mu$m for Bulk, SV & BEOL |
| `/SEE/cuts/hadrons`          | string, double u | n   | Production range cut for $p$ and other hadrons in the selected region     | `SV 200 nm`   | 1 nm for Bulk, SV & BEOL     |

Production range cuts are converted to kinetic energy threshold internally by Geant4.

Default production cuts (both in range and energy values) used for Bulk, SV and BEOL regions (in this case in Silicon material):
```
Index : 1     used in the geometry : Yes
 Material : G4_Si
 Range cuts        :  gamma  1 mm           e-  1 um        e+  1 um      proton 1 nm 
 Energy thresholds :  gamma  6.93672 keV    e-  990 eV      e+  990 eV    proton 100 meV
 Region(s) which use this couple : 
    BEOL
    Bulk
    Sensitive
```

```{attention}
   Set suitable production range cuts for your regions! Lower production range cuts can result in more accurate simulation, but significantly increase running time. 
```

### Tracking cuts

Macro commands related to particle tracking cuts.

| Macro command                      | Type      | M?  | Description                         | Example value | Default value      |
|------------------------------------|-----------|-----|-------------------------------------|---------------|--------------------|
| `/process/em/lowestElectronEnergy` | double u  | n   | Tracking cut for $e^-$ and $e^+$    | `50 eV`       | 100 eV or 1 keV \* |
| `/process/em/lowestMuHadEnergy`    | double u  | n   | Tracking cut for $\mu$ and hadrons  | `10 eV`       | 1 keV              |

In all ionization processes which simulate energy loss along step, tracking cuts are the lowest energy limit which forces full 
energy deposition of electrons and positrons (or muons and hadrons) at a step independently on material.

\* Default value is 100 eV for `G4EmStandardPhysics_option4` (default EM physics), `G4EmStandardPhysics_option3`, `G4EmPenelopePhysics` and `G4EmLivermorePhysics`, otherwise default value is 1 keV.

```{attention}
   Set suitable tracking cuts! Lower tracking cuts can result in more accurate simulation, but significantly increase running time. 
```


```{seealso}
   [Geant4 Low Energy Electromagnetic Physics](https://geant4-internal.web.cern.ch/node/1620) 
```

## Biasing macro commands

Macro commands related to microscopic cross-section (XS) biasing of particle interactions. 

```{caution}
   When biasing is used, the simulation becomes a non-analog Monte Carlo simulation!
   Use with caution, since over- and under-biasing can distort simulation results and therefore lead to wrong conclusions!
```

| Macro command                      | Type   | M?  | Description                                                                                   | Example values                              | Default value |
|------------------------------------|--------|-----|-----------------------------------------------------------------------------------------------|---------------------------------------------|---------------|
| `/SEE/biasing/biasParticle`        | string | n   | Particle name to apply XS biasing for                                                         | `proton`, `neutron`, `alpha`, `gamma`, etc. | -             |
| `/SEE/biasing/biasProcess`         | string | n   | Particle process name to be biased, can be used multiple times after `biasParticle` command   | `protonInelastic`, `hadElastic`, etc.       | -             |
| `/SEE/biasing/biasFactor`          | double | n   | XS bias weight factor per particle (>0.)                                                      | `1000`                                      | -             |
| `/SEE/biasing/biasNonPrimariesToo` | bool   | n   | Apply XS bias for non-primary particles as well, by default only primary processes are biased | `true`                                      | false         |
 
- Multiple processes of particle species can be biased by using `/SEE/biasing/biasProcess` multiple times after the `/SEE/biasing/biasParticle`
- By default, only primary particles are biased
- For biasing particles that are non-primary particles, enable this by the `/SEE/biasing/biasNonPrimariesToo` command
- Multiple particle species can be also biased by using a `biasParticle` command per each particle, and then the related `biasProcess` to define at least one process,
  plus the `biasNonPrimariesToo` is needed too 

The Standard Scoring output histograms always contain the bias weight corrected data, so there is nothing to do with these histograms from a biasing point of view.
The Detailed Scoring Hits output data is not corrected, however contains the inverse biasing weight of each particle and event in separate column (`weight`), 
which is equals to 1, when XS biasing is not used. For more, see [Output Files](output_files.md).

## Particles

By default, all the particles available in Geant4 are defined in *G4SEE* (including short-lived, exotic particles and antiparticles).

Geant4 naming convention is applicable everywhere, where particles could be defined or referenced.

Ions/nuclei heavier than *alpha* are *GenericIon* particles (defined by $Z$ atomic and $A$ nucleon numbers, and optionally $Q$ ionic charge). 

## Physics List

*G4SEE* Physics list is a Modular Physics list, which can be build up from different (e.g. EM, elastic hadronic, inelastic hadronic, etc.) physics modules.

Physics modules are sets of physics models implemented for different particle processes (interactions).

One can add these physics modules listed below via the `/SEE/physics/addPhysics` macro command.

```{note}
Running time (or CPU time) of G4 strongly depends on the physics modules used. One can sacrifice some accuracy and precision in order to improve the performance and run the simulation faster.
```

### Electromagnetic physics

One of the EM modules (listed below) is mandatory and only one can be used for a run.

- `G4EmStandardPhysics_option4` - **DEFAULT, RECOMMENDED** (added and used by default, no need to add)
  - Latest and most accurate EM physics models and settings
  - [G4 doc / EM Opt4](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/electromagnetic/Opt4.html)
- `G4EmStandardPhysics_option3`
  - (was) used for medical and space applications, accurate, but not as accurate as `EM Opt4` 
  - [G4 doc / EM Opt3](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/electromagnetic/Opt3.html)
- `G4EmStandardPhysics_option2` - **Do not use!**
  - for HEP (calorimeters), fast but not precise, nor accurate, used by LHCb
- `G4EmStandardPhysics_option1` - **Do not use!**
  - for HEP (calorimeters), fast but not precise, nor accurate, used by CMS
- `G4EmStandardPhysics_option` - **Do not use!**
  - used by ATLAS
- `G4EmPenelopePhysics`
  - Specific low-energy Penelope models are used for $\gamma$, $e^{\pm}$ between 250 eV and 1 GeV energy
  - Above 1 GeV and for all other charged particle, it is the same as the `EM Opt4`
- `G4EmLivermorePhysics`
  - Low-energy EM, >250 eV energy
  - [G4 doc / Livermore](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/electromagnetic/Liv.html)
- `G4MicroElecPhysics` - **EXPERIMENTAL** (not recommended yet)
  - Low-energy microdosimetry for Silicon only, >16.7 eV energy
  - This module is limited to the Sensitive volume only
  - Extremely CPU time-consuming

### Hadron & Ion Elastic physics

XS = Cross-Section; FS = Final State

Elastic physics modules for hadrons or heavy ions. Only one can be chosen from the elastic modules denoted by (\*). 

- `G4HadronElasticPhysics` (\*)
  - Hadron nuclear elastic process for all hadrons (except *GenericIon*)
  - *proton:* Barashenkov-Glauber-Gribov XS; Chips FS
  - *neutron:* G4NeutronElasticXS; Chips FS
- `G4HadronElasticPhysicsHP` (\*) - **recommended for neutrons**
  - Same as `G4HadronElasticPhysics`, but for *neutron < 20 MeV:* NeutronHP XS & FS
- `G4HadronDElasticPhysics` (\*)
  - Hadron nuclear elastic process for all hadrons (except *GenericIon*)
  - *proton:* Barashenkov-Glauber-Gribov XS; Diffuse model FS where applicable, else Gheisha
  - *neutron:* G4NeutronElasticXS; Diffuse model FS where applicable, else Gheisha
- `G4HadronHElasticPhysics` (\*)
  - Hadron nuclear elastic process for all hadrons (except *GenericIon*)
  - *proton:* Barashenkov-Glauber-Gribov XS; Diffuse model FS (except for Hydrogen where Chips is used)
  - *neutron:* G4NeutronElasticXS; Diffuse model FS (except for Hydrogen where Chips is used)

- `G4IonElasticPhysics` - **recommended for heavy ions**
  - Elastic process only for *GenericIon*: Glauber-Gribov XS and Diffuse model FS

- `G4ThermalNeutrons` - **recommended for thermal neutrons**
  - High Precision thermal scattering model, only for *neutron < 4 eV*
  - Based on thermal scattering data of evaluated nuclear data libraries (ENDF/B-VI, Release 2) 
  - Materials (elements): *TS_Aluminium_Metal (Al), TS_Beryllium_Metal (Be), TS_Iron_Metal (Fe), G4_BERYLLIUM_OXIDE (Be, O), G4_GRAPHITE (C), G4_POLYETHYLENE (H), G4_URANIUM_OXIDE (U, O), G4_WATER (H)* (only the relevant ones are listed here)
  - Should be added as the last (elastic, inelastic) hadronic physics module, since it overwrites any previously added neutron model between 0-4 eV 

### Hadron Inelastic physics

Inelastic physics modules are combinations of several models applicable on different energy ranges. The major models are:

- **QGS**: Quark-Gluon string models (>15 GeV)
- **FTF**: Fritiof Parton model (>5 GeV)
- **BIC**: Binary Cascade model (<10 GeV)
- **BERT**: Bertini Intranuclear Cascade model (<10 GeV)
- **INCLXX**: Liège Intranuclear Cascade model (<10 GeV)
- **P**: Precompound/de-excitation model
- **HP**: High Precision neutron model (<20 MeV)

Inelastic physics modules have a specific naming convention, indicating the consisting sub-modules: `<String(s)>_<PreComp>_<Cascade>_<Neutron>`. The same naming convention is also used by the Reference (pre-packaged) Physics lists (not applicable to *G4SEE*).

It is not trivial which one to use and there is no best option to choose in general. It is heavily depends on the actual simulation case or study. It is strongly recommended to run the simulation several times adding different inelastic modules for comparison. In most cases, they result the same or very similar distribution, but there could be a discrepancy in the higher energy range of the $E_{dep}$ distribution (hadronic tail).

Modules containing HP neutron model (their names ending with `HP`) are **recommended for neutrons**.

Only one can be chosen from the inelastic modules listed below. All of them are optional.

- `G4HadronInelasticQBBC`
  - Recommended for accurate simulation of low-energy transport of *protons* and *neutrons*. Best agreement below 1 GeV for thin target experiments, It is also recommended for medical and space applications
  - For higher energies it is the same as the `FTFP_BERT`
  - [G4 doc / QBBC (inelastic part only)](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/reference_PL/QBBC.html)
- `G4HadronPhysicsFTFP_BERT`
  - Recommended for cosmic rays where good treatment of very high energy particles is required, but only below 10 TeV
  - Recommended for collider physics applications, best agreement with test beam calorimeter data, including shower shape, energy response and resolution
  - [G4 doc / FTFP_BERT (inelastic part only)](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/reference_PL/FTFP_BERT.html)
- `G4HadronPhysicsFTFP_BERT_HP`
  - Same as `FTFP_BERT`, but also including High Precision neutron models (<20 MeV)
  - [G4 doc / FTFP_BERT (inelastic part only)](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/PhysicsListGuide/html/reference_PL/FTFP_BERT.html)
- `G4HadronPhysicsFTFP_BERT_ATL`
- `G4HadronPhysicsFTFQGSP_BERT`
- `G4HadronPhysicsFTF_BIC`
- `G4HadronPhysicsINCLXX`
- `G4HadronPhysicsQGSP_BERT`
- `G4HadronPhysicsQGSP_BERT_HP`
- `G4HadronPhysicsQGS_BIC`
- `G4HadronPhysicsQGSP_BIC`
- `G4HadronPhysicsQGSP_BIC_HP`
- `G4HadronPhysicsQGSP_FTFP_BERT`

#### High Precision model settings  

For inelastic neutron-carbon interactions, the following commands should be used (see `examples/diamond_detector.mac`).
This enables the NRESP71 model for neutron-carbon interactions, which is not enabled (false) by default.
```shell
/process/had/particle_hp/do_not_adjust_final_state    true
/process/had/particle_hp/use_NRESP71_model            true
```

All HP model related commands with their default values:
```shell
# Force the use of the Photon Evaporation model, instead of the neutron capture final state data.
/process/had/particle_hp/use_photo_evaporation        false
# Use only exact isotope data files, instead of allowing nearby isotope files to be used. 
# In this case if the exact file is not available, the cross section will be set to zero. 
/process/had/particle_hp/skip_missing_isotopes        false
# Switch off the Doppler broadening due to the thermal motion of the target nucleus. 
# This option provides a significant CPU performance advantage. 
/process/had/particle_hp/neglect_Doppler_broadening   false
# Disable to adjust final state for getting better conservation.
/process/had/particle_hp/do_not_adjust_final_state    false
# Enable to generate fission fragments.
/process/had/particle_hp/produce_fission_fragment     false
# Enable use of Wendt fission model. 
/process/had/particle_hp/use_Wendt_fission_model      false
# Enable to use NRESP71 model for n on C reaction.      
/process/had/particle_hp/use_NRESP71_model            false
# Set Verbose level of ParticleHP package.
/process/had/particle_hp/verbose                      1
```

### Ion Inelastic physics

Inelastic ion-ion processes for *deuteron*, *triton*, *He3*, *alpha* and *GenericIon* projectiles. Only one can be chosen from the ion inelastic modules listed below. All of them are optional.

- `G4IonPhysics`
  - With Glauber-Gribov XS and Binary Light Ion (BIC, with Precompound/de-excitation) and FTFP (Fritiof string model with Precompound/de-excitation) for the final state. BIC is used for projectiles of kinetic energies below 6 GeV/nucleon, and FTF above 3 GeV/nucleon.
- `G4IonQMDPhysics`
  - With Glauber-Gribov XS and  BIC, QMD and FTFP for the final state. These three final-state models are used in the following intervals of projectile kinetic energy: BIC below 110 MeV/nucleon, QMD between 100 and 10 000 MeV/nucleon, and FTF above 9990 MeV/nucleon.
- `G4IonINCLXXPhysics`
  - With Glauber-Gribov XS and INCLXX and FTFP for the final state. INCLXX is used below 3 GeV/nucleon, and FTF above 2.9 GeV/nucleon.

### Decay physics

Decay processes of unstable particles

- `G4DecayPhysics` - **DEFAULT, MANDATORY** (used by default and can not be removed)
- `G4RadioactiveDecayPhysics` (optional)

### Transportation

- `Transportation` - **DEFAULT, MANDATORY** (used by default and can not be removed)

