<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Scoring

Macro commands related to scoring of quantities within the sensitive volume. Sensitive volume by default is defined inside the Bulk volume.

## Standard Scoring commands 

Quantities which can be scored:
- *Edep*: (event-by-event) total energy deposited per event inside SV.     
- *Ekin*: (hit-by-hit) kinetic energy of a particle in SV. 
- *LET*: (event-by-event or hit-by-hit) linear energy transfer in SV.

### Energy Deposition – *Edep* 

```shell
# Add scoring                       ID
/SEE/scoring/standard/Edep          1       
# Set histogram for last scoring    SCALE   BINS        LOWLIM      UPLIM   (UNIT)
/SEE/scoring/setHistogram           lin     200         0           10      MeV
```

| `/SEE/scoring/standard/Edep` parameters | Type   | M?  | Description                    | Example value | Default value |
|-----------------------------------------|--------|-----|--------------------------------|---------------|---------------|
| ID                                      | int    | y   | Scoring and histogram file ID  | `0`, `1`, `2` | -             |

* Unit of scored quantity: *MeV* 
* Counts include the event biasing weights

### Kinetic Energy – *Ekin* 

```shell
# Add scoring                       ID      (PARTICLE)  (STEP)
/SEE/scoring/standard/Ekin          2       neutron     first
# Set histogram for last scoring    SCALE   BINS        LOWLIM      UPLIM   (UNIT)
/SEE/scoring/setHistogram           log     200         0.1         1000    keV
```

| `/SEE/scoring/standard/Ekin` parameters | Type   | M?  | Description                   | Example value          | Default value |
|-----------------------------------------|--------|-----|-------------------------------|------------------------|---------------|
| ID                                      | int    | y   | Scoring and histogram file ID | `0`, `1`, `2`          | -             |
| (PARTICLE)                              | string | n   | Particle(s) to score          | `proton`, `Si28`, `e-` | `primary`     |
| (STEP)                                  | string | n   | Particle steps in SV to score | `first`, `last`        | `first`       |

* Unit of scored quantity: *MeV*
* *PARTICLE* parameter:
  * `primary`: only primary particle
  * `non-primary`: all particles excluding primary particle
  * `all`: all particles
  * or one can filter particle species by their name
* *STEP* parameter:
  * `first`: only first step in SV (either entering into SV or produced inside SV)
  * `last`: only last step in SV (just before leaving SV)
  * `all`: all steps in SV (normalized by the number of steps per particle)
* Counts include the hit biasing weights

### Linear Energy Transfer – *LET* 

```shell
# Add scoring                       ID      (OPTION)    (PARTICLE)  (STEP)
/SEE/scoring/standard/LET           3       Opt1        primary     first
# Set histogram for last scoring    SCALE   BINS        LOWLIM      UPLIM
/SEE/scoring/setHistogram           log     100         0.01        100
```

| `/SEE/scoring/standard/LET` parameters | Type   | M? | Description                                              | Example value          | Default value |
|----------------------------------------|--------|----|----------------------------------------------------------|------------------------|---------------| 
| ID                                     | int    | y  | Scoring and histogram file ID                            | `0`, `1`, `2`          | -             |
| (OPTION)                               | string | n  | Type of LET to score                                     | `Opt1`, `Opt2`, `Opt3` | `Opt1`        |
| (PARTICLE)                             | string | n  | Particle(s) to score, only for `Opt2` or `Opt3`          | `all`, `non-primary`   | `primary`     |
| (STEP)                                 | string | n  | Particle steps in SV to score, only for `Opt2` or `Opt3` | `first`, `last`        | `first`       |

* Unit of scored quantity: *MeV\*cm^2/mg* 
* *OPTION* parameter: 
  * `Opt1`: LET scored in every event, LET = (Edep per event) / (primary track length)
  * `Opt2`: LET scored along every step (hit), calculated by `G4EmCalculator` class of Geant4
  * `Opt3`: LET scored along every step (hit), LET = (Edep per step) / (step length)
* *PARTICLE* parameter (for `Opt2` or `Opt3`):
  * `primary`: only primary particle
  * `non-primary`: all particles excluding primary particle
  * `all`: all particles
  * or one can filter particle species by their name
* *STEP* parameter (for `Opt2` or `Opt3`):
  * `first`: only first step in SV (either entering into SV or produced inside SV)
  * `last`: only last step in SV (just before leaving SV)
  * `all`: all steps in SV (normalized by the number of steps per particle)
* Counts include the event or hit biasing weights

### Histograms

Directly after *Edep*, *Ekin* or *LET* scoring, one histogram should be defined for each scoring as well:

| `/SEE/scoring/setHistogram` parameters | Type     | M? | Description                     | Example value      | Default value |
|----------------------------------------|----------|----|---------------------------------|--------------------|---------------|
| SCALE                                  | string   | y  | Scale of histogram binning      | `log`, `lin`       | -             |
| BINS                                   | int      | y  | Number of histogram bins        | `300`              | -             |
| LOWLIM                                 | double   | y  | Lower limit of histogram range  | `0.1`              | -             |
| UPLIM                                  | double u | y  | Upper limit of histogram range  | `1000`             | -             |
| (UNIT)                                 | string   | n  | Unit of range limits            | `eV`, `keV`, `GeV` | it depends    |

The `setHistogram` command always sets the following values for the scoring latest added.

Scored quantity is saved to an output histogram file. For details, see [Output Files](output_files.md).

No default values for parameters.

| Other macro commands                    | Type | M?  | Description                                                        | Example value | Default value |
|-----------------------------------------|------|-----|--------------------------------------------------------------------|---------------|---------------|
| `/SEE/scoring/dumpHistogramsAfter`      | int  | n   | Save histograms to file regularly, after every N number of events  | `100000`      | -             |

By default, scored histograms saved into file only at the end of run. Using `dumpHistogramsAfter` command, one can override this.

## Detailed Scoring commands

When Detailed Scoring (DS) feature of *G4SEE* is enabled, each thread saves particle hits of SV particle-by-particle to a "Hits" output file with `Hits_t<N>.out` filename.

- During Detailed Scoring, only the $e^-$, $e^+$ and $\gamma$ particles ABOVE a certain kinetic energy ($E_{kin}$) thresholds are scored and then printed individually to the Hits output file 
- These $E_{kin}$ thresholds for $e^-$, $e^+$ and $\gamma$ can be set via the related `setThreshold` commands
- $E_{kin}$ thresholds can also be set to 0, which means all of these particles independently of their energy are scored and printed to file individually, 
  potentially causing very large output file (because of the thousands of ionization  $e^-$), use with caution!
- The $e^-$, $e^+$ and $\gamma$ particles with energy BELOW their $E_{kin}$ threshold are grouped together per event and per particle species, 
  and printed together as a group into a single line of the Hits DS output file, indicating the group with a `g` letter
- If ancestor grouping (via `groupByAncestor` command) is enabled for $e^-$, $e^+$ or $\gamma$, these particles are grouped by their 
  ancestor particles using their parent IDs and track IDs, resulting a more verbose DS output 
- Hits output files can grow very fast in file size, so they can easily take up huge amount of disk space!

| Other macro commands                          | Type     | M?  | Description                                                     | Example value | Default value |
|-----------------------------------------------|----------|-----|-----------------------------------------------------------------|---------------|---------------|
| `/SEE/scoring/detailed`                       | bool     | n   | Enable/disable Detailed Scoring                                 | `true`        | False         |
| `/SEE/scoring/detailed/printPrimary`          | bool     | n   | Print primary particles to DS Hits output file                  | `true`        | True          |
| `/SEE/scoring/detailed/setCSVFormat`          | bool     | n   | Set CSV file format for DS Hits output file                     | `true`        | True          |
| `/SEE/scoring/detailed/dumpMemoryLimit`       | double u | n   | Exceeding this memory limit, DS output is dumped into Hits file | `1 MB`        | 10 MB         |
| `/SEE/scoring/detailed/dumpEventLimit`        | int      | n   | Exceeding this event limit, DS output is dumped into Hits file  | `100`         | (not used)    |
| `/SEE/scoring/detailed/e-/setThreshold`       | double u | n   | $E_{kin}$ threshold for individual $e^-$ scoring                | `20 keV`      | 10 keV        |
| `/SEE/scoring/detailed/e+/setThreshold`       | double u | n   | $E_{kin}$ threshold for individual $e^+$ scoring                | `20 keV`      | 10 keV        |
| `/SEE/scoring/detailed/gamma/setThreshold`    | double u | n   | $E_{kin}$ threshold for individual $\gamma$ scoring             | `1 MeV`       | 100 keV       |
| `/SEE/scoring/detailed/e-/groupByAncestor`    | bool     | n   | Enable ancestor grouping for $e^-$ below $E_{kin}$ threshold    | `true`        | False         |
| `/SEE/scoring/detailed/e+/groupByAncestor`    | bool     | n   | Enable ancestor grouping for $e^+$ below $E_{kin}$ threshold    | `true`        | False         |
| `/SEE/scoring/detailed/gamma/groupByAncestor` | bool     | n   | Enable ancestor grouping for $\gamma$ below $E_{kin}$ threshold | `true`        | False         |

With commands below, one can enable/disable optional scored quantities to be printed to Hits file:

| Other macro commands                | Type | M?  | Description                                               | Example value | Default value |
|-------------------------------------|------|-----|-----------------------------------------------------------|---------------|---------------|
| `/SEE/scoring/detailed/addTrack`    | bool | n   | Print particle track ID to Hits file                      | `true`        | True          |
| `/SEE/scoring/detailed/addParent`   | bool | n   | Print particle parent ID to Hits file                     | `true`        | True          |
| `/SEE/scoring/detailed/addEdep`     | bool | n   | Print particle total energy deposited to Hits file        | `true`        | True          |
| `/SEE/scoring/detailed/addEkin`     | bool | n   | Print particle kinetic energy to Hits file                | `true`        | True          |
| `/SEE/scoring/detailed/addCounts`   | bool | n   | Print particle counts to Hits file                        | `true`        | True          |
| `/SEE/scoring/detailed/addZ`        | bool | n   | Print particle atomic number to Hits file                 | `true`        | False         |
| `/SEE/scoring/detailed/addA`        | bool | n   | Print particle mass (nucleon) number to Hits file         | `true`        | False         |
| `/SEE/scoring/detailed/addPosition` | bool | n   | Print particle position vector (x,y,z) to Hits file       | `true`        | False         |
| `/SEE/scoring/detailed/addMomentum` | bool | n   | Print particle momentum vector (x,y,z) to Hits file       | `true`        | False         |
| `/SEE/scoring/detailed/addProcess`  | bool | n   | Print particle creator process to Hits file               | `true`        | False         |
| `/SEE/scoring/detailed/addVolume`   | bool | n   | Print particle creator volume to Hits file                | `true`        | False         |
| `/SEE/scoring/detailed/addNdep`     | bool | n   | Print particle non-Ionizing energy deposited to Hits file | `true`        | False         |
| `/SEE/scoring/detailed/addEexc`     | bool | n   | Print particle excitation energy to Hits file             | `true`        | False         |

For more details about Detailed Scoring output, see [Output Files](output_files.md).

## Deposited energy quantities 

- *Edep* (or $E_{dep}$, `E_dep`): Total energy deposited during steps, which in *G4SEE* is summed for all steps in SV
  - the Total energy deposited per step is the sum of:
    - the energy deposited by the energy loss process, and
    - the energy lost by secondaries which have NOT been generated because each of their energies was below the cut threshold
  - *Ndep* Non-ionizing energy deposited is INCLUDED in the *Edep* Total energy deposited
- *Ndep* (or $N_{dep}$, `N_dep`): Non-ionizing energy deposited (or Non-ionizing energy loss (NIEL)) during steps, which in *G4SEE* is summed for all steps in SV 
  - In G4, the Non-ionizing energy loss (NIEL) is an estimation of energy loss of p/ion transferred to an atomic nucleus due to multiple scattering
  - Models calculating NIEL: G4NuclearStopping (used by EM_opt4 and EM_opt3), G4HadronElasticProcess, G4xCoulombScatteringModel (x = e, h, eSingle, Ion), G4PhononReflection
  - In EM_opt4 and EM_opt3 physics modules, NIEL computation is enabled for protons and ions with kinetic energy below 1 MeV/nucleon
  - For elastic processes, NIEL is controlled by "Range cut for proton"
    - If recoil energy below this threshold, then recoil ion is not tracked by G4, but the energy is added to total and non-ionizing energy deposition
    - If this value is set to zero then non-ionizing energy will be zero and all recoil ions will be tracked
    - If recoil ion is tracked by G4, the effective charge approach is used
- All the energies deposited by the generated (and therefore tracked) secondary particles are EXCLUDED from Total energy deposited by primary particle

```{seealso} 
- [G4 User's Guide: For Application Developers](http://cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/fo/BookForApplicationDevelopers.pdf) 
- V. Ivanchenko: "Radiation background simulation capabilities of Geant4" (presentation at CERN, February 2019)
```