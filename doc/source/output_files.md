<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Output Files

## Stdout and Stderr

Important to check during macro testing and before long runs! 
The standard output (stdout) contains useful G4 warning messages, which warns the user about bad macro settings, like 
volume overlaps, ignored parameters, which do not raise error, so code can run with unintentional, wrong settings.

By default, this is printed to command-line, but when submitting to a typical computer cluster node, 
this is usually redirected automatically to a file (`g4see_<#>.sh.o<JobID>` or similar). 
It can be also intentionally redirected by user with the `>` bash operator to a file.  

The standard error (stderr) messages (resulted e.g. by non valid macro commands) are printed when *G4SEE* terminates with an error.
These are also printed to command-line, or usually redirected automatically to file (`g4see_<#>.sh.e<JobID>` or similar) on typical computer clusters. 

## Event counter file

The file `n_event_t<N>.out` (where `<N>` is the thread ID) is being updated regularly during simulation by printing the number of already simulated events per thread. 
This is useful when *G4SEE* is running on a cluster node, and the user has no access to the standard output (stdout) file containing information on the simulation progress.

## The g4see.out output file

Some info about run (date, *G4SEE* version, CPU time, I/O) and input macro. This file is only printed at the end of the simulation run.

```
#####################################################################################
#                                 G4SEE OUTPUT FILE                                 #
#####################################################################################

Simulation started:           2022/09/19 16:30:46

Geant4 release:               geant4-11-00-patch-02 [MT]   (25-May-2022)

G4SEE version:                v0.5
G4SEE tag:                    v0.5
G4SEE commit ID:              06822d21
G4SEE branch:                 master

Multi-Threaded mode
Number of threads used:       2

Number of primary particles:  100

Total CPU time (s):           18.6748
CPU time / primary (s):       0.186748

Output directory:             /home/work/g4see/build/output
Input macro file:             /home/work/g4see/build/examples/detailed_scoring.mac

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+                               Input Macro Commands                                +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/run/numberOfThreads      2

##############
### Geometry
# BULK                    MATERIAL     WIDTH unit  THICK unit  BIAS
/SEE/geometry/Bulk        G4_Si        20 mm       300 um      true
...
```

## Standard Scoring Histograms

At the end of runs, output histogram files are created by each job (or thread if applicable) in output folder of that specific job (e.g. `out_0`, `out_1`, ...).
Histograms can be dumped to file regularly during a simulation run, which can be controlled via the `/SEE/scoring/dumpHistogramsAfter` command.

Naming of histogram files: `<quantity>_<id>_hist_t<N>.out`
* `<quantity>`: `Edep`, `Ekin`, `LET`
* `<id>`: scoring (histogram) ID defined by user via `/SEE/scoring/standard/<quantity>` macro command    
* `<N>`: thread ID run simulation and created the file

In both single- and multi-threaded mode (by default), these histograms have to be merged manually after run, see [Merging histograms](getting_started.md#merging-histograms).

Histogram types:
* *Edep*: **Energy Deposition histogram**
  * Enable via `/SEE/scoring/standard/Edep  <id>` macro command
  * **Bins:** Total energy deposited event-by-event in sensitive volume, unit: *MeV* 
  * **Counts:** Number of occurrence multiplied by event biasing weights
* *Ekin*: **Kinetic Energy histogram**
  * Enable via `/SEE/scoring/standard/Ekin  <id> <particle> <step>` macro command
  * **Bins:** Particle kinetic energy (Ekin) hit-by-hit in sensitive volume, unit: *MeV* 
  * **Counts:** Number of occurrence multiplied by particle hit biasing weights
* *LET*: **Linear Energy Transfer histogram**
  * Enable via `/SEE/scoring/standard/LET  <id> <option> <particle> <step>` macro command
  * **Bins:** Linear Energy Transfer (LET) event-by-event or hit-by-hit in sensitive volume, unit: *MeV\*cm^2/mg* 
  * **Counts:** Number of occurrence multiplied by event or hit biasing weights

Standard scoring histograms in their header also contains 1 underflow bin (below scoring range) and 1 overflow bin (above scoring range), 
therefore histograms contain all the events, even outside scoring range.

Example of a job/thread `Edep_<id>_hist_t<N>.out` file:
```
           Edep         Counts
      underflow      9.000e+00
       overflow              0
      1.000e-01      6.000e+00
      1.023e-01      1.400e+01
      1.047e-01      4.700e+01
      1.072e-01      1.010e+02
      1.096e-01      1.780e+02
      1.122e-01      2.780e+02
      1.148e-01      3.730e+02
      1.175e-01      5.330e+02
...
      9.550e-01              0
      9.772e-01              0
      1.000e+00
```

During merging of the N pieces of raw job/thread histogram file, the following columns are calculated and printed to the final `<quantity>_<id>_histogram.out` file:
* **Bins:** same binning as in job/thread histograms
* **Counts:** sum of the counts (to get the average, just divide this by N)
* **StdDev(corr):** absolute, corrected sample standard deviation of the final population, estimated from samples
  * $StdDev_{corr} = s = \sqrt{ \frac{1}{N-1} \sum_i^N(Y_i - \overline{Y})^2 } $, where $Y_i$ are the bin counts of job/thread histograms and $\overline{Y}$ is their average
  * (to get uncorrected sample standard deviation: $s^2 = \frac{N}{N-1} \sigma_Y^2$)

Example of a merged `Edep_<id>_histogram.out` file:
```
           Edep         Counts   StdDev(corr)
      underflow      1.420e+02      2.809e+00
       overflow              0              0
      1.000e-01      1.740e+02      3.437e+00
      1.023e-01      4.340e+02      5.580e+00
      1.047e-01      9.030e+02      1.192e+01
      1.072e-01      1.767e+03      2.072e+01
      1.096e-01      3.275e+03      3.942e+01
      1.122e-01      5.440e+03      6.223e+01
      1.148e-01      8.038e+03      9.112e+01
      1.175e-01      1.099e+04      1.241e+02
...
      9.550e-01              0              0
      9.772e-01              0              0
      1.000e+00
```

```{figure} _static/figures/G4SEE_14p8MeV_neutron_Edep.png 
:width: 90%
:align: center

Standard Scoring histogram of 14.8 MeV neutrons in Si diode
```

## Detailed Scoring Hits data

When Detailed Scoring feature of *G4SEE* is enabled, then each thread saves hits of SV particle-by-particle to `Hits_t<N>.out` output file.
In Detailed Scoring, all particles are scored and printed to Hits file, which have entered or was produced in the Sensitive Volume (SV).

These files can grow very fast in file size, can easily take up huge amount of file storage space! 
It is strongly recommended to check what is the expected total Hits file size before starting a longer simulation run! 
Particle hits data are dumped to file regularly during a DS simulation run after exceeding a memory or event limit, 
which can be controlled via the `/SEE/scoring/detailed/dumpMemoryLimit` and `dumpEventLimit` commands.

Columns (in the same order as in Hits file):
- `event`: event number {mandatory}
- `particle`: name of particle {mandatory}
- `weight`: inverse biasing weight of particle/event if XS biasing was used {mandatory}
- `Z`: particle atomic number {optional, default: false}
- `A`: particle mass (nucleon) number {optional, default: false} 
- `track`: track (particle) ID {optional, default: true}
- `parent`: parent particle ID {optional, default: true}
- `E_kin`: kinetic energy of particle when entering or produced in SV (unit: *MeV*) {optional, default: true}
- `pos_x`, `pos_y`, `pos_z`: x, y, z coordinates of particle position when entering or produced in SV (unit: *mm*) {optional, default: false}
- `mom_x`, `mom_y`, `mom_z`: x, y, z coordinates of particle momentum when entering or produced in SV (unit: *MeV/c*) {optional, default: false}
- `process`: creator process of particle, 'biasWrapper()' indicates that XS biasing was used for that process {optional, default: false}  
- `volume`: name of the volume where particle produced {optional, default: false}  
- `E_dep`: total energy deposited by particle in SV (unit: *MeV*) {optional, default: true}
- `N_dep`: total non-ionizing energy deposited by particle in SV (unit: *MeV*) {optional, default: false}
- `counts`: number of grouped e-, e+ or gamma particles in event {optional, default: true}
- `E_exc`: excitation energy of nuclei, if it is produced in excited state (unit: *MeV*) {optional, default: false}

Example of a `Hits_t<N>.out` Detailed Scoring output file (showing only 3 proton events):
```
   event          particle      weight       Z       A       track      parent       E_kin       pos_x       pos_y       pos_z       mom_x       mom_y       mom_z                 process            volume       E_dep       N_dep  counts       E_exc
       0            proton   1.008e+00       1       1           1           0  9.7886e+00   -1.95e-08   -3.75e-07   -5.00e-04    6.93e-02   -4.05e-02   -1.36e+02                 primary             World  1.8389e+00           0       1           0
       0                e-   1.051e+00       0       0          20           1  1.7251e-02   -1.59e-04    1.04e-04   -2.15e-02    5.25e-02   -1.34e-02   -1.22e-01                   hIoni         Sensitive  1.7251e-02           0       1           0
       0                e-   1.273e+00       0       0          91           1  1.7018e-02   -7.51e-04   -1.05e-04   -1.11e-01   -5.53e-03   -3.93e-02   -1.27e-01                   hIoni         Sensitive  1.5718e-02           0       1           0
       0                e-   1.319e+00       0       0         118           1  1.8867e-02   -5.28e-04   -1.16e-04   -1.29e-01   -4.95e-03   -8.60e-03   -1.40e-01                   hIoni         Sensitive  1.6024e-02           0       1           0
       0                e-   1.329e+00       0       0         124           1  1.5299e-02   -4.61e-04   -1.39e-04   -1.33e-01   -5.03e-02   -2.97e-02   -1.12e-01                   hIoni         Sensitive  1.5299e-02           0       1           0
       0            ga(e-)         nan       0       0          -1           1         nan         nan         nan         nan         nan         nan         nan                     nan         Sensitive  8.0312e-01           0     289           0
       0            ga(e-)         nan       0       0          -2          91         nan         nan         nan         nan         nan         nan         nan                   eIoni         Sensitive  1.3006e-03           0       1           0
       0            ga(e-)         nan       0       0          -3         118         nan         nan         nan         nan         nan         nan         nan                   eIoni         Sensitive  2.8434e-03           0       2           0
       1            proton   1.006e+00       1       1           1           0  9.9739e+00           0           0   -5.00e-04   -9.57e-02   -8.19e-02   -1.37e+02                 primary             World  5.7929e-02           0       1           0
       1           neutron   1.028e-03       0       1           9           1  1.5393e+00    7.28e-06    2.66e-05   -1.15e-02    4.12e+01    2.26e+01   -2.63e+01      b(protonInelastic)         Sensitive           0           0       1           0
       1             gamma   1.028e-03       0       0          10           1  2.3996e+00    7.28e-06    2.66e-05   -1.15e-02    9.60e-01    1.99e+00    9.30e-01      b(protonInelastic)         Sensitive           0           0       1           0
       1               P30   1.028e-03      15      30          12           1  2.6201e-01    7.28e-06    2.66e-05   -1.15e-02   -4.17e+01   -2.37e+01   -1.11e+02      b(protonInelastic)         Sensitive  2.6201e-01           0       1           0
       1            ga(e-)         nan       0       0          -1           1         nan         nan         nan         nan         nan         nan         nan                   hIoni         Sensitive  2.0196e-02           0       7           0
       1         ga(gamma)         nan       0       0          -2           1         nan         nan         nan         nan         nan         nan         nan      b(protonInelastic)         Sensitive           0           0       1           0
       2            proton   1.002e+00       1       1           1           0  1.0049e+01    6.97e-08   -2.39e-07   -5.00e-04    3.67e-02   -8.86e-02   -1.38e+02                 primary             World  1.4359e+00           0       1           0
       2                e-   1.127e+00       0       0          44           1  1.7355e-02   -5.79e-04   -7.58e-04   -5.08e-02   -3.43e-02   -4.60e-02   -1.21e-01                   hIoni         Sensitive  1.4933e-02           0       1           0
       2                e-   1.177e+00       0       0          65           1  1.9941e-02   -5.67e-04   -9.63e-04   -6.99e-02    2.43e-02    7.73e-03   -1.42e-01                   hIoni         Sensitive  1.8540e-02           0       1           0
       2                e-   1.349e+00       0       0         131           1  1.6440e-02   -1.24e-03   -1.32e-03   -1.34e-01    1.52e-04    4.82e-02   -1.21e-01                   hIoni         Sensitive  1.5360e-02           0       1           0
       2              Si28   1.632e-03      14      28         230           1  5.4918e-01   -3.87e-03   -3.70e-04   -2.40e-01   -3.31e+01   -5.24e+01   -1.57e+02      b(protonInelastic)         Sensitive  5.4918e-01           0       1           0
       2            proton   1.632e-03       1       1         231           1  2.4233e+00   -3.87e-03   -3.70e-04   -2.40e-01    2.58e+01    5.01e+01    3.71e+01      b(protonInelastic)         Sensitive  2.0739e+00  1.0748e-03       1           0
       2             gamma   1.632e-03       0       0         232           1  3.1873e+00   -3.87e-03   -3.70e-04   -2.40e-01    1.80e+00    2.54e+00   -6.81e-01      b(protonInelastic)         Sensitive           0           0       1           0
       2             gamma   1.632e-03       0       0         233           1  1.7836e+00   -3.87e-03   -3.70e-04   -2.40e-01    1.41e-02    1.32e+00   -1.19e+00      b(protonInelastic)         Sensitive           0           0       1           0
       2            ga(e-)         nan       0       0          -1           1         nan         nan         nan         nan         nan         nan         nan                     nan         Sensitive  6.1624e-01           0     237           0
       2            ga(e-)         nan       0       0          -2          44         nan         nan         nan         nan         nan         nan         nan                   eIoni         Sensitive  2.4222e-03           0       2           0
       2            ga(e-)         nan       0       0          -3          65         nan         nan         nan         nan         nan         nan         nan                   eIoni         Sensitive  1.4008e-03           0       1           0
       2            ga(e-)         nan       0       0          -4         131         nan         nan         nan         nan         nan         nan         nan                   eIoni         Sensitive  1.0804e-03           0       1           0
       2            ga(e-)         nan       0       0          -5         231         nan         nan         nan         nan         nan         nan         nan                     nan         Sensitive  3.4940e-01           0     212           0
```
- DS is enabled with `/SEE/scoring/detailed   true`
- 10 MeV proton primaries (Gaussian energy with 200 keV std. dev.) with `protonInelastic` interactions XS biased (indicated with `b()`)
- Kinetic energy thresholds used for individual particle scoring: 15 keV for e$^-$ and 1 MeV for $\gamma$
- e$^-$ and $\gamma$ are grouped by their ancestors (indicated with `ga()`), groups have negative track ID
- All the optional quantities are enabled (thus scored and printed) using all the commands like `/SEE/scoring/detailed/addMomentum`
- CSV file format is disabled (just for better visual display)

Another example of Detailed Scoring output with different settings (showing only 4 neutron events):
```
   event          particle      weight       track      parent       E_kin                 process       E_dep  counts       E_exc
      15           neutron   2.021e-03           1           0  1.4800e+01                 primary           0       1           0
      15            proton   2.021e-03           2           1  3.2360e+00     b(neutronInelastic)  2.6470e+00       1           0
      15             gamma   2.021e-03           3           1  5.4022e+00     b(neutronInelastic)           0       1           0
      15             gamma   2.021e-03           4           1  2.1153e+00     b(neutronInelastic)           0       1           0
      15             Al28m   2.021e-03           5           1  1.5723e-01     b(neutronInelastic)  1.5723e-01       1   3.064e+01
      15             g(e-)         nan          -1         nan         nan                     nan  5.8904e-01     331           0
      16           neutron   1.008e-03           1           0  1.4800e+01                 primary           0       1           0
      16              Si28   1.008e-03           2           1  1.0745e+00     b(neutronInelastic)  1.0745e+00       1           0
      16           neutron   1.008e-03           3           1  4.0265e+00     b(neutronInelastic)           0       1           0
      16             gamma   1.008e-03           4           1  7.9169e+00     b(neutronInelastic)           0       1           0
      16             gamma   1.008e-03           5           1  1.7821e+00     b(neutronInelastic)           0       1           0
      17           neutron   2.151e-03           1           0  1.4800e+01                 primary           0       1           0
      17            proton   2.151e-03           2           1  3.6445e+00     b(neutronInelastic)  2.9932e+00       1           0
      17             gamma   2.151e-03           3           1  3.7906e+00     b(neutronInelastic)           0       1           0
      17             gamma   2.151e-03           4           1  2.9867e+00     b(neutronInelastic)           0       1           0
      17              Al28   2.151e-03           5           1  5.1960e-01     b(neutronInelastic)  5.1960e-01       1           0
      17             g(e-)         nan          -1         nan         nan                     nan  6.5123e-01     351           0
      18           neutron   1.400e-03           1           0  1.4800e+01                 primary           0       1           0
      18                e-   1.400e-03          17           2  1.0328e-02                   hIoni  8.9250e-03       1           0
      18            proton   1.400e-03           2           1  4.8414e+00     b(neutronInelastic)  3.7430e+00       1           0
      18             gamma   1.400e-03           3           1  5.6328e+00     b(neutronInelastic)           0       1           0
      18             Al28m   1.400e-03           4           1  4.3648e-01     b(neutronInelastic)  4.3648e-01       1   3.064e+01
      18             g(e-)         nan          -1         nan         nan                     nan  1.0894e+00     535           0
```
- `/SEE/scoring/detailed   true` is used to enable DS 
- 14.8 MeV mono-energetic neutron primaries with `neutronInelastic` interactions XS biased (indicated with `b()`)
- Default kinetic energy thresholds for individual particle scoring are used (10 keV for e$^-$ and 100 keV for $\gamma$)
- No ancestor grouping enabled, so just regular grouping (max. 1 e$^-$ group and 1 $\gamma$ group per event, indicated with `g()`), groups have negative track ID
- Additional to the default scored quantities, excitation energy and creator process scoring are also enabled

```{figure} _static/figures/G4SEE_DetailedScoring_14p8MeV_neutrons.png 
:width: 90%
:align: center

Detailed Scoring data of 14.8 MeV neutrons in Si diode
```
