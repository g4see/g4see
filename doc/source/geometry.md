<!--
 
G4SEE Single Event Effect simulation toolkit
============================================
SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>

This file is distributed under the terms of the Creative Commons Attribution 4.0 International,
copied verbatim in the file "LICENSES/CC-BY-4.0.txt".

In applying this license, CERN does not waive the privileges and immunities granted to it
by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.

SPDX-License-Identifier: CC-BY-4.0
 
Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>
 
-->

# Geometry & Materials

## Material macro commands

Macro commands related to material of the Target.

### Element definition

```shell
# define element with isotopes  NAME        CH.SYM.   ISOTOPES 
/SEE/material/addElement        enriched_B  B         10  0.96    11  0.04
```

| `addElement` parameters | Type   | M?  | Description                            | Example value |
|-------------------------|--------|-----|----------------------------------------|---------------|
| NAME                    | string | y   | Name of new element (its reference)    | `enriched_B`  |
| CHEMICAL SYMBOL         | string | y   | Chemical symbol of element             | `B`           |
| ISOTOPE NUCLEON NUMBER  | int    | y   | Nucleon (mass) number of an isotope, A | `10`          |
| ISOTOPE ABUNDANCE       | double | y   | Abundance fraction of an isotope       | `0.96`        |

ISOTOPE NUCLEON NUMBER and ISOTOPE ABUNDANCE parameters can only be used in pairs: "A$_1$ abundance$_1$ A$_2$ abundance$_2$ ..."

There are no default values for the parameters of this command.

### Material definition

```shell
# define material           NAME        DENSITY       ELEMENTS
/SEE/material/addMaterial   ShieldMat   2.52 g/cm3    enriched_B 4  C 1
/SEE/material/addMaterial   Si3N4       3.17 g/cm3    Si 3          N 4
```

| `addMaterial` parameters        | Type     | M?  | Description                           | Example value     |
|---------------------------------|----------|-----|---------------------------------------|-------------------|
| NAME                            | string   | y   | Name of new material (its reference)  | `Si3N4`           |
| DENSITY with unit               | double u | y   | Material density                      | `3.17 g/cm3`      |
| ELEMENT NAME or CHEMICAL SYMBOL | string   | y   | Name or chemical symbol of an element | `enriched_B`, `C` |
| ELEMENT STOICHIOMETRIC RATIO    | double   | y   | Chemical ratio of an element          | `4`               | 

Any element of the periodic table can be used for CHEMICAL SYMBOL parameter.  

ELEMENT NAME/CHEMICAL SYMBOL and ELEMENT STOICHIOMETRIC RATIO parameters can only be used in pairs: "name$_1$ ratio$_1$ name$_2$ ratio$_2$ ..."

There are no default values for the parameters of this command.

### Mixture definition

```shell
# define mixture            NAME        DENSITY       MATERIALS
/SEE/material/addMixture    BEOLmix     3 g/cm3       G4_Cu 0.4   Si3N4 0.6
```

| `addMixture` parameters   | Type     | M?  | Description                             | Example value    |
|---------------------------|----------|-----|-----------------------------------------|------------------|
| NAME                      | string   | y   | Name of new mixture (its reference)     | `mixMat`         |
| DENSITY with unit         | double u | y   | Material density of homogeneous mixture | `3.41 g/cm3`     |
| MATERIAL NAME             | string   | y   | Name of a component                     | `G4_Cu`, `Si3N4` |
| MATERIAL MASS FRACTION    | double   | y   | Mass fraction of a component            | `0.35`           | 

Any predefined [G4 element/material from this list](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html)
can be used for the MATERIAL NAME parameter.

MATERIAL NAME and MATERIAL MASS FRACTION parameters can only be used in pairs: "name$_1$ mass_fraction$_1$ name$_2$ mass_fraction$_2$ ..."

There are no default values for the parameters of this command.


## Geometry macro commands

Macro commands related to geometry of the target.

Target geometry is located inside a World, and consists of a Bulk and arbitrary number of Back End Of Line (BEOL) layer volumes (optional). 
Inside the Bulk volume, one Sensitive Volume (SV) is defined with same material as the Bulk. 
The scoring of defined quantities ($E_{dep}$, $E_{kin}$) happens only inside this SV.

Center of the top Bulk surface is always in the origin (0,0,0). Position of SV (the center of its top surface) is defined relative to this, inside Bulk only.

```{figure} _static/figures/SRAM_example_xz.png
:align: center

X-Z side view of a multi-layer target geometry 
```

```{figure} _static/figures/SRAM_example_xy.png
:width: 70%
:align: center

X-Y top view of a multi-layer target geometry 
```

See `examples/SRAM.mac`.

### Definition of BULK

Use this command only once.
```shell
# BULK                MATERIAL      WIDTH unit      THICK unit  BIAS
/SEE/geometry/Bulk    G4_Si         10. um          10. um      true
```

| `Bulk` parameters   | Type     | M?  | Description                    | Example value |
|---------------------|----------|-----|--------------------------------|---------------|
| MATERIAL            | string   | y   | Bulk layer material            | `G4_Si`       |
| WIDTH with unit     | double u | y   | Bulk width (X and Y dimension) | `10 um`       |
| THICKNESS with unit | double u | y   | Bulk thickness (Z dimension)   | `9 um`        |
| BIAS                | bool     | n   | Enable XS bias in Bulk layer   | `true`        |

Any predefined [G4 element/material from this list](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html)
can be used for the MATERIAL parameter. Additionally, `VACUUM` can be also used as predefined material.

Bulk dimensions (RPP): $w_{b}$ $\times$ $w_{b}$ $\times$ $t_{b}$

There are no default values for the parameters of this command.

### Definition of SV

Use this command only once.
```shell
# SV                  POSITION unit       X-Y WIDTH unit  THICK unit  BIAS
/SEE/geometry/SV      0. 0. -1. um        3. 1. um        3. um       true
```

| `SV` parameters            | Type          | M?  | Description                       | Example value |
|----------------------------|---------------|-----|-----------------------------------|---------------|
| X, Y, Z POSITION with unit | double[3] u   | y   | Sensitive Vol. X, Y, Z position   | `0 0 -1.5 um` |
| X, Y WIDTH with unit       | double[1,2] u | y   | Sensitive Vol. X, Y dim.          | `2 1 um`      |
| THICKNESS with unit        | double u      | y   | Sensitive Vol. thickness (Z dim.) | `1 um`        |
| BIAS                       | bool          | n   | Enable XS bias in Sensitive Vol.  | `true`        |

Material of the SV is the same as its mother volume, which by default is the Bulk.

SV dimensions (RPP): $w^x_{sv}$ $\times$ $w^y_{sv}$ $\times$ $t_{sv}$

There are no default values for the parameters of this command.

### Definition of BEOL layers

Optional command, define as many Back End Of Line (BEOL) layers on top of the Bulk volume as one needs.
Each BEOL layer is added on top of the previous BEOL layers in the same order as the `/SEE/geometry/BEOL/addLayer` commands follow each other in the macro.

```shell
# BEOL                        MATERIAL    WIDTH unit  THICK unit  NAME      BIAS 
/SEE/geometry/BEOL/addLayer   BEOLmix     4. um       800 nm      BEOL1     false
/SEE/geometry/BEOL/addLayer   G4_Ti       4. um       200 nm      BEOL2     false
/SEE/geometry/BEOL/addLayer   OxideMat    4. um       400 nm      BEOL3     false
```

```{figure} _static/figures/G4SEE_example_geometry.png
:width: 70%
:align: center

BEOL layers in a target geometry
```

| `addLayer` parameters | Type          | M?  | Description                     | Example value |
|-----------------------|---------------|-----|---------------------------------|---------------|
| MATERIAL              | string        | y   | BEOL layer material             | `G4_Al`       |
| WIDTH with unit       | double[1,2] u | y   | BEOL layer width (X and Y dim.) | `2 1 um`      |
| THICKNESS with unit   | double u      | y   | BEOL layer thickness (Z dim.)   | `1 um`        |
| NAME                  | string        | y   | Name of BEOL layer              | `Oxide`       |

Any predefined [G4 element/material from this list](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/Appendix/materialNames.html) 
can be used for the MATERIAL parameter. Additionally, 'VACUUM' can be also used as material.

BEOL dimensions (RPP): $w_{beol1}$ $\times$ $w_{beol1}$ $\times$ $t_{beol1}$

There are no default values for the parameters of this command.

### Special geometry commands

| Macro command                          | Type   | M?  | Description                                              | Example value | Default value |
|----------------------------------------|--------|-----|----------------------------------------------------------|---------------|---------------|
| `/SEE/geometry/setCylindricalGeometry` | bool   | n   | Use cylindrical volumes instead of default cuboids       | `true`        | false         |
| `/SEE/geometry/setWorldMaterial`       | string | n   | Set material for World volume                            | `G4_AIR`      | `VACUUM`      | 
| `/SEE/geometry/setWorldAsMotherOfSV`   | bool   | n   | Use World volume as mother of SV, instead of Bulk volume | `true`        | false         | 

Setting the `WorldAsMotherOfSV` true makes possible to score outside Bulk. Use with caution! In this case, SV material is VACUUM and SV should be placed outside Bulk volume.

```{figure} _static/figures/diode_example_xy.png
:width: 70%
:align: center

X-Y top view of a cylindrical diode geometry
```
