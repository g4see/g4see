<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# Building the docs

First install Sphinx and all dependencies:

`pip install -r requirements-docs.txt`

Then build the multi-version HTML documentation (`git` is also needed!):

`sphinx-multiversion source_dir build_dir`

Or build only the latest version of the HTML documentation:

`sphinx-build -E -a source_dir build_dir`

Build the LaTeX pdf:

`sphinx-build -b latex source_dir build_dir`

then inside `build_dir`: `make`

Build the man page:

`sphinx-build -b man source_dir build_dir`
