<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# Changelog 

## Release v1.0

**[https://gitlab.cern.ch/g4see/g4see/-/releases/v1.0](https://gitlab.cern.ch/g4see/g4see/-/releases/v1.0)**

### New features
- Linear Energy Transfer (LET) scoring in SV with 3 different options: LET can be scored either event-by-event, step-by-step, or calculated based on Kinetic Energy (Ekin) of particles
- Particle filtering options for step-by-step scoring of Ekin and LET: values can be scored either for primary, non-primary, all particles, or named particle species in SV
- Particle step filtering options for step-by-step scoring of Ekin and LET: values can be scored either during first, last or all steps of particles in SV (the step-by-step values are weighted by the number of steps per particles)
- Total Ionizing Dose (TID) and Total Non-Ionizing Dose (TNID) scoring in Sensitive Volume (SV)
- The use of various options for the “High Precision” (HP) particle models have been enabled, including special neutron-carbon interaction models
- Material of “World” volume (around target) can be defined (e.g. it can be air), by default it is vacuum
- New “Blackbody” volume has been added to stop particle transport behind the target (“Bulk”) volume to reduce simulation running time in air
- Particle production range cuts can be defined per region
- User-defined visualization of simulation geometry and particle trajectories via Geant4 Graphical User Interface (GUI), `vis.mac` macro file was added for visualization 
- New `diamond_detector.mac` example simulating diamond detector's fast neutron response has been added 
- New `HEARTS@CERN.mac` example with very high-energy heavy ions and LET scoring has been added 

### Changes
- Supported Geant4 releases: 
  - 11.2.2 (recommended)
  - 11.2.1
  - 11.2.0 
- New, improved User Interface (UI) macro commands
  - Changes in standard scoring and related histogram macro commands, see example commands:
    ```shell
    /SEE/scoring/standard/Edep          1
    /SEE/scoring/setHistogram           lin     200     0       1       GeV
    /SEE/scoring/standard/Ekin          2       primary first
    /SEE/scoring/setHistogram           lin     200     0       10      GeV
    /SEE/scoring/standard/LET           3       Opt3    primary all
    /SEE/scoring/setHistogram           lin     200     0       100
    ```
  - Optional use of biasing parameter in case of geometry macro commands
- Energy threshold of production range cut was changed to 990 eV, which is the default value of Geant4 (previously it was 10 eV). Users should explicitly set this threshold with `/cuts/setLowEdge` command if they need a lower (or higher) energy cut threshold. Please check the documentation about production and tracking cuts, as well as the `SRAM.mac` example!
- Example marco files have been updated with minor changes
- Point-wise spectrum data file (`differential_fluence.data`) has been added for the `spectrum.mac` example
- Restructured, updated and extended online documentation

### Fixes
- Excitation energies are extracted in case of every heavy ion
- Excitation energies are converted and printed in MeV to detailed scoring output files
- Random number seeds can be set properly
- Macro file parametrization works with multiple placeholders per macro line
- GitLab CI/CD fixes and improvements

----

## Release v0.5.2

**[https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5.2](https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5.2)**

----

## Release v0.5.1

**[https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5.1](https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5.1)**

----

## Release v0.5

**[https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5](https://gitlab.cern.ch/g4see/g4see/-/releases/v0.5)**

### New features
- Reimplementation of the Detailed Scoring (DS):
  - Individual scoring and printing of electrons above energy threshold (#90, #92, #114, #115)
  - Disabling primary particle printing (#100)
  - New quantity scored: nuclei excitation energy (#103)
  - Printing DS data to file less often (#102)
- DevOps:
  - New HTML documentation generated and published at [https://g4see-docs.web.cern.ch](https://g4see-docs.web.cern.ch)
  - Licenses and copyrights compliant with SPDX standards and CERN policies (#121)
  - Compiled and tested with latest G4 releases (<=11.0.3) (#113)
  - New Docker images (#16)
  - Updated and extended GitLab CI/CD pipeline (#11)
  - Added `make install` (#128)
- Deleting cluster jobs based on jobID list/range (#79)

### Changes
- Moved to a new GitLab repository: [https://gitlab.cern.ch/g4see/g4see](https://gitlab.cern.ch/g4see/g4see)
- Refactored python scripts into a git submodule repository: [https://gitlab.cern.ch/g4see/g4see-scripts](https://gitlab.cern.ch/g4see/g4see-scripts)
- Removed smessmer/gitversion dependency (#96)
- Detailed Scoring is enabled by a new macro command (#112)
- Set CSV file format as default for the DS Hits file (#101)
- Set DS `Counts` column is enabled by default (#108)
- More general `g4see.py submit` script (#131)
- No more `VerboseElectron` and `VerboseGamma` commands (#89)
- Renamed grouped electrons to avoid confusion (#107)
- Renamed `macro` folder (#124)
- Renamed C++ classes (#118)
- Renamed macro commands starting with capital letter (#109)
- Renamed `biasWrapper()` in detailed scoring output (#105)

### Fixes
- Memory leaks in new Detailed Scoring (#120, #119)
- Segmentation fault when running with DS (#117, #116)
- Can not find `mergeHistograms` in MT mode (#127)
- GPS spectrum definition in `spectrum.mac` file (#104)
- Name of excited nuclei contains excitation energy (#103)
- Multiple macro lines can not be overwritten if they are the same (#94)
- Not all macro files are copied by `cmake` build (#91)
- Clang-formatted C++ source, coding style can be found in `.clang-format` file

----

## Release v0.4.2

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4.2](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4.2)**

### New features
* Particle Z, A and Volume where particle produced can be also scored during Detailed scoring (#85, #83) 
* CSV file format can be used for the Detailed scoring Hits file (#86)

----

## Release v0.4.1

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4.1](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4.1)**

### New features
* Print number of events simulated to file with histograms
* Particle momentum can be also scored during Detailed scoring 
* Scoring of the quantities in Detailed scoring can be enabled/disabled 
* G4SEE Docker images (see [GitLab Container Registry](https://gitlab.cern.ch/r2e-bmi/g4see/geant4-see/container_registry))

### Changes
* The latest Geant4 version 10.07 is the new baseline, backward compatibility with 10.06
* BEOL layer can be created with zero thickness (to simplify parametric runs)
* Figures of `g4see.py` scripts are saved to file by default
* Allow zero value for production range cuts
* Renamed macro command `/SEE/physics/setProtonCut` to `/SEE/physics/setHadronCut`

### Fixes
* Last bins of plot made by `g4see.py plot` are shown properly

----

## Release v0.4

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.4)**

### New features
* Material and Geometry:
  * Material definitions in macro: element, compound, mixture
  * Multi-layer target structure, `/BEOL/addLayer` macro command
  * Option to define cylindrical target geometry
  * Option to define World as mother of SV (default: Bulk) 
  * Predefined material additional to G4 materials: `VACUUM`
* General scoring:
  * Arbitrary number of histograms per simulation, `addScoring` and `setHistogram` macro commands
  * Particle kinetic energy scoring in SV
  * Option to write histogram data to file after every N events
  * Option to set floating point number precision for histogram file 
* Detailed scoring per particle
  * Grouped or individual (verbose) gamma scoring 
  * Grouped or grouped by closest non-electron ancestors (verbose) electron scoring
* New example macro and parametric yaml files

### Changes
* Format of `SV` and `Bulk` macro commands
* Top center points of Bulk and (by default) SV are located in origin (0,0,0)  
* X,Y,Z coordinates of SV is user-defined, Asymmetric SV in X and Y dimension 
* Option to bias multiple particle species and non primary particles
* Raise messenger error when non-existing physics module or particle for biasing was added
* `mergeHistograms`: refactoring and improvements  
  * Calculating corrected sample standard deviation during histogram merging
* Python scripts have been refactored and can be run via `g4see.py {submit,delete,view,merge,plot}`:  
  * `submit`: input arguments, parametric run with multiple parameters
  * `delete`: delete jobs submitted to cluster nodes
  * `view`: geometry visualisation from macro file
  * `merge`: recursive histogram plotting, input arguments
  * `plot`: 2D visualisation of target geometry from 3 views

### Fixes
* No more hardcoded application paths in *g4see.py*
* str->int conversion bug in *g4see.py* script
* NaN value as histogram counts bug

----

## Release v0.3.1

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.3.1](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.3.1)**

### Changes
* created `build-latest` sym link to latest build directory
* `scripts` directory is copied into build
* fixed and updated documentation  

### Fixes
* Segmentation fault caused by lack of insulator volume

----

## Release v0.3

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.3](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.3)**

### New features
* Python scripts for submitting, checking and processing data
* `mergeHistograms` tool for merging data files
* Listing and removing particle processes with new macro commands
* Added all relevant G4 Physics constructors
* Added `G4MicroElecPhysics`
* Physics and Macro commands documentation

### Changes
* Default EM physics: `G4EmStandardPhysics_option4`
* Energy threshold of production range cut conversion was changed to 10 eV
* Range cut macro commands
* Default production range cuts: $\gamma$: 1 mm, $e^{\pm}$: 1 $\mu$m, proton: 100 nm 
* Default geometry parameters
* Biasing macro commands
* Optimized code to enhance performance 
* Single-threaded by default, optional multi-threaded mode

### Fixes
* Bug in creating absolute file paths
* Random seed generation

----

## Release v0.2

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.2](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.2)**

### New features
* Generating output file `g4see.out` including input macro
* Getting `git` version using `gitversion` submodule
* Checking paths if they exist
* Measuring total CPU time

### Changes
* Command-line arguments `-j` and `-s` were removed, instead use G4 macro commands
* Better multi-threading

----

## Release v0.1

**[https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.1](https://gitlab.cern.ch/r2e-bmi/geant4-see/-/releases/v0.1)**


